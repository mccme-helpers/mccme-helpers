(mccme-helpers/packages:file mccme-helpers/functional
  :use ()
  :export (
           #:capture
           #:defdelay
           #:defcaptured
           #:defcached
	   #:cache
	   #:cached-progn
	   #:multifuncall
           #:curry
           #:transpose
           #:backtraceable
           #:lazy-map
           #:*backtrace-error-context*
	   #:negation
	   #:negation-1
	   #:callback-collect
    )
  )

(defmacro capture (&rest body)
  `(with-output-to-string (*standard-output*)
                          ,@body))

(defmacro defdelay (name args &rest body)
  `(defun ,name ,args
          (lambda ()
                  ,@body)))

(defmacro defcaptured (name args &rest body)
  `(defun ,name ,args
          (capture ,@body)))

(defun cache (f)
  (let* (
         (cache-table (make-hash-table :test 'equal))
         )
        (lambda (&rest x)
                (multiple-value-bind (elem found) (gethash x cache-table)
                                     (if found elem
                                         (let ((result (apply 'funcall f x)))
                                              (setf (gethash x cache-table) result)
                                              result))))))

(defmacro defcached (f args &rest body)
  (let* (
         (cached-sym (gensym))
         )
        `(let* (
                (orig-fun (lambda ,args ,@body))
                (,cached-sym (cache orig-fun))
                )
              (defun ,f (&rest args)
                     (apply 'funcall ,cached-sym args)))))

(defmacro cached-progn (&rest body)
  `(cache (lambda () ,@body)))

(defun curry (f &rest args)
  (lambda (&rest args-2)
          (apply 'funcall f 
                 (append args args-2))))

(defun transpose (f)
  (lambda (x y &rest args) (apply 'funcall f y x args)))

(defparameter *backtrace-error-context* nil)

(defmacro backtraceable (&rest body)
  `(progn
    (handler-bind
     ((error
       (lambda 
        (x)
      (with-open-file (f 
                        (mccme-helpers/trivial:temp-file-name 
                        (format nil "/var/log/esr/~a/~a/~a/~a"
                                (local-time::today)
                                (ignore-errors 
                                   (symbol-value 
                                     (find-symbol "*SERVER-PORT*"
                                        (find-package :olymp-site/launcher))))
                                (local-time:now)
                                "backtrace-XXXXXX"
                                  )
                                ) 
                        :direction :output :if-exists :supersede :if-does-not-exist :create)
        (format f "At: ~s~%" (local-time:now))
        (format f "With context:~%~s~%" 
                *backtrace-error-context*)
        (format f "Error: ~s~%" x)
        (format f "Error readably: ~a~%" x)
        (ignore-errors
         (format f "Error description: ~s~%" 
                 (apply 'format nil (simple-condition-format-control x)
                        (simple-condition-format-arguments x))))
            (trivial-backtrace:print-backtrace-to-stream
               f)
               )
       (when
             (ignore-errors
               (not
               (<= 6761
                 (symbol-value 
                  (find-symbol "*SERVER-PORT*"
                     (find-package :olymp-site/launcher))) 6776)))
            (trivial-backtrace:print-backtrace-to-stream
               *error-output*))
        (format *error-output* "Error readably again: ~a~%" x)
        )))
     (setf *backtrace-error-context* nil)
     ,@body)))

(defun lazy-map (generator f &optional pass-state starting-state)
  (let
   ((state starting-state)
    )
   (lambda 
    ()
    (let
     ((el (funcall generator))
      )
     (when 
      el
      (multiple-value-bind
       (next-el next-state)
       (apply f el (when pass-state (list state)))
       (setf state next-state)
       next-el))))))

(defun multifuncall (f &key args)
  (if (functionp f) (multifuncall (apply f args) :args args) f))

(defun negation (f) (lambda (&rest args) (not (apply f args))))
(defun negation-1 (f) (lambda (x) (not (funcall f x))))

(defun callback-collect (f &rest args)
  (let*
    ((res nil))
    (apply f (lambda (x) (push x res)) args)
    (reverse res)))
