(mccme-helpers/packages:file mccme-helpers/password 
  :use (mccme-helpers/parsetype)
  :export (#:hash-password
           #:password-matches-p
           #:hash)
  )

(defun hash (algo plaintext)
  (let* (
         (keyword-package (find-package :keyword))
         (digest-key (intern (string-upcase algo) 
                                  keyword-package))
         (digester (ironclad:make-digest digest-key))
         (plainoctets (
                       #+sbcl sb-ext:string-to-octets 
                       #+ccl ccl:encode-string-to-octets
                       plaintext 
                                              :external-format 
                                              :utf-8))
         (hashedtext (ironclad:digest-sequence digester 
                                               plainoctets))
         )
        hashedtext))

(defun salted-hash (algo salt password)
  (let* (
         (plaintext (concatenate 'string salt password))
         (hashedtext (hash algo plaintext))
         )
  hashedtext))

(defun password-matches-p (provided db-stored)
  (let* (
         (split-password (cl-utilities:split-sequence #\$ db-stored))
         (algo (first split-password))
         (salt (second split-password))
         (stored-hash (third split-password))
         (provided-hash (salted-hash algo salt provided))
         (provided-hash-string (octet-array-to-base16-string provided-hash))
         (matchp (equalp (string-upcase stored-hash) 
                         (string-upcase provided-hash-string)))
         )
        matchp))

(defun hash-password (algo salt password)
  (let* (
         (salt-string (cond 
                       ((stringp salt) salt)
                       ((listp salt) (apply 'concatenate 'string salt))
                       ))
         (full-salt (concatenate 'string 
                                 salt-string 
                                 (format nil "~a" (random 999999999))))
         (plaintext (concatenate 'string full-salt password))
         (digest (hash algo plaintext))
         (hash-string (octet-array-to-base16-string digest))
         (result (concatenate 'string algo "$" full-salt "$" hash-string))
         )
        result))

