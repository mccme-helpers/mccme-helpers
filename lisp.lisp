(mccme-helpers/packages:file mccme-helpers/lisp
  :use ()
  :export (
           #:source-directory
           #:timed
           #:concatenate-symbol-names
           #:capture-vars
           #:def-and-set-var
           #:pipe-value
           #:source-relative-path
	   #:save-and-exec
     #:defun-weak
     #:map-deep
     #:symbol-from-file
    )
  )

(defmacro source-directory (&key (to-absolute nil))
  (ignore-errors
    `',(progn
	 (pathname-directory
	   (funcall
	     (if to-absolute 'truename 'identity)
	     (or *compile-file-pathname* *load-pathname* ))))))

(defmacro timed (name f &rest args)
  `(progn
    (format *error-output* "~s~%" ,name)
    (time (,f ,@args))))

(defun concatenate-symbol-names (&rest args)
  (intern
   (apply 'concatenate 'string
         (mapcar (lambda (x) 
                         (cond 
                          ((symbolp x) (symbol-name x))
                          (t (format nil "~a" x))
                          )) 
                 args))
   (symbol-package (car args))
   ))

(defmacro capture-vars (variables &rest body)
  `(let
    ,(loop for v in variables collect `(,v ,v))
    ,@body
    )
  )

(defmacro def-and-set-var (var val)
  `(progn
    (defvar ,var)
    (setf ,var ,val)))

(defmacro pipe-value ((var starting-value) &rest forms)
  `(let ((,var ,starting-value))
        ,@(loop for x in forms collect `(setf ,var ,x))
        ,var
    ))

(defmacro source-relative-path (s &key (to-absolute nil))
  `(let* 
    (
     (s ,s)
     (sd (source-directory :to-absolute ,to-absolute))
     (sda (eq (car sd) :absolute))
     (ls (length s))
     (sa (and (>= ls 1) (equal (elt s 0) #\/)))
     (s-explicit-local
      (and (>= ls 2) (equal (subseq s 0 2) "./")))
     (sdp (if sda "/" ""))
     (sds (format nil "~a~{~a/~}" sdp (cdr sd)))
     (res 
      (when 
       (> ls 0) 
       (if (or sa s-explicit-local) 
           s (concatenate 'string sds s))))
     )
    res
    ))

(defmacro save-and-exec (storage &rest body)
  `(progn
     (eval-when
       (:load-toplevel :compile-toplevel :execute)
       (setf ,storage ',body))
     (progn
       ,@body)
     ))

(defmacro defun-weak (name args &rest body)
  "defun unless fboundp"
  `(unless (fboundp ',name)
           (defun ,name ,args
                  ,@body)))

(defmacro map-deep (f getter setter data &key place return-full)
  `(mapcar
     (lambda (x)
       (,@(if place `(setf (,setter x)) `(,setter x)) 
	 (,f (,getter x)))
       ,@(when (or place return-full) '(x)))
     ,data
    )
  )

(let ((counter 0))
  (defun symbol-from-file (file name &key packages)
    (let* 
      (
       (pname 
	 (string-upcase
	   (format 
	     nil "fresh-package-~a-~a"
	     (incf counter)
	     (random (expt 10 10))
	     )))
       (*package* (make-package pname :use (cons :common-lisp packages)))
       )
      (load (truename file))
      (prog1
	(intern (string-upcase name) *package*)
	(delete-package *package*)
	)
      )
    ))
