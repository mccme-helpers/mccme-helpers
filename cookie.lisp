(mccme-helpers/packages:file mccme-helpers/cookie
  :use (mccme-helpers/parsetype
        mccme-helpers/password
        local-time
        )
  :export (
           #:with-cookie-key
           #:save-value-cookie
           #:load-value-cookie
           #:set-secure-cookie
           #:get-secure-cookie
    )
  )

(mccme-helpers/packages:alias-package hunchentoot ht)

(defvar *cookie-key* "00000000000000000")

(defmacro with-cookie-key (key &body body)
  `(progv 
     '(*cookie-key*)
     (list ,key)
    ,@body))

(defun crypto-key (password)
  (let* (
         (hasher (make-instance 'ironclad:pbkdf2 :digest :sha1))
         (key (ironclad:derive-key hasher 
                                   (
                                    #+sbcl sb-ext:string-to-octets 
                                    #+ccl ccl:encode-string-to-octets
                                    password :external-format :utf-8)
                                   (make-array 8 :initial-element 32 :element-type '(unsigned-byte 8))
                                   1
                                   32)))
        key
        ))

(defun random-iv (length)
  (let ((res (make-array length :element-type '(unsigned-byte 8)
                         :initial-element 0)))
    (if (probe-file "/dev/urandom")
      (with-open-file (f "/dev/urandom" :element-type '(unsigned-byte 8))
        (read-sequence res f))
      (loop for k from 0 to length do (setf (elt res k) (random 256))))
    res))

(defun unencrypt-aes (x key)
  (let* (
         (iv-text (subseq x 0 32))
         (iv (base16-string-to-octet-array iv-text))
         (cipher (ironclad:make-cipher :aes :key key :mode :ofb 
                                       :initialization-vector iv))
         (ciphertext (subseq x 32))
         (byte-arr (base16-string-to-octet-array ciphertext))
         (buffer (make-array (length byte-arr) :initial-element 32 :element-type '(unsigned-byte 8)))
         (dummy (ironclad:decrypt cipher byte-arr buffer))
         (output (
                  #+sbcl sb-ext:octets-to-string 
                  #+ccl ccl:decode-string-from-octets
                  buffer :external-format :utf-8))
         )
        (declare (ignorable dummy))
        output
        ))

(defun encrypt-aes (x key)
  (let* (
         (iv (random-iv 16))
         (iv-text (octet-array-to-base16-string iv))
         (cipher (ironclad:make-cipher :aes :key key :mode :ofb 
                                       :initialization-vector iv))
         (byte-arr (
                    #+sbcl sb-ext:string-to-octets 
                    #+ccl ccl:encode-string-to-octets
                    x :external-format :utf-8))
         (buffer (make-array (length byte-arr) :initial-element 32 :element-type '(unsigned-byte 8)))
         (dummy (ironclad:encrypt cipher byte-arr buffer))
         (output-text (octet-array-to-base16-string buffer))
         (output (concatenate 'string iv-text output-text))
         )
        (declare (ignorable dummy))
        output
        ))

(defun save-value-cookie (value)
  (let* (
         (hash (hash "sha512" (concatenate 'string *cookie-key* value *cookie-key*)))
         (hash-str (octet-array-to-base16-string (subseq hash 0 16)))
         (timestamp-str (format nil "~12,'0d" (timestamp-to-unix (now))))
         (string-to-save (concatenate 'string timestamp-str value hash-str))
         (key-salt (format nil "~10,10,'0r" (random 999999999)))
         (password (concatenate 'string key-salt *cookie-key*))
         (real-key (crypto-key password))
         )
          (concatenate 'string 
                       key-salt
                       (encrypt-aes string-to-save
                                    real-key))
        ))

(defun load-value-cookie (cookie &optional timeout)
  (ignore-errors
   (let* (
          (timeout (or timeout 86400))
          (key-salt (subseq cookie 0 10))
          (password (concatenate 'string key-salt *cookie-key*))
          (real-key (crypto-key password))
          (ciphertext (subseq cookie 10))
          (plaintext (unencrypt-aes ciphertext real-key))
          (text-length (length plaintext))
          (checkhash (subseq plaintext (- text-length 32)))
          (output (subseq plaintext 12 (- text-length 32)))
          (timestamp-old (parse-integer (subseq plaintext 0 12)))
          (timestamp-fresh (timestamp-to-unix (now)))
          (hash (hash "sha512" (concatenate 'string *cookie-key* output *cookie-key*)))
          (hash-str (octet-array-to-base16-string (subseq hash 0 16)))
          )
         (when 
          (and
           (equalp checkhash hash-str)
           (< (- timestamp-fresh timestamp-old) timeout)
           )
          output)
         )))

(defun set-secure-cookie (name value)
  (when ht:*request*
        (if value (ht:set-cookie name
                                 :value (save-value-cookie value) 
                                 :path "/")
            (ht:set-cookie name :value "" :path "/"))))

(defun get-secure-cookie (name &optional timeout)
  (load-value-cookie (and (boundp 'ht:*request*) (ht:cookie-in name))
                     timeout))
