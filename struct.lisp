(mccme-helpers/packages:file
  mccme-helpers/struct
  :use (:iterate)
  :export (
     ; Core API
	   #:has-entry
	   #:get-entry
	   #:with-updated-entry
     
     ; Standard helpers
	   #:with-deep-update
	   #:->
	   #:-!>
     #:{}->
	   #:-->
	   #:--!>
	   #:set->
	   #:set-->
	   #:change-->
	   #:push->
	   #:push-->
	   #:incf-->
	   #:incf->
     
	   #:map-generic
	   #:get-keys
	   #:list-as-seq
	   #:join-values-and-names
	   #:index-entry
	   #:pos-by-entry
	   #:pos-by-getf-entry
	   #:kv-q
	   #:kv-f-q
	   #:output-structure
	   #:mapper-q
	   #:filter-q
	   #:apply-q
	   #:getf-q

	   #:list-q
	   #:or-q

	   #:merge-assoc
	   #:create-struct-index
           #:group-struct-by
	   #:join-struct-lists

	   #:to-getf-struct
	   )
  )

(defgeneric has-entry (obj attr)
  (:documentation
    "A predicate, tells whether extracting `attr` from `obj` is a meaningful operation"))
(defgeneric get-entry (obj attr &optional (default))
  (:documentation
    "Tries to extract `attr` from `obj`, returning the default value if it fails. The default defaults to `nil`. Yeah."))

(defgeneric with-updated-entry (obj attr value)
  (:documentation
    "Returns `obj` with the attribute `attr` set to `value`; can be destructive, depending on type of `obj`"))

(defgeneric map-generic (obj fn)
  (:documentation
    "Interprets `obj` as a list of {two element lists} '(attr value) and applies `fn` to each pair (as a single argument). Returns a list."))

(defun get-keys (obj) 
  "Returns a list of all keys in `obj`"
  (map-generic obj 'first)
  )

(defmethod has-entry (obj attr)
  nil)

(defmethod has-entry ((obj hash-table) (attr t))
  (multiple-value-bind 
              (elem found)
              (gethash attr obj)
              (declare (ignore elem))
              found))

(defmethod has-entry ((obj standard-object) (attr t))
  (slot-boundp obj attr))

(defmethod has-entry ((obj list) (attr t))
  (find attr obj
        :test 'equal
        :key 'car))

(defmethod has-entry ((obj list) (attr integer))
  (> (length obj) attr))

(defmethod has-entry ((obj package) (attr t))
             (multiple-value-bind (elem found) 
                                  (find-symbol attr obj) 
                                  (and found (boundp elem))))

(defmethod has-entry ((obj sequence) (attr integer))
  (< attr (length obj)))

(defmethod get-entry ((obj hash-table) attr &optional (default nil))
  (multiple-value-bind 
   (elem found)
   (gethash attr obj default)
   (if found elem default)))

(defmethod get-entry ((obj standard-object) attr &optional (default nil))
  (if (slot-boundp obj attr) (slot-value obj attr) default))

(defmethod get-entry ((obj list) attr &optional (default nil))
   (let ((x (find attr obj 
                  :test 'equal
                  :key 'car)))
        (if x (cadr x) default)))

(defmethod get-entry ((obj package) attr &optional (default nil))
  (multiple-value-bind (elem found) 
                       (find-symbol attr obj) 
                       (if (and found (boundp elem))
                           (symbol-value elem)
                           default)))

(defmethod get-entry ((obj sequence) (attr integer) &optional (default nil))
  (elt obj attr))

(defmethod get-entry ((obj list) (attr integer) &optional (default nil))
  (elt obj attr))

(defmethod with-updated-entry ((obj hash-table) attr value)
  (progn 
   (setf (gethash attr obj) value)
   obj
   ))

(defmethod with-updated-entry ((obj standard-object) attr value)
  (progn
   (setf (slot-value obj attr ) value)
   obj
   ))

(defmethod with-updated-entry ((obj list) attr value)
  (cons (list attr value) 
        (remove attr obj :test 'equal :key 'first)))

(defmethod with-updated-entry ((obj package) attr value)
  (progn
   (set (find-symbol attr obj) value)
   obj
   ))

(defmethod with-updated-entry ((obj sequence) (attr integer) value)
  (progn 
   (setf (elt obj attr) value)
   obj))

(defmethod with-updated-entry ((obj list) (attr integer) value)
  (progn 
   (setf (elt obj attr) value)
   obj))

(defun -> (obj attr &optional (default nil))
  "An alias for `get-entry`"
  (get-entry obj attr default))
(defun -!> (attr &optional (default nil))
  "Returns a function that extracts the value of attribute `attr` from its argument, returning `default` on failure"
  (lambda (obj) (-> obj attr default)))

(defun {}-> (obj &optional (default nil))
  "Returns a function that extracts the value of an attribute from `obj`, returning `default` on failure"
  (lambda (attr) (-> obj attr default))
)

(defun --> (obj default &rest attrpath)
  "Extracts the value /from/deep/inside/`obj`, returning `default` if the process fails at any step"
  (iter 
   (with x := obj)
   (for attr :in attrpath)
   (if (has-entry x attr) (setf x (get-entry x attr)) (return default))
   (finally (return x))
   ))
(defun --!> (default &rest attrpath)
  "Returns a function that extracts the value /from/deep/inside/its/argument, returning `default` on failure"
  (lambda (obj) (apply '--> obj default attrpath)))
(defmacro set-> (obj attr value)
  "Sets the value of the `attr` attribute of the `obj` object to `value`; updates the `obj` binding if `obj`'s value was immutable"
  `(setf ,obj (with-updated-entry ,obj ,attr ,value )))
(defmacro push-> (obj attr value)
  `(setf ,obj (with-updated-entry ,obj ,attr (cons ,value (get-entry ,obj ,attr)))))
(defun with-deep-update
  (obj attrs value &optional creator )
  "It is to `-->` what `with-updated-entry` is to `->`"
  (if attrs
      (let*
       (
        (current-val
         (or 
          (-> obj (car attrs)) 
          (funcall (or creator
                       (lambda 
                        (&key) (make-hash-table :test 'equal)))
                   :allow-other-keys t
                   :obj obj
                   :attr (car attrs))))
        )
       (set-> 
        obj (car attrs) 
        (with-deep-update current-val (cdr attrs) value creator))
       )
      value
      ))
(defmacro set--> (obj value creator &rest attrs)
  "It is to `-->` what `set->` is to `->`"
  `(setf ,obj (with-deep-update ,obj (list ,@attrs) ,value ,creator)))
(defmacro push--> (obj value creator &rest attrs)
  `(setf ,obj (with-deep-update 
		,obj (list ,@attrs) 
		(cons ,value (--> ,obj nil ,@attrs))
		,creator)))

(defmacro change--> (obj func init creator &rest attrs)
  `(setf ,obj (with-deep-update ,obj (list ,@attrs) 
				(funcall ,func (--> ,obj ,init ,@attrs)) 
				,creator)))

;(defclass wrap-entry-access () (
;  (obj :initarg :data)
;  (as-type :initarg :type)
;  ))
;
;(defmethod has-entry ((obj wrap-entry-access) attr)
;  (call-method (find-method #'has-entry nil (list (slot-value obj 'as-type) t))
;           (slot-value obj 'obj) attr))
;(defmethod get-entry ((obj wrap-entry-access) attr &optional (default nil))
;  (call-method (find-method #'get-entry nil (list (slot-value obj 'as-type) t t))
;           (slot-value obj 'obj) attr default))
;(defmethod with-updated-entry ((obj wrap-entry-access) attr value)
;  (call-method (find-method #'with-updated-entry nil (list (slot-value obj 'as-type) t t))
;           (slot-value obj 'obj) attr value))
;
;(defun list-as-seq (x)
;  (make-instance 'wrap-entry-access :data x :type 'common-lisp:sequence))

(defun join-values-and-names (vs ns)
  (iter
   (for v in vs)
   (for n in ns)
   (for sym := (intern (string-upcase n) :keyword))
   (collect `(,sym ,v))
   ))

(defun index-entry (s fs &key (test 'equal))
  (iter
   (with ht := (make-hash-table :test test))
   (for x in s)
   (push-> ht (iter (for f in fs) (collect (-> x f))) x)
   (finally (return ht))
   ))

(defmethod map-generic (obj fn)
  nil)

(defmethod map-generic ((obj sequence) fn)
  (map 'list fn obj))

(defmethod map-generic ((obj hash-table) fn)
  (let*
    (
     (res nil)
     )
    (maphash (lambda (k v) (push (funcall fn (list k v)) res)) obj)
    res))

(defmacro incf-> (obj key &optional (delta 1))
  `(set-> ,obj ,key (+ ,delta (-> ,obj ,key 0))))

(defmacro incf--> (obj delta creator &rest keys)
  `(setf ,obj (with-deep-update ,obj (list ,@keys) (+ ,delta (--> ,obj 0 ,@keys)) ,creator)))

(defun pos-by-entry
  (obj key value &key (test 'equal))
  (position-if
   (lambda (x) (funcall test (-> x key) value))
   obj))

(defun pos-by-getf-entry
  (obj key value &key (test 'equal))
  (position-if
   (lambda (x) (funcall test (getf x key) value))
   obj))

(defclass 
  entry-query () 
  (
   (key :initarg :key)
   (value :initarg :value)
   (processor :initarg :processor)
   ))

(defun kv-q (key value &key (processor 'identity))
  (make-instance 'entry-query
		 :key key
		 :value value
     :processor processor
     ))

(defmethod has-entry 
  ((obj list) (attr entry-query))
  (loop 
   with s := (gensym)
   for x in obj
   do
   (when 
    (equal
     (funcall (-> attr 'processor) (-> x (-> attr 'key) s))
     (-> attr 'value)))
   (return-from has-entry t)))

(defmethod get-entry
  ((obj list) (attr entry-query) &optional (default nil))
  (loop 
    with s := (gensym)
    for x in obj
    do
    (when 
      (equal
	(funcall (-> attr 'processor) (-> x (-> attr 'key) s))
	(-> attr 'value))
      (return-from get-entry x)))
  default)

(defmethod with-updated-entry
  ((obj list) (attr entry-query) value)
  (block nil
	 (loop 
	   with s := (gensym)
	   for x in obj
	   for n upfrom 0
	   do
	   (when 
	     (equal
	       (funcall (-> attr 'processor) (-> x (-> attr 'key) s))
	       (-> attr 'value))
	     (setf (elt obj n) value)
	     (return nil))))
  obj)

(defclass 
  entry-filter-query () 
  (
   (key :initarg :key)
   (value :initarg :value)
   (processor :initarg :processor)
   ))

(defun kv-f-q (key value &key (processor 'identity))
  (make-instance 'entry-filter-query
		 :key key
		 :value value
     :processor processor
     ))

(defmethod has-entry 
  ((obj list) (attr entry-filter-query))
  (loop 
   with s := (gensym)
   for x in obj
   do
   (when 
    (equal
     (funcall (-> attr 'processor) (-> x (-> attr 'key) s))
     (-> attr 'value)))
   (return-from has-entry t)))

(defmethod get-entry
  ((obj list) (attr entry-filter-query) &optional (default nil))
  (or
   (loop 
    with s := (gensym)
    for x in obj
    when 
    (equal
     (funcall (-> attr 'processor) (-> x (-> attr 'key) s))
     (-> attr 'value))
    collect x)
   default))

(defgeneric output-structure (obj &key stream prefix prefix-add))
(defmethod output-structure ((obj t) &key (stream t) (prefix "") (prefix-add " "))
  (format stream "~s" obj))
(defmethod output-structure ((obj string) &key (stream t) (prefix "") (prefix-add " "))
  (format stream "~s" obj))
(defmethod output-structure ((obj sequence) &key (stream t) (prefix "") (prefix-add " "))
  (format stream "[")
  (let 
   ((first t))
   (map 
    nil 
    (lambda 
     (x) 
     (unless first
             (format stream "~a" prefix))
     (output-structure 
      x :stream stream
      :prefix (concatenate 'string prefix-add prefix)
      :prefix-add prefix-add)
     (setf first nil)
     (format stream "~%")) 
    obj))
  (format stream "~a]" prefix)
  )
(defmethod output-structure ((obj hash-table) &key (stream t) (prefix "") (prefix-add " "))
  (format stream "{")
  (let 
   ((first t))
   (maphash
    (lambda 
     (k x) 
     (unless first
             (format stream "~a" prefix))
     (output-structure 
      k :stream stream 
      :prefix (concatenate 'string prefix-add prefix)
      :prefix-add prefix-add)
     (format stream ": ")
     (output-structure 
      x :stream stream
      :prefix (concatenate 'string prefix-add prefix)
      :prefix-add prefix-add)
     (setf first nil)
     (format stream "~%")) 
    obj))
  (format stream "~a}" prefix)
  )

(defclass access-mapper ()
  ((func :initarg :func)))
(defun mapper-q (f) (make-instance 'access-mapper :func f))
(defmethod has-entry ((obj sequence) (attr access-mapper)) t)
(defmethod has-entry ((obj t) (attr access-mapper)) t)
(defmethod get-entry ((obj sequence) (attr access-mapper) &optional (default nil))
  (map 'list (-> attr :func) obj))
(defmethod get-entry ((obj hash-table) (attr access-mapper) &optional (default nil))
  (let*
    ((ht (make-hash-table :test (hash-table-test obj))))
    (maphash
      (lambda (k v) (set-> ht k (funcall (-> attr :func) v)))
      obj)
    ht
    ))

(defclass access-filter ()
  ((func :initarg :func)))
(defun filter-q (f) (make-instance 'access-filter :func f))
(defmethod has-entry ((obj sequence) (attr access-filter)) t)
(defmethod get-entry ((obj sequence) (attr access-filter) &optional (default nil))
  (or (loop for x in obj when (funcall (-> attr 'func) x) collect x) default))

(defclass apply-query ()
  ((func :initarg :func)))
(defun apply-q (f) (make-instance 'apply-query :func f))
(defmethod has-entry ((obj t) (attr apply-query)) t)
(defmethod get-entry ((obj t) (attr apply-query) &optional (default nil))
  (or (funcall (-> attr 'func) obj) default))

(defmethod has-entry ((obj list) (attr access-mapper)) t)
(defmethod get-entry ((obj list) (attr access-mapper) &optional (default nil))
  (map 'list (-> attr 'func) obj))
(defmethod has-entry ((obj list) (attr access-filter)) t)
(defmethod get-entry ((obj list) (attr access-filter) &optional (default nil))
  (or (loop for x in obj when (funcall (-> attr 'func) x) collect x) default))
(defmethod has-entry ((obj list) (attr apply-query)) t)
(defmethod get-entry ((obj list) (attr apply-query) &optional (default nil))
  (or (funcall (-> attr 'func) obj) default))

(defmethod has-entry ((obj hash-table) (attr access-mapper)) t)
(defmethod get-entry ((obj hash-table) (attr access-mapper) 
  &optional (default nil))
  (let ((res (make-hash-table :test 'equal))) 
       (maphash (lambda (k v) (set-> res k (funcall (-> attr 'func) v))) obj)
       res))
(defmethod has-entry ((obj hash-table) (attr access-filter)) t)
(defmethod get-entry ((obj hash-table) (attr access-filter) 
  &optional (default nil))
  (let ((res (make-hash-table :test 'equal))) 
       (maphash (lambda (k v) (when (funcall (-> attr 'func) v) 
                                    (set-> res k v))) obj)
       res))
(defmethod has-entry ((obj hash-table) (attr apply-query)) t)
(defmethod get-entry ((obj hash-table) (attr apply-query)
  &optional (default nil))
  (or (funcall (-> attr 'func) obj) default))

(defclass getf-query ()
  ((key :initarg :key)))
(defun getf-q (k) (make-instance 'getf-query :key k))
(defmethod has-entry ((obj list) (attr getf-query)) 
  (getf obj (-> attr 'key)))
(defmethod get-entry ((obj list) (attr getf-query) &optional default) 
  (getf obj (-> attr 'key) default))
(defmethod with-updated-entry ((obj list) (attr getf-query) value)
  (setf (getf obj (-> attr 'key)) value)
  obj)

(defclass list-query ()
  ((keys :initarg :keys)))
(defun list-q (&rest k) (make-instance 'list-query :keys k))
(defmethod has-entry ((obj list) (attr list-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod has-entry ((obj hash-table) (attr list-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod has-entry ((obj t) (attr list-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod get-entry ((obj list) (attr list-query) &optional default)
  (mapcar (lambda (x) (-> obj x default)) (-> attr 'keys)))
(defmethod get-entry ((obj hash-table) (attr list-query) &optional default)
  (mapcar (lambda (x) (-> obj x default)) (-> attr 'keys)))
(defmethod get-entry ((obj t) (attr list-query) &optional default)
  (mapcar (lambda (x) (-> obj x default)) (-> attr 'keys)))
(defmethod with-updated-entry ((obj list) (attr list-query) (value list))
  (mapcar 
    (lambda (k v) (set-> obj k v))
    (-> attr 'keys)
    value)
  obj)
(defmethod with-updated-entry ((obj hash-table) (attr list-query) (value list))
  (mapcar 
    (lambda (k v) (set-> obj k v))
    (-> attr 'keys)
    value)
  obj)

(defclass or-query ()
  ((keys :initarg :keys)))
(defun or-q (&rest k) (make-instance 'or-query :keys k))
(defmethod has-entry ((obj list) (attr or-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod has-entry ((obj hash-table) (attr or-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod has-entry ((obj t) (attr or-query)) 
  (some (lambda (k) (has-entry obj k)) (-> attr 'keys)))
(defmethod get-entry ((obj list) (attr or-query) &optional default)
  (loop for k in (-> attr 'keys)
        when (has-entry obj k) return (-> obj k)
        finally (return default)))
(defmethod get-entry ((obj hash-table) (attr or-query) &optional default)
  (loop for k in (-> attr 'keys)
        when (has-entry obj k) return (-> obj k)
        finally (return default)))
(defmethod get-entry ((obj t) (attr or-query) &optional default)
  (loop for k in (-> attr 'keys)
        when (has-entry obj k) return (-> obj k)
        finally (return default)))
(defmethod with-updated-entry ((obj list) (attr list-query) (value t))
  (loop 
    for k in (-> attr 'keys)
    when (has-entry obj k) return (set-> obj k value)
    finally (set-> obj (first (-> attr 'keys)) value)
    )
  obj)
(defmethod with-updated-entry ((obj hash-table) (attr list-query) (value t))
  (loop 
    for k in (-> attr 'keys)
    when (has-entry obj k) return (set-> obj k value)
    finally (set-> obj (first (-> attr 'keys)) value)
    )
  obj)
(defmethod with-updated-entry ((obj t) (attr list-query) (value t))
  (loop 
    for k in (-> attr 'keys)
    when (has-entry obj k) return (set-> obj k value)
    finally (set-> obj (first (-> attr 'keys)) value)
    )
  obj)

(defun merge-assoc (dst src)
  (loop for x in (get-keys src)
	do (set-> dst x (-> src x)))
  dst)

(defmethod create-struct-index (l idx-field &key creator (ht-test 'equal))
  (let
    ((ht (funcall (or creator (lambda () (make-hash-table :test ht-test))))))
    (loop for x in (get-keys l)
	  do (set-> ht (--> l nil x idx-field) (-> l x))
	  )
    ht))

(defmethod create-struct-index ((l list) idx-field 
                                         &key creator (ht-test 'equal))
  (let
    ((ht (funcall (or creator (lambda () (make-hash-table :test ht-test))))))
    (loop for x in l
          do (set-> ht (-> x idx-field) x)
          )
    ht))

(defmethod group-struct-by (l idx-field &key creator (ht-test 'equal))
  (let
    ((ht (funcall (or creator (lambda () (make-hash-table :test ht-test))))))
    (loop for x in (get-keys l)
	  do (push-> ht (--> l nil x idx-field) (-> l x))
	  )
    ht))

(defmethod group-struct-by ((l list) idx-field 
                                         &key creator (ht-test 'equal))
  (let
    ((ht (funcall (or creator (lambda () (make-hash-table :test ht-test))))))
    (loop for x in l
          do (push-> ht (-> x idx-field) x)
          )
    (-> ht (mapper-q 'reverse))))

(defmethod join-struct-lists 
  (main-data lookup main-field idx-field
	     &key
	     (ht-test 'equal)
	     )
  (let*
    (
     (ht (create-struct-index lookup idx-field :ht-test ht-test))
     )
    (loop for x in (get-keys main-data)
	  collect
	  (merge-assoc (-> main-data x) 
		       (-> ht (--> main-data nil x main-field))))))

(defmethod join-struct-lists ((main-data list) lookup main-field idx-field
					       &key
					       (ht-test 'equal)
					       )
  (let*
    (
     (ht (create-struct-index lookup idx-field :ht-test ht-test))
     )
    (loop for x from 0 to (- (length main-data) 1)
	  collect
	  (merge-assoc (-> main-data x)
		       (-> ht (--> main-data nil x main-field))))))

(defmethod has-entry ((obj function) attr)
  (ignore-errors (progn (funcall obj attr) t)))
(defmethod get-entry ((obj function) attr &optional default)
  (block
    nil
    (handler-bind
      ((error (lambda (x) (return default))))
      (funcall obj attr))))

(defun to-getf-struct (obj)
  (reduce 'append (map-generic obj 'identity)))

