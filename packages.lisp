(defpackage 
  mccme-helpers/packages
  (:use :common-lisp :iterate)
  (:export alias-package package-file 
           file force-import merge-package-into
           proxy)
  )

(in-package :mccme-helpers/packages)

(defmacro alias-package (name alias)
  `(eval-when (:load-toplevel :execute :compile-toplevel)
              (rename-package 
               ',name ',name 
               (union 
                (list ',alias) 
                (package-nicknames ',name) 
                :test 'equalp))
              ))

(defmacro package-file (name &key (use nil) (export nil) (import nil))
  `(progn
    (defpackage ,name 
                (:use ,@use :mccme-helpers/packages :common-lisp)
                (:export ,@export))
    (in-package ,(symbol-name name))
    (eval-when (:compile-toplevel :load-toplevel :execute)
               ,@(loop for x in import collect `(import ',x))
               )
    ))

(defmacro file (&rest params)
  `(package-file ,@params))

(defmacro force-import (pkg &rest symbols)
  `(progn
    ,@(mapcar 
       (lambda (x) `(let*
                     ((s (find-symbol ,(symbol-name x) ',pkg)))
                     (unintern (find-symbol ,(symbol-name x)))
                     (import s)))
       symbols)))

(defun merge-package-into (source target
				  &key (external-only t)
				  verbose
				  )
  (let*
    (
     (spkg (find-package source))
     (tpkg (find-package target))
     )
    (when verbose
      (format *error-output* "~s ~s~%" spkg tpkg))
    (iterate
      (for symb in-package spkg :external-only t)
      (import symb tpkg)
      (export symb tpkg)
      )
    (unless external-only
      (iterate
	(for symb in-package spkg :external-only nil)
	(import symb tpkg)
	)
      )
    ))

(defmacro proxy (name &key source-packages shadowing-import)
  `(progn
    (eval-when (:compile-toplevel :load-toplevel :execute)
     (unless (find-package ,name) (package-file ,name)))
    ,@(loop for x in shadowing-import
	    collect `(shadowing-import ',x))
    ,@(loop for x in source-packages collect
            `(merge-package-into ,x ,name))
    ))
