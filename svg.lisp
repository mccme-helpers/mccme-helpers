(mccme-helpers/packages:file mccme-helpers/svg
  :use (:cl-emb
        :mccme-helpers/trivial
	:mccme-helpers/lisp
	:mccme-helpers/parsetype
        )
  :export (
           :svg-to-pdf
           :svg-emb-to-pdf
           :svg-multiline
           )
  )

(defun svg-to-pdf (output-dir name-local &key (dpi 300) output extra-deps)
 (let*
  (
   (name-local (if (listp name-local) name-local (list name-local)))
   (output (or output (concatenate 'string (cl-ppcre:regex-replace "[.]svg" (first name-local) "") ".pdf")))
  )
  (multiple-value-bind
   (stdout stderr exit-code)
   (uiop:run-program
    (format nil 
     "cd '~a' && ~{ cp  '~a' . &&  ~} rsvg-convert -f pdf -d ~a -p ~a ~{ '~a' ~} -o ~a "
     output-dir 
     extra-deps
     dpi dpi 
     name-local output
     )
    :ignore-error-status t)
   (progn stdout stderr nil)
   exit-code    
  )))

(defun svg-emb-to-pdf (template env output-dir-prefix &key (code "") extra-deps)
  (let*
    (
     (true-output-dir-template
        (format nil "~a/~a/~a/XXXXXXXX"
           output-dir-prefix
           (local-time:today)
           (local-time:now)))
     (true-output-dir
       (progn
         (ensure-directories-exist true-output-dir-template)
         (temp-dir-name true-output-dir-template)))
     (template (if (listp template) template (list template)))
     (svg-names
       (loop for k upfrom 1
             for entry in template 
             for el := (if (listp entry) entry (list entry))
             for x := (first el)
             for bx := (cl-ppcre:regex-replace ".*/" x "")
             for extra-env := (second el)
             for svg = (format nil "~a.~a.~4,'0d.svg" bx code k)
             for full-svg := (format nil "~a/~a" true-output-dir svg)
             do (with-open-file
                   (f full-svg 
                     :direction :output 
                     :if-exists :error :if-does-not-exist :create)
                   (let* ((*standard-output* f) 
                          (cl-emb::*emb-stream-redirection* "progn"))
                     (execute-emb (truename x) 
                        :env (append extra-env (list :svg-template-page k
                                          :svg-template-output-dir true-output-dir
                                           ) env))
                   )
                   )
             collect svg
             )
       )
     (output-name (format nil "~a.pdf" (first svg-names)))
     (full-output-name (format nil "~a/~a" true-output-dir output-name))
     (success (svg-to-pdf true-output-dir svg-names :output output-name 
                          :extra-deps extra-deps))
     )
   (values (if (= 0 success) full-output-name nil) success full-output-name)))

(defun svg-multiline (text l x dy &key (split-on `(#\Tab #\Space)) (emergency-split "-")
   extra-attributes)
 (cond 
  ((emptyp text) "")
  ((<= (length text) l) 
   (format nil "<tspan x=\"~a\" dy=\"~a\" ~a>~a</tspan>" 
    x dy (or extra-attributes "") (cl-emb::escape-for-xml text)))
  (t
   (let*
    (
     (split-point (loop for p downfrom (1- l) to 0 
                        for c := (elt text p)
                        for s := (find c split-on :test 'equal)
                   when s return p))
     (backup-split (- l (length emergency-split)))
    )
    (if split-point (concatenate 'string
                     (svg-multiline (subseq text 0 (1+ split-point)) l x dy)
                     (svg-multiline (subseq text (1+ split-point)) l x dy 
                                     :split-on split-on :emergency-split emergency-split))
                    
     (concatenate 'string
      (svg-multiline (concatenate 'string (subseq text 0 backup-split) emergency-split)
                     l x dy)
      (svg-multiline (subseq text backup-split) l x dy 
                      :split-on split-on :emergency-split emergency-split))
     )
    )
   )))
