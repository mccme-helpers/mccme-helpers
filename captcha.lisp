(mccme-helpers/packages:file mccme-helpers/captcha
  :use (
    mccme-helpers/trivial
    mccme-helpers/lisp
  )
  :export (
    #:generate-expression
    #:render-expression-html
    #:*captcha-bypass-password*
  )
)

(defvar *captcha-bypass-password* :none)

(def-and-set-var *legal-operators* '(+ - - *))
(def-and-set-var *legal-range* '(1 9))

(defun generate-expression (size)
  (let*
   ((op (random-of *legal-operators*))
    (val (apply 'random-between *legal-range*))
    (lhs (truncate (/ (- size 1) 2)))
    (rhs (- size 1 lhs))
    )
   (if 
    (> size 0)
    (list op (generate-expression lhs) (generate-expression rhs))
    val
    )
   ))

(defparameter *html-modifiers*
  '(:a :b :i :span :font)
  )

(defparameter *html-spoilers*
  '(
    "<span style='display:none'>[это не должно быть видно this should be hidden]</span>"
    "<span hidden style='display:none'>[ this should be hidden это не должно быть видно ]</span>"
    "<!-- -= > 2+3 -->"
    )
  )

(defun render-expression-html (expr)
  (let*
   ((modifier (random-of *html-modifiers*))
    (spoiler (random-of *html-spoilers*))
    )
   (if (listp expr)
       (format nil "<~a>~a(<~a>~A</~a> ~A ~A</~a>)" 
               modifier
               spoiler
               modifier
               (render-expression-html (second expr))
               modifier
               (first expr)
               (render-expression-html (third expr))
               modifier
               )
       (format nil "~a" expr)
       )
   ))
