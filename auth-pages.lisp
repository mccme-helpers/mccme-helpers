(mccme-helpers/packages:file mccme-helpers/auth-pages
  :use (mccme-helpers/db-auth
        mccme-helpers/cookie
        )
  :export (
           #:make-login-pages
           #:verify-login
           #:*su-allowed-p*
    )
  )

(mccme-helpers/packages:alias-package hunchentoot ht)

(defvar *su-allowed-p* (constantly nil))

(defun make-login-pages (login-template)
  (ht:define-easy-handler (login-form :uri "/user/login") (wrong)
                          (setf (ht:content-type*) "text/html; charset=utf-8")
                          (
                           #+sbcl sb-ext:string-to-octets 
                           #+ccl ccl:encode-string-to-octets
                           (cl-emb:execute-emb (pathname login-template) 
                                                                       :env `(:wrong ,wrong))
                                                   :external-format :utf-8))
  (ht:define-easy-handler (login-submit :uri "/user/submit_login") ()
                          (let* (
                                 (username (ht:post-parameter "login"))
                                 (password (ht:post-parameter "password"))
                                 (match (login-correct-p username password))
                                 (url (or (get-secure-cookie "url") "/"))
                                 )
                                (if match (progn 
                                           (set-secure-cookie "user" username)
                                           (set-secure-cookie "password" password)
                                           (ht:redirect url))
                                    (progn
                                     (with-open-file 
                                      (s "fail-login.log" 
                                         :if-does-not-exist :create 
                                         :if-exists :append 
                                         :direction :output)
                                      #+nil(when 
                                       nil
                                       (format s 
                                               "Failed login check, username [[~a]] password [[~a]].~%" 
                                               username password)))
                                     (set-secure-cookie "user" nil)
                                     (set-secure-cookie "password" nil)
                                     (ht:redirect "/user/login?wrong=1")
                                     ))
                                ))

  (ht:define-easy-handler (user-logout :uri "/user/logout") ()
                          (set-secure-cookie "user"     nil)
                          (set-secure-cookie "su_user"  nil)
                          (set-secure-cookie "password" nil)
                          (set-secure-cookie "url"      nil)
                          (ht:redirect "/"))

  (ht:define-easy-handler (user-su :uri "/user/su") (to)
                          (verify-login)
                          (unless (get-secure-cookie "su_user")
                                  (set-secure-cookie "su_user" 
                                                     (get-secure-cookie "user")))
                          (set-secure-cookie "user" to)
                          (when (equal (get-secure-cookie "su_user")
                                       (get-secure-cookie "user"))
                                (set-secure-cookie "su_user" nil))
                          "You tried to su"
                          )
  )

(defun verify-login (&key known-stored-hash known-stored-hash-su)
  (let* (
         (username (get-secure-cookie "user"))
         (password (get-secure-cookie "password"))
         (match (login-correct-p username password 
				 :known-stored-hash known-stored-hash))
         (url (and (boundp 'ht:*request*) (ht:request-uri*)))
         (su-username (get-secure-cookie "su_user"))
         (su-match (login-correct-p su-username password 
				    :known-stored-hash known-stored-hash-su))
         (su-allowed (and
                      su-match
                      (boundp '*su-allowed-p*)
                      (functionp *su-allowed-p*)
                      (funcall *su-allowed-p* su-username username)
                      ))
         )
        (when (and (not match) (not su-allowed))
              (set-secure-cookie "url" url)
              (ht:redirect "/user/login"))))

