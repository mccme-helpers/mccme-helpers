(mccme-helpers/packages:file mccme-helpers/web-gen-pdf
  :use (
        mccme-helpers/emb-helpers
        mccme-helpers/ht
        mccme-helpers/data-transformer
        mccme-helpers/lisp
        mccme-helpers/csv
        mccme-helpers/trivial
        mccme-helpers/cookie
        mccme-helpers/struct
        mccme-helpers/auth-pages
        mccme-helpers/tex
        :iterate
        )
  :export (
           #:make-pdf-web-form
    )
  )

(defmacro 
  make-pdf-web-form
  (handler-symbol url-base page-template tex-template file-name
                  file-field-name view-log-field-name
                  data-set-name entry-names
                  &key
                  (force-field-names nil) (extra-entry-names nil)
                  (extra-data-set-name :extra-data) (temp-tex-dir "tex/"))
  `(progn
    (define-emb-page 
     (,(concatenate-symbol-names handler-symbol :-request) 
       ,(concatenate 'string url-base "/request") ,page-template)
     )
    (hunchentoot:define-easy-handler
     (,(concatenate-symbol-names handler-symbol :-submit)
       :uri ,(concatenate 'string url-base "/submit")
      )
     ()
     (mccme-helpers/functional:backtraceable
      (verify-login)
      (let*
       (
        (schema 
         `(
           ((:code-name ,,file-field-name)
            (:type :string)
            (:html-input-type :file)
            )
           ((:code-name ,,view-log-field-name)
            (:type :bool)
            (:html-input-type :checkbox)
            )
           ))
        (dt (data-transformer schema))
        (dummy1 (transformer-http-post-collect dt))
        (csv-content
         (multiple-value-list
          (load-csv-file-with-headers 
           (-> dt ,file-field-name) ,(if extra-entry-names 1 0)
           ,(if force-field-names 0 1) 0 :force-field-names ,force-field-names
           ,@(when extra-entry-names (list :extra-data-position 1))
           )
          ))
        (hts (first csv-content))
        (hted (second csv-content))
        (data `(,,data-set-name
                ,(iterate (for ht :in hts) 
                          (collect
                           (iterate
                            (for x in ,entry-names)
                            (when (-> ht (first x))
                                  (collect (second x))
                                  (collect (-> ht (first x)))
                                  )
                            )
                           ))
                ,,extra-data-set-name
                ,(iterate
                  (for x in ,extra-entry-names)
                  (when (-> hted (first x))
                        (collect (second x))
                        (collect (-> hted (first x)))
                        )
                  )
                ))
        (pdf-name 
         (third
          (multiple-value-list
           (tex-to-pdf ,tex-template data ,temp-tex-dir))))
        )
       (declare
        (ignore dummy1))
       (set-secure-cookie "target-pdf-file" pdf-name)
       (if 
         (-> dt ,view-log-field-name)
         (ht:redirect ,(concatenate 'string url-base "/" file-name ".log"))
         (ht:redirect ,(concatenate 'string url-base "/" file-name))
         )
       )
      ))
    (hunchentoot:define-easy-handler
     (,(concatenate-symbol-names handler-symbol :-pdf)
       :uri ,(concatenate 'string url-base "/" file-name)
      )
     ()
     (mccme-helpers/functional:backtraceable
      (verify-login)
      (let* 
       (
        (target-pdf-file (get-secure-cookie "target-pdf-file"))
        )
       (set-secure-cookie "target-pdf-file" nil)
       (setf (ht:content-type*) "application/x-pdf")
       (setf (ht:header-out "Content-Disposition") (format nil "attachment; filename=\"~a\"" ,file-name))
       (file-contents target-pdf-file)
       )
      ))
    (hunchentoot:define-easy-handler
     (,(concatenate-symbol-names handler-symbol :-log)
       :uri ,(concatenate 'string url-base "/" file-name ".log")
      )
     ()
     (mccme-helpers/functional:backtraceable
      (verify-login)
      (let* 
       (
        (target-pdf-file (get-secure-cookie "target-pdf-file"))
        )
       (set-secure-cookie "target-pdf-file" nil)
       (setf (ht:content-type*) "text/plain; charset=cp1251")
       (file-contents (cl-ppcre:regex-replace-all "[.]pdf$" target-pdf-file ".log"))
       )
      ))
    ))
