(mccme-helpers/packages:file mccme-helpers/db-auth
  :use (mccme-helpers/basic-db
        :iterate
        )
  :export (
           #:login-correct-p
           #:*db-auth-schema*
           #:create-user
           #:create-user-case-insensitive
           #:edit-user
           #:login-to-user-id
    )
  )

(defun login-correct-p (username password &key known-stored-hash)
  #.(sql-on)
  (and 
    username
    password
    (let* (
           (db-stored-hash
	     (unless
	       known-stored-hash
	       (with-db
                 (select-with-marked-fields
                    [password]
                    [login]
                    :from [account]
                    :where [= [lower [account login]]
                              (string-downcase username)]))))
           (db-stored-hash
             (cond
               ((= (length db-stored-hash) 1)
                 (mccme-helpers/struct::-> (car db-stored-hash) :password))
               (db-stored-hash
                 (mccme-helpers/struct::->
                   (find-if 
                     (lambda (x)
                       (equal (mccme-helpers/struct::-> x :login) username)) db-stored-hash)
                   :password))
               (t nil)))
	   (stored-hash 
	     (or
	       known-stored-hash
               db-stored-hash))
	   (match (and password stored-hash (mccme-helpers/password:password-matches-p password stored-hash)))
	   )
      match
      ))

  #.(sql-off))

(defvar *db-auth-schema*)
(setf *db-auth-schema* `(
  (account (
            (login varchar (255) unique)
            (password varchar (512))
            (email varchar (255))
            ))
  ))

  #.(sql-on) 
(defun create-user (username &optional password email &rest args)
  (with-db 
   (clsql:insert-records 
    :into [account]                            
    :av-pairs `(
                (login ,username)
                (password ,(when password 
                                 (mccme-helpers/password:hash-password
                                  "sha512"
                                  username
                                  password)))
                (email ,email)
                ,@(iter (generate x in args) 
                        (collect (list 
                                  (sql-value :attribute 
                                             (next x)) 
                                  (next x))))))))

(defun create-user-case-insensitive (username &rest args)
  (with-db
   (let*
    (
     (lowlevel-ok (ignore-errors (progn (apply 'create-user username args) t)))
     (icase-ok (equal 
                (caar 
                 (clsql:select 
                  [count [id]]
                  :from [account] 
                  :where [= [lower [login]] 
                            (string-downcase username)]))
                1))
     )
    (cond
     ((not lowlevel-ok) nil)
     ((not icase-ok)
      (progn
       (clsql:delete-records :from [account] :where [= [login] username])
       nil))
     (t t)
     )
    )
   ))

(defun login-to-user-id (user)

  (and user
    (let* (
           (db-data
	       (with-db
                 (select-with-marked-fields
                    [id]
                    [login]
                    :from [account]
                    :where [= [lower [account login]]
                              (string-downcase user)])))
            )
             (cond
               ((= (length db-data) 1)
                 (mccme-helpers/struct::-> (car db-data) :id))
               (db-data
                 (mccme-helpers/struct::->
                   (find-if 
                     (lambda (x)
                       (equal (mccme-helpers/struct::-> x :login) user)) db-data)
                   :id))
               (t nil)))))
#.(sql-off)

(defun edit-user (username &optional password email &rest args)
  #.(sql-on) 
  (with-db 
   (clsql:update-records 
    [account]   
    :where [= [login] username]
    :av-pairs `(
                (login ,username)
                ,@(if (and (not (equal password "")) 
			  (not (equal password nil)))  
			(list (list 'password (when password 
                                 (mccme-helpers/password:hash-password
                                  "sha512"
                                  username
                                  password)))))
                 ,@(cond 
                       ((equal email "")
                        (list (list 'email nil)))
                       ((not (equal email nil))
                        (list (list 'email email))))
                ,@(iter (generate x in args) 
                        (collect (list 
                                  (sql-value :attribute 
                                             (next x)) 
                                  (next x)))))))
  #.(sql-off))

