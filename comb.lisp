(mccme-helpers/packages:file mccme-helpers/comb
  :use (
        :mccme-helpers/struct
        :mccme-helpers/functional
	:mccme-helpers/trivial

        :iterate
        )
  :export (
           #:aggregate-list
           #:direct-product
           #:uniq
           #:uniq-keyed
           #:uniq-stable
           #:uniq-stable-keyed
           #:uniq-stable-custom
	   #:transitive-closure
	   #:recursive-modification
	   #:list-to-table
     #:table-totals-2d
     #:group-counts

     #:verhoeff
     #:verhoeff-check
     #:with-verhoeff
     #:string-verhoeff
     #:string-verhoeff-check
     #:string-with-verhoeff

     #:okpo

     #:list-unroll
    )
  )

; raw data - list of lists of ...
; rule-list - depth-ordered list of rules
;   first element: position of split between key and value
;   second element: whether key should be car'ed 
;   third element: whether value should be car'ed
;   fourth element: whether we should push to list instead of setting
(defun aggregate-list (raw-data rule-list &key (test 'equalp))
  (let*
   (
    (result (make-hash-table :test test))
    )
   (iter
    (for el in raw-data)
    (iter 
     (for rule on rule-list)
     (for considered-hashtable initially result then next-hash)
     (for rest-data initially el then val-subseq)
     (for key-subseq := (subseq rest-data 0 (caar rule)))
     (for val-subseq := (subseq rest-data (caar rule)))
     (for key := (if (second (car rule)) (car key-subseq) key-subseq))
     (for val := (if (third  (car rule)) (car val-subseq) val-subseq))
     (if
      (null (cdr rule))
      (if (null (fourth (car rule)))
	(set-> considered-hashtable key val)
	(push-> considered-hashtable key val)
	)
      (unless (-> considered-hashtable key)
              (set-> considered-hashtable 
                     key (make-hash-table :test test)))
      )
     (for next-hash := (-> considered-hashtable key))
     )
    )
   result))

(defun direct-product (&rest lists)
  (if lists
      (let (res)
           (loop for x in (car lists) do
                 (loop for xs in (apply 'direct-product (cdr lists)) do
                       (push (cons x xs) res)
                       )
                 )
           res
           )
      '(nil)
      ))

(defun uniq (l &optional (test 'equal))
  (let* (
         (ht (make-hash-table :test test))
         res
         )
        (loop for x in l do (setf (gethash x ht) t))
        (maphash (lambda (k v) (declare (ignore v)) (push k res)) ht)
        res
        ))

(defun uniq-stable (l &optional (test 'equal))
  (let* (
         (ht (make-hash-table :test test))
         res
         )
        (loop for x in l 
	      do (unless (gethash x ht) (push x res))
	      do (setf (gethash x ht) t))
        (reverse res)
        ))

(defun uniq-stable-keyed (l key &optional (test 'equal))
  (let* (
         (ht (make-hash-table :test test))
         res
         )
        (loop for x in l 
              for k := (funcall key x)
              do (unless (gethash k ht) (push x res))
              do (setf (gethash k ht) t))
        (reverse res)
        ))

(defun uniq-keyed (l &key (test 'equal) (key 'identity) (aggregate nil) 
		     use-initial initial-element)
  (let* (
	 (ht (make-hash-table :test test))
	 res
	 )
    (loop 
      for x in l do 
      (if aggregate
	(let*
	  ((k (funcall key x)))
	  (multiple-value-bind
	    (value present)
	    (gethash k ht)
	    (let*
	      (
	       (has-value (or present use-initial))
	       (old-value (if present value initial-element))
	       (v (if has-value (funcall aggregate x old-value) x))
	       )
	      (setf (gethash k ht) v)
	      )))
	(setf (gethash (funcall key x) ht) x)))
    (maphash (lambda (k v) (declare (ignore k)) (push v res)) ht)
    res
    ))

(defun uniq-stable-custom (l &key (key 'identity) (test 'equal))
  (let* (
	 res
	 keys
	 )
    (loop for x in l 
	  for k := (funcall key x)
	  do (unless (find k keys :test test) 
	       (push x res) (push k keys))
	  )
    (reverse res)
    ))

(defun transitive-closure (l dep-fn &optional (test 'equal))
  (uniq-stable
    (apply 
      'concatenate 'list
      (mapcar 
	(lambda (x) 
	  (concatenate 
	    'list
	    (transitive-closure (funcall dep-fn x) dep-fn test)
	    (list x)))
	l))))

(defun recursive-modification (tree pred fn)
  (mapcar
    (lambda (x)
      (cond
	((funcall pred x) (funcall fn x))
	((consp x) (recursive-modification x pred fn))
	(t x)
	)
      )
    tree))

(defun list-to-table 
  (data keys value-key
	&key
	(mk-level (constantly nil))
	(less-than 'uni-<)
	)
  (iter
    (with data-table := (funcall mk-level))
    (with key-lists :=
	  (progn
	  (make-array (length keys)
		      :initial-element nil)))
    (for entry in data)
    (for value := (-> entry value-key))
    (for key-set-raw :=
	 (mapcar
	   (lambda (x) (-> entry x))
	   keys))
    (for key-set :=
	 (mapcar
	   (lambda (x) (format nil "~a" x))
	   key-set-raw))
    (setf data-table
	  (with-deep-update
	    data-table
	    key-set
	    value 
	    mk-level))
    (iter
      (for n upfrom 0)
      (for val in key-set-raw)
      (push-> key-lists  n val)
      )
    (finally
      (return
	(append
	  (list data-table)
	  (map 'list
	       (lambda (x) 
		 (mapcar 
		   (lambda (y)
		     (format nil "~a" y))
		   (sort
		     (uniq x)
		     less-than
		     )))
	       key-lists
	       )
	  )))
    ))

(defun table-totals-2d
  (table range params group-by)
  (iter
   (with total-table := (make-hash-table :test 'equal))
   (for k in params)
   (set-> total-table k
          (apply 
           '+
           (mapcar
            (cond
             ((eq group-by :first)
              (lambda (x) (--> table 0 x k)))
             ((eq group-by :second)
              (lambda (x) (--> table 0 k x)))
             (t (constantly 0)))
            range
            )
           ))
   (finally 
    (return
     total-table
     ))
   )
  )

(defun group-counts (l &key (key 'identity) (weight (constantly 1)))
  (let*
    (
     (ht (make-hash-table :test 'equal))
     )
    (loop for x in l do (incf-> ht (funcall key x) (funcall weight x)))
    (map-generic ht 'identity)))

(defparameter verhoeff-d
  `(
    (0 1 2 3 4 5 6 7 8 9)
    (1 2 3 4 0 6 7 8 9 5)
    (2 3 4 0 1 7 8 9 5 6)
    (3 4 0 1 2 8 9 5 6 7)
    (4 0 1 2 3 9 5 6 7 8)
    (5 9 8 7 6 0 4 3 2 1)
    (6 5 9 8 7 1 0 4 3 2)
    (7 6 5 9 8 2 1 0 4 3)
    (8 7 6 5 9 3 2 1 0 4)
    (9 8 7 6 5 4 3 2 1 0)
    ))

(defparameter verhoeff-p
  `(
    (0 1 2 3 4 5 6 7 8 9)
    (1 5 7 6 2 8 3 0 9 4)
    (5 8 0 3 7 9 6 1 4 2)
    (8 9 1 6 0 4 3 5 2 7)
    (9 4 5 3 1 2 6 8 7 0)
    (4 2 8 6 5 7 3 9 0 1)
    (2 7 9 3 8 0 6 4 1 5)
    (7 0 4 6 9 1 3 2 5 8)
    ))

(defparameter verhoeff-inv `(0 4 3 2 1 5 6 7 8 9))

(defun verhoeff (arr &optional (shift 0))
  (loop
    with c = 0
    for i upfrom 1
    for n in (reverse arr)
    do (setf c (--> verhoeff-d 0 c (--> verhoeff-p 0 (mod (+ i shift) 8) n)))
    finally (return (-> verhoeff-inv c))
    ))

(defun verhoeff-check (arr) (equal (verhoeff arr -1) 0))

(defun with-verhoeff (arr) (append arr (list (verhoeff arr))))

(defun string-verhoeff (str &optional (shift 0)) 
  (format 
    nil "~a" 
    (verhoeff (map 'list (lambda (ch) 
			   (- (char-code ch) 48)) str)
	      shift)))

(defun string-with-verhoeff (str)
  (concatenate 'string str (string-verhoeff str)))

(defun string-verhoeff-check (str)
  (equal "0" (string-verhoeff str -1)))

(or
  (and
    (equal (verhoeff '(7 5 8 7 2 )) 2) 
    (equal (verhoeff '(7 5 8 7 2 2 ) -1) 0) 
    (equal (verhoeff '(1 2 3 4 5 )) 1) 
    (equal (verhoeff '(1 2 3 4 5 1 ) -1) 0) 
    (equal (verhoeff '(1 4 2 8 5 7 )) 0) 
    (equal (verhoeff '(1 4 2 8 5 7 0 ) -1) 0) 
    (equal (verhoeff '(1 2 3 4 5 6 7 8 9 0 1 2 )) 0) 
    (equal (verhoeff '(1 2 3 4 5 6 7 8 9 0 1 2 0 ) -1) 0) 
    (equal (verhoeff '(8 4 7 3 6 4 3 0 9 5 4 8 3 7 2 8 4 5 6 7 8 9 )) 2) 
    (equal (verhoeff '(8 4 7 3 6 4 3 0 9 5 4 8 3 7 2 8 4 5 6 7 8 9 2 ) -1) 0) 
    (equal (with-verhoeff '(1 2 3 4 5)) '(1 2 3 4 5 1))
    (verhoeff-check '(1 2 3 4 5 1))
    (not (verhoeff-check '(1 2 2 4 5 1)))
    (not (verhoeff-check '(1 2 8 4 5 1)))

    (equal (string-verhoeff "75872") "2")
    (equal (string-verhoeff "758722" -1) "0")
    (equal (string-with-verhoeff "12345") "123451")
    (string-verhoeff-check "123451")
    (not (string-verhoeff-check "122451"))
    )
  (error "Verhoeff checksum testing failure"))

(defun okpo (arr)
  (let*
    (
     (first-cs (mod (loop for k upfrom 0 for n in arr sum (* (1+ (mod k 10)) n)) 11))
     (second-cs (mod (loop for k upfrom 2 for n in arr sum (* (1+ (mod k 10)) n)) 11))
     )
    (cond 
      ((< first-cs 10) first-cs)
      ((< second-cs 10) second-cs)
      (t 0)
      )
    ))

(or
  (and
    (= (okpo '( 0 9 8 0 8 1 1 )) 7)
    (= (okpo '( 3 8 2 7 0 9 5 )) 1)
    )
  (error "OKPO checksum testing failure"))

(defun list-unroll (l)
  (let
    ((res nil)
     (lf (find-if (lambda (x) (and (listp x) x)) l)))
    (if lf
      (loop
	for n upfrom 0
	for d in lf
	do (push (loop for e in l
		       collect
		       (if (listp e)
			 (nth n e)
			 e)) res))
      (push l res)
      )
    (reverse res)))
