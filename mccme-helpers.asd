(pushnew :clsql-uffi *features*)

(asdf:defsystem :mccme-helpers
 :name "mccme-helpers"
 :depends-on
 (
  :cl-utilities
  :iterate
  :yason
  :cl-ppcre
  :parse-number
  :cl-emb
  :clsql
  :hunchentoot
  :ironclad
  :trivial-backtrace
  :local-time
  :fare-csv
  :trivial-utf-8
  #+sbcl :sb-posix
 )
 :components (
	 (:file "packages")
	 (:file "lisp" :depends-on ("packages"))
	 (:file "struct" :depends-on ("packages"))
	 (:file "trivial" :depends-on ("packages"))
	 (:file "functional" :depends-on ("packages"))
	 (:file "csv" 
	   :depends-on ("struct" "trivial" "functional" "packages"))
	 (:file "captcha"
	   :depends-on ("trivial" "lisp" "packages"))
	 (:file "parsetype" :depends-on ("packages" "lisp" "struct" "trivial"))
	 (:file "tex" :depends-on ("packages" "trivial" "lisp" "parsetype"))
	 (:file "password" :depends-on ("packages" "parsetype"))
	 (:file "cookie"
	   :depends-on ("parsetype"
	                "password"
			"packages"
	                ))
	 (:file "basic-db"
	   :depends-on ("struct" "packages" "lisp" "trivial"))
	 (:file "db-auth"
	   :depends-on ("basic-db" "packages" "password"))
	 (:file "auth-pages"
	   :depends-on ("db-auth" "cookie" "packages"))
	 (:file "emb-helpers"
	   :depends-on ("functional" "packages"))
	 (:file "ht"
	   :depends-on ("emb-helpers" "auth-pages" "packages"))
	 (:file "data-transformer"
	   :depends-on 
	   ("basic-db" "struct" "parsetype" "packages" "functional"
	    "ht" "trivial"))
	 (:file "db-versioning"
	   :depends-on ("data-transformer" "struct" 
	     "lisp" "basic-db" "packages" "trivial"))
	 (:file "comb"
	   :depends-on ("struct" "functional" "packages"))
	 (:file "dmtx" :depends-on ("trivial" "packages"))
	 (:file "regions" :depends-on ("packages"))
	 (:file "web-gen-pdf" :depends-on 
	   (
	     "emb-helpers"
	     "ht"
	     "data-transformer"
	     "lisp"
	     "csv"
	     "trivial"
	     "cookie"
	     "struct"
	     "packages"
	     "tex"
	     ))
	 (:file "rdf" :depends-on ("packages"))
	 (:file "svg" :depends-on ("packages" "trivial" "lisp" "parsetype"))
	 ))
