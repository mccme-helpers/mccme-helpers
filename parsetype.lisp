(mccme-helpers/packages:file mccme-helpers/parsetype 
  :use(
    :iterate
    :cl-ppcre
    :mccme-helpers/lisp
    :mccme-helpers/trivial
    :mccme-helpers/struct
    )
  :export (
    #:base16-string-to-octet-array 
    #:octet-array-to-base16-string
    #:valid-date
    #:date-to-russian
    #:date-to-ddmmyyyy
    #:date-to-ddmmyy
    #:date-from-russian

    #:capitalize-words

    #:escape-unicode-long
    #:escape-unicode-bmp
    #:escape-for-regex
    #:escape-for-url
    #:unescape-from-url

    #:current-reporting-year

    #:float-to-string

    #:integer-to-digits
    #:*digit-symbols*

    #:capitalize
    #:symbol-to-camelcase-string
    #:list-to-ht

    #:value-to-string

    #:clean-gender

    #:surrounded

    #:string-into-matches
    #:sort-strings

    #:naive-kill-tags

    #:simplify-known-unicode
    #:clean-for-unibyte-encoding

    #:trimmer
    #:trim-whitespace

    #:fix-glaring-capitalization

    #:recast-string-encoding
    #:string-to-octets
    #:octets-to-string

    #:email-synonyms
    
    #:encode-utf16-as-dash-separated-hex
    #:encode-list-as-dash-separated-hex
    
    #:valid-iso-date-string-p

    #:transliterate-to-latin
    #:kill-filename-unsafe-chars
    #:string-to-struct
    #:struct-to-string

    #:truncate-string

    #:*multibyte-encodings*

    #:plist-list
    #:plist-json
    #:list-json
    )
  )

(defun base16-string-to-octet-array (s)
  (let*
    (
      (pairs (iter
        (generate x :in-string s)
        (collect (list (next x) (next x)))))
      (pair-strings (mapcar
        (lambda (p)
          (map 'string 'identity p))
        pairs))
      (octet-array (make-array (length pair-strings) :element-type '(unsigned-byte 8)))
    )
    (iter
      (for p in pair-strings)
      (for n from 0)
      (setf (aref octet-array n) (parse-integer p :radix 16)))
    octet-array))

(defun octet-array-to-base16-string (a)
  (let*
    (
      (strings (map 'list
        (lambda (x) (format nil "~16,2,'0r" x))
        a))
      (full-string (apply 'concatenate 'string strings))
    )
    full-string))

(defun valid-date (year month day)
  (and
   (integerp year)
   (integerp month)
   (integerp day)
   (<= 1900 year 9999)
   (<= 1 month 12)
   (<= 1 day 31)
   (equal (list 0 0 0 day month year )
          (subseq 
           (multiple-value-list 
            (decode-universal-time 
             (encode-universal-time 
              0 0 0 day month year )))
           0 6
           ))))

(defvar *month-names-genitive*)
(Setf *month-names-genitive* `(
  "января"
  "февраля"
  "марта"
  "апреля"
  "мая" 
  "июня"
  "июля"
  "августа"
  "сентября"
  "октября"
  "ноября"
  "декабря"
  ))

(defun date-to-russian (str)
  (when (> (length str) 0)
        (let* (
               (split-data (cl-utilities:split-sequence #\- str))
               (year (parse-integer (first split-data)))
               (month (parse-integer (second split-data)))
               (day (parse-integer (third split-data)))
               (month-str (nth (- month 1) *month-names-genitive*))
               (result (format nil "~a ~a ~a г." day month-str year))
               )
              result)))

(defun date-from-russian (str)
  (when (> (length str) 0)
    (let*
      (
       (split-str (cl-ppcre:split " " str))
       (year-part-rx "^[^0-9а-яА-Я]*([0-9]{4})($|[^0-9]).*")
       (day-part-rx "^[^0-9а-яА-Я]*([0-9]{1,2})($|[^0-9]).*")
       (year-part (find-if (lambda (x) (cl-ppcre:scan year-part-rx x)) split-str))
       (month-parts (loop for x in *month-names-genitive* collect (subseq x 0 3)))
       (month-parts (mapcar (lambda (x) (cl-ppcre:regex-replace-all "мая" x "ма[яй]")) month-parts))
       (month-regex (format nil "^[^0-9а-яА-Я]*(~{~a~#[~:;|~]~}).*" month-parts))
       (month-part (find-if (lambda (x) (cl-ppcre:scan month-regex x)) split-str))
       (day-part (find-if (lambda (x) (cl-ppcre:scan day-part-rx x)) split-str))
       (month-line (cl-ppcre:regex-replace-all month-regex month-part "\\1"))
       (month-line (if (equal month-line "май") "мая" month-line))
       (month-idx
         (when month-line
           (position-if
             (lambda
               (x)
               (cl-ppcre:scan (format nil "^~a" month-line) x)
               )
             *month-names-genitive*
             )
           )
         )
       (month (when month-idx (1+ month-idx)))
       )
      (and
        year-part
        month
        day-part
        (format 
          nil "~4,'0d-~2,'0d-~2,'0d"
          (parse-integer (cl-ppcre:regex-replace-all year-part-rx year-part "\\1"))
          month
          (parse-integer (cl-ppcre:regex-replace-all day-part-rx day-part "\\1"))
          )
        )
      )
    )
  )

(defun date-to-ddmmyyyy (Str)
  (when str
        (let* ((split-data (cl-utilities:split-sequence #\- str)))
              (format nil "~2,'0d.~2,'0d.~4,'0d" 
                      (third split-data) (second split-data) 
                      (first split-data)))))

(defun date-to-ddmmyy (Str)
  (when str
        (let* ((split-data (cl-utilities:split-sequence #\- str)))
              (format nil "~2,'0d.~2,'0d.~2,'0d" 
                      (third split-data) (second split-data) 
                      (subseq (format nil "~a" (first split-data)) 2)
                      ))))

(defun capitalize-words (x)
  (apply 'concatenate 'string
  (iter (for w in (cl-utilities:split-sequence #\Space x))
        (for f initially nil then t)
        (if f (collect " "))
        (collect
         (if (> (length w) 1)
          (concatenate 'string (string-upcase (subseq w 0 1)) (string-downcase (subseq w 1)))
          (string-upcase w)
          ))
        )))

(defun escape-char-unicode-octochar (chr)
  (format nil "\\U~8,'0x" (char-code chr)))
(defun escape-char-unicode-quadchar (chr)
  (if (<= (char-code chr) #xffff)
    (format nil "\\u~4,'0x" (char-code chr))
    (string chr)))
(defun escape-unicode-long (str)
  (apply 'concatenate 'string (map 'list 'escape-char-unicode-octochar str)))
(defun escape-unicode-bmp (str)
  (apply 'concatenate 'string (map 'list 'escape-char-unicode-quadchar str)))

(defun escape-for-regex (str)
  (cl-ppcre:regex-replace-all 
   "\\[\\^]"
   (cl-ppcre:regex-replace-all 
    "\\\\"
    (cl-ppcre:regex-replace-all "(.)" str "[\\1]")
    "\\\\\\\\"
    )
   "\\^"
   ))

(defun escape-for-url (str)
  (let*
   (
    (bs (trivial-utf-8:string-to-utf-8-bytes str))
    (chunks (iter (for b in-vector bs)
                  (collect
                   (if
                    (or
                     (<= 48 b 57)
                     (<= 65 b 90)
                     (<= 97 b 122)
                     )
                    (string (code-char b))
                    (format nil "%~2,'0x" b)
                    ))))
    (res (apply 'concatenate 'string chunks))
    )
   res
   ))
(defun unescape-from-url (str)
  (let* 
   (
    (bs (iter (generate c in-string str) 
              (while (next c))
              (if (equal c #\%) 
                  (collect (parse-integer 
                            (map 'string 'identity 
                                 (list (next c) (next c)))
                            :radix 16))
                  (collect (char-code c)))
              ))
    (res (trivial-utf-8:utf-8-bytes-to-string 
          (map 'vector 'identity bs)))
    )
   res
   ))

(defun current-reporting-year (&optional (new-year-month 1) (new-year-day 1))
  (local-time::with-decoded-timestamp
   (:year year :month month :day day)
   (local-time::today)
   (+
    year
    (if
     (or
      (< month new-year-month)
      (and
       (= month new-year-month)
       (< day new-year-day)
       )
      )
     -1
     0
     ))
   ))

(defun float-to-string (f)
  (if f 
    (regex-replace-all 
      "[.]"
      (regex-replace-all
        "([.]0)?([d]0)?$"
        (format nil "~a" f)
        ""
        )
      ","
      )
    ""))

(defun integer-to-digits (n &optional (radix 10))
  (assert (integerp n))'
  (assert (>= n 0))
  (or
    (reverse
      (iter
        (for x initially n then (truncate (/ x radix)))
        (while (> x 0))
        (collect (mod x radix))
        )
      )
    (list 0)
    )
  )

(def-and-set-var 
  *digit-symbols*
  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")

(defun capitalize (s)
  (if (> (length s) 0)
    (concatenate
      'string
      (string-upcase (string (elt s 0)))
      (subseq s 1))
    s))

(defun fix-glaring-capitalization (s)
  (cond
    ((=(length s) 0) "")
    ((cl-ppcre:scan "^[а-яa-zё][а-яa-zё ]+($|-)" s)
     (concatenate 'string (string-upcase (subseq s 0 1))
                  (subseq s 1)))
    ((cl-ppcre:scan "^[А-ЯA-ZЁ][А-ЯA-ZЁ]" s)
     (concatenate 'string (string-upcase (subseq s 0 1))
                  (string-downcase (subseq s 1))))
    (t s)))

(defun symbol-to-camelcase-string (x)
  (let*
    (
     (s (split "-" (string-downcase (symbol-name x))))
     (st (cdr s))
     (stcc (mapcar 'capitalize st))
     )
    (if 
      s
      (format nil "~a~{~a~}" (car s) stcc)
      "")))

(defun list-to-ht (x)
  (let ((h (make-hash-table :test 'equal)))
    (map-generic x 
                 (lambda (k) 
                   (set-> h 
                          (cond
                            ((stringp (first k))
                            (first k))
                            ((symbolp (first k)) 
           (symbol-to-camelcase-string (first k)))
          (t (format nil "~a" (first k)))
                            )
                          (cond
         ((eq t (second k)) "1")
         ((eq nil (second k)) "0")
         ((symbolp (second k)) 
          (symbol-to-camelcase-string (second k)))
         (t (second k))
         )
        ))) h))

(defun value-to-string (x)
  (cond
    ((null x) "")
    ((stringp x) x)
    ((floatp x) (float-to-string x))
    (t (format nil "~a" x))
    ))

(defun clean-gender (x)
  (let*
   (
    (x x)
    (x (format nil "~a" x))
    (x (cl-ppcre:regex-replace-all "^ *" x ""))
    (x (subseq (concatenate 'string x " ") 0 1))
    (x (if (equal x "д") "ж" x))
    (x (string-downcase x))
    )
   x
   ))

(defun surrounded (left data right empty)
  (if (or
       (not (typep data 'sequence))
       (> (length data) 0)
       )
      (format nil "~a~a~a" left data right)
      empty))

(defun string-into-matches (s r)
  (loop
   with remains := s
   with re := (concatenate 'string "^(" r ").*$")
   for m := (cl-ppcre:regex-replace re remains "\\1")
   while (> (length m) 0)
   collect m
   do (setf remains (subseq remains (length m)))))

(defun sort-strings (l)
  (mapcar
   'second
   (sort
    (mapcar 
     (lambda 
      (x) 
      (list 
       (mapcar 
        (lambda (x) (if (cl-ppcre:scan "^[0-9]" x) (parse-integer x) x))
        (string-into-matches x "([0-9]+|[^0-9]+)")) 
       x))
     l)
    'uni-<)))

(defun naive-kill-tags (x)
  (cl-ppcre:regex-replace-all 
   "[<] */? *[a-zA-Z]+[^>]*[>]" 
   x ""))

(defun simplify-known-unicode (str &optional encoding)
  (map
   'string
   'identity
   (iterate
    (for x in-string str)
    (for xc := (char-code x))
    (case 
     xc
     ((160) (collect #\Space))
     ((173) )
     ((8203) )
     ((8212 8211) (collect #\-))
     ((8216 8217) (collect #\'))
     ((171) (if (find encoding '(:cp1251 :utf-8 :utf-16le :utf-16be))
              (collect x)
              (progn (collect #\<) (collect #\<))))
     ((187) (if (find encoding '(:cp1251 :utf-8 :utf-16le :utf-16be)) 
              (collect x) 
              (progn (collect #\>) (collect #\>))))
     ((185) (collect #\1))
     ((178) (collect #\2))
     ((179) (collect #\3))
     ((177) (collect #\+) (collect #\-))
     ((207) (collect #\I))
     ((214) (collect #\O))
     ((1257) (collect #\o))
     (t (collect x))
     )
    )
   )
  )

(defun clean-for-unibyte-encoding (str encoding)
  (let*
   (
    (ht (make-hash-table))
    )
   (loop 
    for code from 0 to 255 do 
    (ignore-errors 
     (setf 
      (gethash 
       #+sbcl
       (elt
        (sb-ext:octets-to-string 
         (make-array 1 :initial-element code 
                     :element-type '(unsigned-byte 8)) 
         :external-format encoding) 
        0)
       ht) t)))
   (map
    'string
    'identity
    (iterate
     (for x in-string str)
     (if (gethash x ht) 
         (collect x)
         (collect #\Space)
         )
     )
    )
   )
  )

(defun trimmer (&optional (ws *whitespace-list*))
  (lambda (x) (string-trim ws x)))

(defun trim-whitespace (x) (string-trim *whitespace-list* x))

(defgeneric string-to-octets (str enc))
(defgeneric octets-to-string (oct enc))

#+sbcl(defmethod string-to-octets ((str string) (enc t))
  (sb-ext:string-to-octets str :external-format enc)
)

#+ccl(defmethod string-to-octets ((str string) (enc t))
  (ccl:encode-string-to-octets str :external-format enc)
)

#+sbcl(defmethod octets-to-string ((oct sequence) (enc t))
  (sb-ext:octets-to-string oct :external-format enc)
)

#+ccl(defmethod octets-to-string ((oct sequence) (enc t))
  (ccl:decode-string-from-octets oct :external-format enc)
)

(defun recast-string-encoding (str from to)
  (when str
    (octets-to-string (string-to-octets str from) to)
  )
)

(defun email-synonyms (email)
  `(
    ,email
    ,@(loop
        for x in 
        `(
          ("@ya.ru$" "@yandex.ru")
          ("@yandex.ru$" "@ya.ru")
          )
        when (cl-ppcre:scan (first x) email)
        collect (cl-ppcre:regex-replace (first x) email (second x))
        )
    ))

(defun pairs-of-octets-of (str)
  (split-into-chunks (string-to-octets str :utf-16le) 2))

(defun encode-utf16-as-dash-separated-hex (str)
  "encode a UTF-16 string as dash-separated hexcodes"
  (string-downcase (join-list ; `string-downcase` is for bit-for-bit compatibility with the JS code
    (map 'list
      (lambda (p) (format nil "~16r" (+ (* 256 (second p)) (first p))))
      (pairs-of-octets-of str))
    "-")))

(defun encode-list-as-dash-separated-hex (lst)
  (mapcar (lambda (e) (when (position #\| e) (error "encoded list items can't contain #\|"))) lst)
  (encode-utf16-as-dash-separated-hex (join-list lst "|"))
  )

(defun valid-iso-date-string-p (x)
  (or
    (emptyp x)
    (let
      ((date-component-list (mapcar #'parse-integer (coerce
        (multiple-value-second (cl-ppcre:scan-to-strings "^(\\d{4})-(\\d{2})-(\\d{2})$" x))
        'list))))
      (and date-component-list (apply #'valid-date date-component-list)))))

(defparameter *transliterations-to-latin* (make-hash-table :test 'equal))
(loop 
  for x in 
  (cl-ppcre:split 
    (format nil "[~{~a~}]+" *whitespace-list*)
    "
    аa бb вv гg дd еye ёyo жzh зz иi йyh кk лl мm нn оo пp рr сs тt уu фf хkh цc чch шsh щshh ъ- ыih ь- эe юyu яya
    АA БB ВV ГG ДD ЕYe ЁYo ЖZh ЗZ ИI ЙYh КK ЛL МM НN ОO ПP РR СS ТT УU ФF ХKh ЦC ЧCh ШSh ЩShh Ъ- ЫIh Ь- ЭE ЮYu ЯYa
    ςw εe ρr τt υy θu ιi οo πp αa σs δd φf γg ηh ξj κk λl ζz χx ψc ωv βb νn μm
    ΣW ΕE ΡR ΤT ΥY ΘU ΙI ΟO ΠP ΑA ΣS ΔD ΦF ΓG ΗH ΞJ ΚK ΛL ΖZ ΧX ΨC ΩV ΒB ΝN ΜM
    ")
  unless (emptyp x)
  do (set-> 
       *transliterations-to-latin* 
       (elt x 0) (subseq x 1)))

(defun transliterate-to-latin (s)
  (clean-for-unibyte-encoding
    (apply
      'concatenate 'string
      (map 
	'list
	(lambda (x)
	  (->
	    *transliterations-to-latin*
	    x (string x)
	    )
	  )
	s))
    :latin1))

(defun kill-filename-unsafe-chars (s)
  (cl-ppcre:regex-replace-all
    " "
    (cl-ppcre:regex-replace-all
      "[~:\\\\/()*&;[\\]\"'!#$%^=+{}?`]"
      s "-")
    "_"))

(defun string-to-struct (s separator field-finisher)
  (loop
    for x in (cl-ppcre:split (format nil " *~a *" separator) s)
    collect 
    (list
      (cl-ppcre:regex-replace (format nil " *~a.*" field-finisher) x "")
      (cl-ppcre:regex-replace (format nil "((?!:~a).)*~a *" 
                                      field-finisher field-finisher) x "")
      )))

(defun struct-to-string (s separator field-finisher fields)
  (format 
    nil
    (format nil "~~{~~{~~a~a ~~a~~}~a ~~}" 
            field-finisher separator)
    (loop 
      for field in fields
      collect (list (first field)
                    (-> s (first field) 
                        (or (second field) "")))
      )))

(defun truncate-string (str length suffix)
  (if
    (> (length str) (+ length (length suffix)))
    (concatenate 'string (subseq str 0 length) suffix)
    str
    ))

(defparameter *multibyte-encodings*
  `(
    "utf-8" "utf8" "utf-32" "utf-16" "utf-7"
    "utf-32le" "utf-32be" "utf-16le" "utf-16be"
    "ucs-2" "ucs-4" "ucs2" "ucs4"
    "ucs-2le" "ucs-4le" "ucs2le" "ucs4le"
    "ucs-2be" "ucs-4be" "ucs2be" "ucs4be"
    ))

(declaim (inline safe-endp))
(defun safe-endp (x)                                                                                                                                                                                        
  (declare (optimize safety))
  (endp x))


(defun plist-list (plist)
  "Returns an list containing the same keys and values as the
property list PLIST in the same order."
  (let (llist)
    (do ((tail plist (cddr tail)))
        ((safe-endp tail) (nreverse llist))
      (push (list (car tail) (cadr tail)) llist))))


(defun plist-json (plist)
  (with-output-to-string (s)
    (yason:encode
          (mccme-helpers/parsetype:list-to-ht
            (plist-list plist))
   s)))


(defun list-json (llist)
  (with-output-to-string (s)
    (yason:encode
          (mccme-helpers/parsetype:list-to-ht
            llist)
   s)))
