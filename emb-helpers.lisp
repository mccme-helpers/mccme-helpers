(mccme-helpers/packages:file mccme-helpers/emb-helpers
  :use (mccme-helpers/functional)
  :export (
           #:emb-env
           #:emb-env-path
           #:capture-to
           #:emb-template-by-path
           #:*emb-template-vars*
    )
  )

(defvar *emb-template-vars* nil)

(defvar cl-emb-intern::env)
(defvar cl-emb-intern::topenv)

(defmacro emb-env (var)
  `(multifuncall (getf cl-emb-intern::env ,var)))

(defmacro emb-env-path (var)
  (let*
   (
    (name (symbol-name var))
    (slash-pos (position #\/ name :from-end t))
    (first-steps
     (when slash-pos 
           (subseq name 0 slash-pos)))
    (last-step
     (when slash-pos
           (subseq name (1+ slash-pos))))
    )
   (cond
    ((equal name "") '(progn cl-emb-intern::topenv))
    (slash-pos
      `(multifuncall
	 (getf (emb-env-path ,(intern first-steps :keyword)) 
	       ,(intern (string-upcase last-step) :keyword))))
    (t `(emb-env ,(intern (string-upcase name) :keyword)))
    )
   ))

(defmacro capture-to (var &rest body)
  `(progn
    (defvar ,var)
    (setf ,var (capture ,@body))))

(defun emb-template-by-path (path &optional (env nil))
  (cl-emb:execute-emb 
    (pathname path) 
    :env (append 
	   env
	   `(,@(apply 
		 'append 
		 (mapcar 
		   (lambda (x) 
		     (list (car x) 
			   (funcall (cadr x))))
		   *emb-template-vars*)))
	   )))

(loop for i in '(
  emb-env emb-env-path capture 
  defcaptured defcached defdelay 
  capture-to emb-template-by-path
  cl-emb::getf* cl-emb::getf-emb
  )
  do (import i :cl-emb-intern))

(unintern 'cl-emb-intern::for :cl-emb-intern)
(use-package :iterate :cl-emb-intern)
