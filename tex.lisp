(mccme-helpers/packages:file mccme-helpers/tex
  :use (:cl-emb
        :mccme-helpers/trivial
	:mccme-helpers/lisp
	:mccme-helpers/parsetype
        )
  :export (
           :tex-to-pdf
           :tex-code-to-pdf
           :compile-tex
	   :*mccme-helpers-tex-layout-templates*
           )
  )

(def-and-set-var
  *mccme-helpers-tex-layout-templates*
  (source-relative-path "tex/tex-layout.emb"))

#+sbcl (defun compile-tex (output-dir name-local &key
				      (tex-name "pdflatex") (iterations nil))
  (sb-ext:process-exit-code
   (sb-ext:run-program 
    "/bin/sh" 
    `("-c" 
      ,(format 
        nil "cd '~a' && for n in $(seq ~a); do ~a '~a'; done"
        output-dir (or iterations 3) tex-name name-local 
        )))))

(defun tex-code-to-pdf 
  (texcode output-dir &key (enc :cp1251) (code "") (tex-name "pdflatex")
           iterations (suffix ".pdf"))
  (ensure-directories-exist output-dir)
  (let*
    (
     (name-template (concatenate 'string output-dir "/temp-tex-" code "-XXXXXX"))
     (name-short (progn (ensure-directories-exist name-template)
			(temp-file-name name-template)))
     (name (concatenate 'string name-short ".tex"))
     (name-local (car (last (cl-utilities:split-sequence #\/ name))))
     )
    (with-open-file 
      (f name :direction :output :external-format enc :if-does-not-exist :create)
      (write-string texcode f)
      )
    (let*
      ((tex-exit-code (compile-tex output-dir name-local :tex-name tex-name :iterations iterations))
       (pdf-name (concatenate 'string name-short suffix)))
      (values (if (= 0 tex-exit-code) pdf-name nil) tex-exit-code pdf-name)
      )))

(defun tex-to-pdf (template env output-dir &key (enc :cp1251) (code "")(tex-name "pdflatex")
  (iterations nil) (suffix ".pdf"))
  (let*
   (
    (texcode (execute-emb (truename template) :env env))
    (texcode
      (if
        (find (format nil "~a" enc) *multibyte-encodings* :test 'equalp)
        texcode
        (clean-for-unibyte-encoding (simplify-known-unicode texcode) enc)
        ))
    )
   (tex-code-to-pdf texcode output-dir :enc enc :code code :tex-name tex-name :iterations iterations
                    :suffix suffix)
   ))
