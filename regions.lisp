(mccme-helpers/packages:file mccme-helpers/regions
  :use (mccme-helpers/lisp)
  :export (
           #:russia-regions
	   #:russia-region-synonym-translation
    )
  )

(def-and-set-var russia-regions
  `(
    (0 "нет")
    (1 "Республика Адыгея")
    (2 "Республика Башкортостан")
    (3 "Республика Бурятия")
    (4 "Республика Алтай")
    (5 "Республика Дагестан")
    (6 "Республика Ингушетия")
    ;(7 "Республика Кабардино-Балкария")
    (7 "Кабардино-Балкарская республика")
    (8 "Республика Калмыкия")
    ;(9 "Республика Карачаево-Черкесия")
    (9 "Карачаево-Черкесская республика")
    (10 "Республика Карелия")
    (11 "Республика Коми")
    (12 "Республика Марий Эл")
    (13 "Республика Мордовия")
    (14 "Республика Саха-Якутия")
    (15 "Республика Северная Осетия")
    (16 "Республика Татарстан")
    (17 "Республика Тыва")
    ;(18 "Республика Удмуртия")
    (18 "Удмуртская республика")
    (19 "Республика Хакасия")
    ;(20 "Республика Чечня")
    (20 "Чеченская республика")
    (21 "Республика Чувашия")
    (22 "Алтайский край")
    (23 "Краснодарский край")
    (24 "Красноярский край")
    (25 "Приморский край")
    (26 "Ставропольский край")
    (27 "Хабаровский край")
    (28 "Амурская область")
    (29 "Архангельская область")
    (30 "Астраханская область")
    (31 "Белгородская область")
    (32 "Брянская область")
    (33 "Владимирская область")
    (34 "Волгоградская область")
    (35 "Вологодская область")
    (36 "Воронежская область")
    (37 "Ивановская область")
    (38 "Иркутская область")
    (39 "Калининградская область")
    (40 "Калужская область")
    (41 "Камчатский край")
    (42 "Кемеровская область")
    (43 "Кировская область")
    (44 "Костромская область")
    (45 "Курганская область")
    (46 "Курская область")
    (47 "Ленинградская область")
    (48 "Липецкая область")
    (49 "Магаданская область")
    (50 "Московская область")
    (51 "Мурманская область")
    (52 "Нижегородская область")
    (53 "Новгородская область")
    (54 "Новосибирская область")
    (55 "Омская область")
    (56 "Оренбургская область")
    (57 "Орловская область")
    (58 "Пензенская область")
    (59 "Пермский край")
    (60 "Псковская область")
    (61 "Ростовская область")
    (62 "Рязанская область")
    (63 "Самарская область")
    (64 "Саратовская область")
    (65 "Сахалинская область")
    (66 "Свердловская область")
    (67 "Смоленская область")
    (68 "Тамбовская область")
    (69 "Тверская область")
    (70 "Томская область")
    (71 "Тульская область")
    (72 "Тюменская область")
    (73 "Ульяновская область")
    (74 "Челябинская область")
    (75 "Забайкальский край")
    (76 "Ярославская область")
    (77 "Москва")
    (78 "Санкт-Петербург")
    (79 "Еврейская автономная область")
    ;(80 "Забайкальский край")
    ;(81 "Пермский край")
    ;(82 "Камчатский край")
    (82 "Республика Крым")
    (83 "Ненецкий автономный округ")
    ;(84 "Красноярский край")
    ;(85 "Иркутская область")
    (86 "Ханты-Мансийский автономный округ – Югра")
    (87 "Чукотский автономный округ")
    ;(88 "Красноярский край")
    (89 "Ямало-Ненецкий автономный округ")
    (90 "Школы РФ, находящиеся за пределами территории РФ")
    (92 "Севастополь")
    (100 "Не РФ")
    (101 "Школы РФ, находящиеся за пределами территории РФ")
    ))

(defparameter *russia-regions* russia-regions)

(defparameter
  *russia-federal-districts*
  `(
    ("Белгородская область" "ЦФО")
    ("Брянская область" "ЦФО")
    ("Владимирская область" "ЦФО")
    ("Воронежская область" "ЦФО")
    ("Ивановская область" "ЦФО")
    ("Калужская область" "ЦФО")
    ("Костромская область" "ЦФО")
    ("Курская область" "ЦФО")
    ("Липецкая область" "ЦФО")
    ("Москва" "ЦФО")
    ("Московская область" "ЦФО")
    ("Орловская область" "ЦФО")
    ("Рязанская область" "ЦФО")
    ("Смоленская область" "ЦФО")
    ("Тульская область" "ЦФО")
    ("Тверская область" "ЦФО")
    ("Тамбовская область" "ЦФО")
    ("Ярославская область" "ЦФО")

    ("республика Карелия" "СЗФО")
    ("республика Коми" "СЗФО")
    ("Архангельская область" "СЗФО")
    ("Вологодская область" "СЗФО")
    ("Калининградская область" "СЗФО")
    ("Санкт-Петербург" "СЗФО")
    ("Ленинградская область" "СЗФО")
    ("Мурманская область" "СЗФО")
    ("Новгородская область" "СЗФО")
    ("Псковская область" "СЗФО")
    ("Ненецкий автономный округ" "СЗФО")

    ("республика Адыгея" "ЮФО")
    ("республика Калмыкия" "ЮФО")
    ("Астраханская область" "ЮФО")
    ("Ростовская область" "ЮФО")
    ("Краснодарский край" "ЮФО")
    ("Волгоградская область" "ЮФО")

    ("республика Дагестан" "СКФО")
    ("республика Ингушетия" "СКФО")
    ;("республика Кабардино-Балкария" "СКФО")
    ;("республика Карачаево-Черкесия" "СКФО")
    ("Кабардино-Балкарская республика" "СКФО")
    ("Карачаево-Черкесская республика" "СКФО")
    ("республика Северная Осетия" "СКФО")
    ;("республика Чечня" "СКФО")
    ("Чеченская республика" "СКФО")
    ("Ставропольский край" "СКФО")

    ("республика Башкортостан" "ПФО")
    ("республика Марий-Эл" "ПФО")
    ("республика Мордовия" "ПФО")
    ("республика Татарстан" "ПФО")
    ;("республика Удмуртия" "ПФО")
    ("Удмуртская республика" "ПФО")
    ("республика Чувашия" "ПФО")
    ("Кировская область" "ПФО")
    ("Нижегородская область" "ПФО")
    ("Оренбургская область" "ПФО")
    ("Самарская область" "ПФО")
    ("Пензенская область" "ПФО")
    ("Ульяновская область" "ПФО")
    ("Саратовская область" "ПФО")
    ("Пермский край" "ПФО")

    ("Курганская область" "УФО")
    ("Свердловская область" "УФО")
    ("Тюменская область" "УФО")
    ("Челябинская область" "УФО")
    ("Ханты-Мансийский автономный округ" "УФО")
    ("Ямало-Ненецкий автономный округ" "УФО")

    ("республика Бурятия" "СФО")
    ("республика Алтай" "СФО")
    ("республика Тыва" "СФО")
    ("республика Хакасия" "СФО")
    ("Алтайский край" "СФО")
    ("Красноярский край" "СФО")
    ("Иркутская область" "СФО")
    ("Кемеровская область" "СФО")
    ("Новосибирская область" "СФО")
    ("Омская область" "СФО")
    ("Томская область" "СФО")
    ("Забайкальский край" "СФО")

    ("республика Саха-Якутия" "ДФО")
    ("Приморский край" "ДФО")
    ("Хабаровский край" "ДФО")
    ("Камчатский край" "ДФО")
    ("Амурская область" "ДФО")
    ("Сахалинская область" "ДФО")
    ("Магаданская область" "ДФО")
    ("Еврейская автономная область" "ДФО")
    ("Чукотский автономный округ" "ДФО")

    ("республика Крым" "КФО")
    ("Севастополь" "КФО")

    ("нет" "НеФО")
    ("Не РФ" "НеФО")
    ("Школы РФ, находящиеся за пределами территории РФ" "НеФО")
    ))

(defun russia-region-synonym-translation (region)
  (case region
    ((95) 20)
    ((91) 82)
    (t region)
    ))
