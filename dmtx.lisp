(mccme-helpers/packages:file mccme-helpers/dmtx
  :use (
    mccme-helpers/trivial
  )
  :export (
    write-dmtx
  )
)

#+sbcl (defun write-dmtx (output-dir content &key resolution size margin (format "png"))
  (let*
    (
      (fname (concatenate 'string
        (temp-file-name (concatenate 'string output-dir "/dmtx-XXXXXX"))
        "."
        format
      ))
      (process (with-input-from-string
        (s content)
        (sb-ext:run-program "dmtxwrite" `(
          "-o" ,fname
          ,@(when resolution `("-r" ,resolution))
          ,@(when size `("-s" ,size ))
          ,@(when margin `("-m" ,margin ))
          "-f" ,format
        )
        :search t
        :input s
      )))
    )
    (sb-ext:process-exit-code process)
    fname
  )
)
