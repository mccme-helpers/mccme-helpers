(mccme-helpers/packages:file
  mccme-helpers/basic-db
  :use (
	:mccme-helpers/struct
	:mccme-helpers/lisp
	:iterate
	:cl-ppcre
  :mccme-helpers/trivial
	)
  :export (
	   #:sql-on
	   #:sql-off
	   #:with-db
	   #:sql-as
	   #:sql-op
	   #:sql-op-wrapped
	   #:sql-value
	   #:sql-mk-as
	   #:sql-wrap
	   #:sql-wrap-list
	   #:sql-literal
	   #:sql-ident-string
	   #:sql-eq-if-given
	   #:sql-match-attrib

	   #:many-to-many
	   #:join-via-many-to-many
	   #:add-many-to-many-entries
	   #:many-to-many-name-symbol

	   #:sql-insert
	   #:sql-insert-id
	   #:sql-inserted-id-of
	   #:sql-pluck

	   #:sql-insert-or-update

	   #:sql-select-query
	   #:sql-select-list-query

	   #:sql-explain

	   #:sql-sync-table

	   #:*db-connection-spec*
	   #:*db-type*

	   #:*name-entries*

	   #:query-with-marked-fields
	   #:select-with-marked-fields

	   #:trace-db

	   #:sql-suffixed
	   #:sql-op-suffixed
	   #:sql-eq-suffixed

	   #:sql-create-multiindex

	   #:sql-coerce
	   #:sql-call-fun

	   #:sql-equal

	   #:sql-in-op

	   #:sql-cast

	   #:table-struct

	   #:sql-create-name
     
     #:sql-not-or-null
     
     #:sql-desc

           #:sql-ident-from-string-desc
	   )
  )

(defvar *db-connection-spec*)
(defvar *db-type*)

(defvar *name-entries*)
(setf *name-entries* `(
  (code-name varchar unique)
  (display-name varchar not null)
  ))

(defmacro with-db (&body body)
  (let ((db-var (gensym)))
       `(clsql:with-database (,db-var *db-connection-spec* 
                             :pool t 
                             :database-type 
                             *db-type*)
                             (clsql:with-default-database 
                              (,db-var)
                              ,@body))))

(defun sql-as (table-name alias)
  (clsql:sql-expression :table table-name :alias alias))

(defun sql-value (&rest params)
  (apply 'clsql:sql-expression params))

(defun sql-op (&rest params)
  (apply 'clsql:sql-operation params))

(defun sql-op-wrapped (&rest params)
  (list (apply 'clsql:sql-operation params)))

(defmacro sql-mk-as (var table-name)
  `(setf ,var (sql-as ',table-name ',var)))

(eval-when (:load-toplevel :execute :compile-toplevel)
  (defun sql-on () 
    (clsql:restore-sql-reader-syntax-state)
    (clsql:locally-enable-sql-reader-syntax))
  (defun sql-off () (clsql:restore-sql-reader-syntax-state)))

#.(sql-off)
(defclass clsql-wrapper (clsql-sys::%sql-expression)
  ((content :initarg :content :initform nil :accessor clsql-wrapper-content)
   (clsql-sys::name :initarg :name :initform nil)))

(defmethod clsql-sys::output-sql ((self clsql-wrapper) database)
  (format clsql-sys::*sql-stream* 
          "~{~a ~} " 
          (mapcar (lambda (x) 
                          (progv '(clsql-sys::*sql-stream*) `(,(make-string-output-stream))
                                 (clsql-sys::output-sql x database)
                                 (get-output-stream-string clsql-sys::*sql-stream*)))
                  (clsql-wrapper-content self))))

(defun sql-wrap (name &rest args) 
  (make-instance 'clsql-wrapper :content args :name (or name (gensym))))
(defun sql-wrap-list (l)
  (apply 'sql-wrap nil l))

(defclass clsql-asis-sql (clsql-sys::%sql-expression)
  ((content :initarg :content :initform "" :accessor clsql-asis-sql-content)
   (clsql-sys::name :initarg :name :initform nil)))
(defmethod clsql-sys::output-sql ((self clsql-asis-sql) database)
  (format clsql-sys::*sql-stream*
	  "~a"
	  (clsql-asis-sql-content self)))
(defun sql-literal (name content)
  (make-instance 'clsql-asis-sql :content content :name (or name (gensym))))

(defun sql-ident-string (&rest args)
  (clsql:sql (apply 'clsql:sql-expression args)))

#.(sql-on)
(defun sql-table-exists (table-name)
 (or
  (find
   (format nil "~a" table-name)
   (clsql:list-tables)
   :test 'equalp
  )
  (ignore-errors
   (progn
    (and
    (cl-ppcre:scan "^[-_a-zA-Z0-9]*$" table-name)
    (clsql:select [id] :from (sql-literal nil table-name) :limit 1)
    )
    t
   )
  )
  (ignore-errors
   (progn
    (clsql:select [id] :from (sql-value :table table-name) :limit 1)
    t
   )
  )
 )
 )

(defun sql-column-exists (table-name column-name)
 (or
  (find
   (format nil "~a" column-name)
   (clsql:list-attributes table-name)
   :test 'equalp
  )
  (and
   (eq *db-type* :sqlite3)
   (equalp (format nil "~a" column-name) "id")
  )
  (ignore-errors
   (progn
    (clsql:select (sql-value :atribute column-name) 
     :from (sql-value :table table-name) :limit 1)
    t
   )
  )
  (ignore-errors
   (progn
    (and
     (cl-ppcre:scan "^[-_a-zA-Z0-9]*$" table-name)
     (cl-ppcre:scan "^[-_a-zA-Z0-9]*$" column-name)
     (clsql:select (sql-literal nil column-name) 
      :from (sql-literal nil table-name) :limit 1)
    )
    t
   )
  )
	))
#.(sql-off)

(defun sql-create-multiindex (table-name index-name column-names)
  (clsql:execute-command 
    (apply 'sql-wrap 'index-id
	   `(create 
	      index 
	      ,(intern 
		 (concatenate 
		   'string "IDX_NAMED_"
		   (sql-ident-string :attribute index-name)
		   ))
	      on ,(sql-value :table table-name) 
	      ,(sql-literal nil "(") ,@(cdr (loop 
			   for x in column-names 
			   collect (sql-literal nil ",") 
			   collect (sql-value :attribute x))) ,(sql-literal nil ")")
	      ))))

(defun sql-create-index (table-name column-name)
          (clsql:execute-command 
           (apply 'sql-wrap 'index-id
                  `(create index ,(intern (concatenate 'string "IDX_FOR_" (sql-ident-string :table table-name) 
                                                       "_COL_" (sql-ident-string :attribute column-name)))
                           on ,(sql-value :table table-name) 
                           ,(sql-literal nil "(") ,(sql-value :attribute column-name) ,(sql-literal nil ")")
                           ))))

(defun sql-sync-table (table-name columns &optional skip-columns)
  (unless (sql-table-exists (sql-ident-string :table table-name))
          (clsql:execute-command 
           (apply 
            'sql-wrap 'table-create 
            `(create table ,(sql-value :table table-name) 
                     ,(sql-literal nil "(")
                       id 
		       ,(cond
			  ((or (equal *db-type* :sqlite3) (equal *db-type* :sqlite))
			   'integer)
			  (t 'serial))
		       primary key 
		       ,@(cond
			   ((or (equal *db-type* :sqlite3) (equal *db-type* :sqlite))
			    '(autoincrement))
			   (t nil))
                       ,(sql-literal nil ")") )))
          (sql-create-index table-name 'id)
          )
  (unless skip-columns
          (loop for x in columns do
                (unless (sql-column-exists (sql-ident-string :table table-name) 
                                           (sql-ident-string :attribute (car x)))
                        (clsql:execute-command 
                         (apply 'sql-wrap 
                                'column-create 
                                (append
                                 `(alter table ,(sql-value :table table-name) 
                                         add column ,(sql-value :attribute (car x)))
                                 (cdr x))))
                        (ignore-errors (sql-create-index table-name (car x)))
                        ))))

(defun many-to-many-name-symbol (left right)
  (intern (concatenate 'string
               "MANY_MANY_"
               (sql-ident-string :table left)
               "_AND_"
               (sql-ident-string :table right))))

(defun fk-symbol (table)
  (intern (concatenate 'string (sql-ident-string :table table) "_ID")))

(defun many-to-many (left right &optional name)
  (let* (
         (name (or name (many-to-many-name-symbol left right)))
         (link-table-symbol (if (stringp name) (intern name) name))
         (left-link-symbol  (fk-symbol left))
         (right-link-symbol (fk-symbol right))
         )
        `(,link-table-symbol (
                              (,left-link-symbol integer references ,left)
                              (,right-link-symbol integer references ,right)
          ))
        ))

(defun join-via-many-to-many (existing added &key name existing-alias added-alias join-name
    existing-is-right)
  (let* (
         (join-name (or join-name (gensym)))
         (existing-alias (or existing-alias existing))
         (added-alias (or added-alias added))
         (name (or name (if existing-is-right 
                            (many-to-many-name-symbol added existing)
                            (many-to-many-name-symbol existing added))))
         (existing-fk (fk-symbol existing))
         (added-fk (fk-symbol added))
         )
  (sql-wrap join-name existing-alias 'inner 'join name 'on 
             (sql-op '= 
                     (sql-value :table existing-alias :attribute 'id)
                     (sql-value :table name :attribute existing-fk))
             'inner 'join added-alias 'on
             (sql-op '= 
                     (sql-value :table added-alias :attribute 'id)
                     (sql-value :table name :attribute added-fk))
             )))

(defun add-many-to-many-entries (left right pairs)
  (loop for x in pairs do 
        (clsql:insert-records :into 
                              (sql-value 
                               :table (many-to-many-name-symbol left right))
                              :attributes (list (fk-symbol left) (fk-symbol right))
                              :values x)))

(defun sql-insert (table &rest value-args)
  (let* (
         (nv-pairs (iter (generate x :in value-args)
                              (collect (list (next x) (next x)))))
         (table-sql (sql-value :table table))
         )
        (clsql:insert-records :into table-sql :av-pairs nv-pairs)))

#.(sql-on)
(defun sql-insert-id (table &rest value-args)
  (let* (
	 (nv-pairs (iter (generate x :in value-args)
       (collect
          (let* ((el (next x)))
            (cond
              ((listp el) 
               (list (sql-create-name (first el))
                             (second el))
               )
              (t (list (sql-create-name el) (next x)))
              )
          ))
			 ))
	 (table-sql (sql-create-name table :kind :table))
	 )
    (ecase *db-type*
      ((:postgresql :postgresql-socket :postgresql-socket3)
       (clsql:query
	 (concatenate 
	   'string
	   (clsql:sql 
	     (clsql-sys::make-sql-insert
	       :into table-sql
	       :av-pairs nv-pairs
	       )
	     )
	   (clsql:sql
	     'returning 
	     [id]
	     )
	   ))
       )
      ((:sqlite3 :sqlite)
       (clsql:query 
	 (clsql:sql 
	   (clsql-sys::make-sql-insert
	     :into table-sql
	     :av-pairs nv-pairs
	     )
	   ))
       (clsql:select 
	 [id] :from table-sql
	 :where [= [rowid] 
		   (sql-call-fun 
		     [last-insert-rowid])])
       )
      )))
#.(sql-off)

(defun sql-inserted-id-of (&rest everything)
  (only (only (apply #'sql-insert-id everything))))

(defun sql-explain (expr)
  (clsql:query (concatenate 'string "explain " expr)))

(defun sql-select-query (&rest args)
  (clsql:sql (apply 'sql-op 'clsql:select args)))

(defun sql-select-list-query (fields &rest args)
  (clsql:sql (apply 'sql-op 'clsql:select (append fields args))))

#.(sql-on)
(defun sql-pluck (source-table source-id target-table target-id fields)
  (let* ((data (car (clsql:query
                     (sql-select-list-query 
                      fields
                      :from 
                      source-table
                      :where
                      [= [id] source-id])))))
        (clsql:update-records target-table
                              :where 
                              [= [id] target-id]
                              :attributes
                              fields
                              :values
                              data)))

(defun sql-eq-if-given (attribute value)
  (if value [= attribute value] "t"))

(defun sql-match-attrib (attribute struct &optional table)
  [= (if table (sql-value :table table :attribute attribute) (sql-value :attribute attribute))
     (-> struct attribute)])

(defun query-with-marked-fields (query)
  (let* 
   (
    (data (multiple-value-list
           (clsql:query query)))
    (field-names (second data))
    (fields (first data))
    (field-syms 
     (mapcar (lambda 
              (x) 
              (intern 
               (regex-replace-all 
                "_"
                (string-upcase x) "-")
               :keyword))
             field-names))
    )
   (iterate
    (for l in fields)
    (collect
     (iterate
      (for x in l)
      (for h in field-syms)
      (collect (list h x))
      ))
    )))

(defun select-with-marked-fields (&rest args)
  (query-with-marked-fields
    (clsql:sql (apply 'sql-op 'select args))))

(defmacro trace-db (&rest body)
  `(progn
    (clsql:start-sql-recording)
    ,@body
    (clsql:stop-sql-recording)
    ))

(defun sql-insert-or-update (table conditions av-pairs &key on-insert)
  (let*
   (
    (ids (clsql:select [id] :from (sql-value :table table) :where conditions))
    )
   (values
    (if ids
        (progn
         (when av-pairs
	   (clsql:update-records 
	     (sql-value :table table) 
	     :av-pairs av-pairs :where [in [id] (mapcar 'car ids)])
	   )
         ids)
        (apply 'sql-insert-id table (apply 'append (append av-pairs on-insert)))
        )
    (null ids)
    )
   ))

(defun sql-suffixed (table suffix)
  (if suffix 
    (sql-wrap nil table 'as (concatenate-symbol-names table suffix))
    table))

(defun sql-op-suffixed (op table field s1 s2)
  (sql-op op
	  (sql-value :table (if s1 (concatenate-symbol-names table s1) 
			      table) 
		     :attribute field)
	  (sql-value :table (if s2 (concatenate-symbol-names table s2) 
			      table) 
		     :attribute field)
	  ))

(defun sql-eq-suffixed (table field s1 s2)
  (sql-op-suffixed '= table field s1 s2))

(defun sql-coerce (expr typ)
  (sql-wrap nil expr '|::| typ))

(defun sql-call-fun (f &rest args)
  (apply 
    'sql-wrap 
    nil f `(,(sql-literal nil "(")
	     ,@(cdr (loop for a in args collect (sql-literal nil ",") collect a))
	     ,(sql-literal nil ")"))))

(defun sql-equal (x y)
  (equal (clsql:sql x) (clsql:sql y)))

(defun sql-in-op (x y)
  (if 
   y
   [in x y]
   "f"
   ))

(defmacro sql-cast (x typ)
  `(sql-wrap
    nil 
    (sql-literal nil "cast (")
    ,x
    (sql-literal nil " as ")
    ,typ
    (sql-literal nil ")")
    )
  )

(defclass table-struct-wrapper ()
  (
   (table :initarg :table)
   (value-column :initarg :value-column)
   (key-column :initarg :key-column)
   (extra-condition :initarg :extra-condition)
   (cache :initarg :cache)
   ))

(defun table-struct (table key value &key extra-condition cache)
  (make-instance 
   'table-struct-wrapper
   :table table
   :key-column key
   :value-column value
   :extra-condition (or extra-condition "t")
   :cache cache
   ))

(defmethod has-entry
  ((obj t) (attr table-struct-wrapper))
  (let*
    (
     (value-column (-> attr 'value-column))
     (value-column
       (cond
	 ((symbolp value-column) 
	  (sql-value :attribute value-column))
	 (t value-column))
       )
     (table (-> attr 'table))
     (table
       (cond
	 ((keywordp table) (sql-value :table table))
	 ((symbolp table) (sql-value :table table))
	 (t table)
	 ))
     (key-column (-> attr 'key-column))
     (key-column
       (cond
	 ((symbolp key-column)
	  (sql-value :attribute key-column)
	  )
	 (t key-column)
	 )
       )
     (cache (-> attr 'cache))
     (cache-filled
       (or
	 (and (hash-table-p cache)
	      (< 0 (hash-table-count cache)))
	 ))
     (db-data
       (cond
	 (cache-filled nil)
	 (cache
	   (with-db
	     (clsql:select
	       key-column value-column :from table
	       :where
	       [and
		 (-> attr 'extra-condition)
		 ]
	       ))
	   )
	 (t 
	   (with-db
	     (clsql:select
	       value-column :from table
	       :where
	       [and
		 [= obj key-column ]
		 (-> attr 'extra-condition)
		 ]
	       )))))
     )
    (and
      cache
      (not cache-filled)
      (map-generic
	db-data
	(lambda (x)
	  (set-> cache (first x) (second x))))
      )
    (if 
      cache
      (if
	(has-entry cache obj)
	(list (list (get-entry cache obj))))
      db-data
      )
    )
  )

(defmethod get-entry
  ((obj t) (attr table-struct-wrapper) &optional (default nil))
  (caar
    (or
      (has-entry
	obj attr
	)
      (list (list default)))))

(defmethod with-updated-entry
  ((obj list) (attr table-struct-wrapper) value)
  (with-db
   (clsql:update-records
    (sql-value :table (-> attr 'table))
    :where [= obj (sql-value :attribute (-> attr 'key-column))]
    :av-pairs `(((sql-value :attribute (-> attr 'value-column))
                 ,value))
    )))

(defmethod has-entry
  ((obj string) (attr table-struct-wrapper))
  (let*
    (
     (value-column (-> attr 'value-column))
     (value-column
       (cond
	 ((symbolp value-column) 
	  (sql-value :attribute value-column))
	 (t value-column))
       )
     (table (-> attr 'table))
     (table
       (cond
	 ((keywordp table) (sql-value :table table))
	 ((symbolp table) (sql-value :table table))
	 (t table)
	 ))
     (key-column (-> attr 'key-column))
     (key-column
       (cond
	 ((symbolp key-column)
	  (sql-value :attribute key-column)
	  )
	 (t key-column)
	 )
       )
     (cache (-> attr 'cache))
     (cache-filled
       (or
	 (and (hash-table-p cache)
	      (< 0 (hash-table-count cache)))
	 ))
     (db-data
       (cond
	 (cache-filled nil)
	 (cache
	   (with-db
	     (clsql:select
	       key-column value-column :from table
	       :where
	       [and
		 (-> attr 'extra-condition)
		 ]
	       ))
	   )
	 (t 
	   (with-db
	     (clsql:select
	       value-column :from table
	       :where
	       [and
		 [= obj key-column ]
		 (-> attr 'extra-condition)
		 ]
	       )))))
     )
    (and
      cache
      (not cache-filled)
      (map-generic
	db-data
	(lambda (x)
	  (set-> cache (first x) (second x))))
      )
    (if 
      cache
      (if
	(has-entry cache obj)
	(list (list (get-entry cache obj))))
      db-data
      )
    )
  )

(defmethod get-entry
  ((obj string) (attr table-struct-wrapper) &optional (default nil))
  (caar
    (or
      (has-entry
	obj attr
	)
      (list (list default)))))

(defmethod with-updated-entry
  ((obj string) (attr table-struct-wrapper) value)
  (with-db
   (clsql:update-records
    (sql-value :table (-> attr 'table))
    :where [= obj (sql-value :attribute (-> attr 'key-column))]
    :av-pairs `(((sql-value :attribute (-> attr 'value-column))
                 ,value))
    )))

(defun sql-create-name
  (name &key (kind :attribute) (args nil))
  (cond
    ((symbolp name) (apply 'sql-value kind name args))
    ((stringp name) (apply 'sql-value kind (intern (string-upcase name) :keyword) args))
    (t name)
    )
  )
  
(defun sql-not-or-null (x)
  [or
    [null x]
    [not x]
    ])

(defun sql-desc (field)
  "(sql-wrap nil field 'desc)"
  (sql-wrap nil field 'desc)
  )

(defun-weak 
  sql-table-abbreviation-expand (table)
  table)

(defun-weak 
  sql-attribute-abbreviation-expand (attribute)
  attribute)
	    

(defun sql-ident-from-string-desc (x &key table)
  (pipe-value
    (x x)
    (string-upcase x)
    (cl-ppcre:regex-replace-all "[-]" x "_")
    (cl-ppcre:regex-replace "(.*)[+](.*)" x "\\2.\\1")
    (cl-ppcre:split "[.]" x)
    (mapcar (lambda (y) (intern y :keyword)) x)
    (if (second x) x (cons table x))
    (sql-value :table (sql-table-abbreviation-expand (first x))
	       :attribute (sql-attribute-abbreviation-expand (second x))
	       )
    ))

#.(sql-off)
