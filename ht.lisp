(mccme-helpers/packages:file mccme-helpers/ht
  :use (
        mccme-helpers/emb-helpers
        mccme-helpers/auth-pages
        )
  :export (
           #:define-emb-page
    )
  )

(defvar *last-emb-page-env*)

(defmacro define-emb-page-straight (handler-symbol uri template &key attrs args (verify t)
  (content-type "text/html; charser=utf-8") body)
  `(ht:define-easy-handler 
    (,handler-symbol :uri ,uri ,@attrs) ,args
    (mccme-helpers/functional:backtraceable
     ,@(when verify `((verify-login)))
     (when ht:*reply* (setf (ht:content-type*) ,content-type))
     (let* (
            (env (setf *last-emb-page-env* (progn ,@body))) 
            (data 
             (progv
              '(cl-emb:*escape-type*) '(:html)
             (emb-template-by-path ,template env))))
           (when (getf env :log) 
                 (format *error-output* "Environment ~s~%" env)
                 (format *error-output* "Output ~s~%" data)
                 )
           (
            #+sbcl sb-ext:string-to-octets 
            #+ccl ccl:encode-string-to-octets
            data :external-format :utf-8))
     )))

(defmacro define-emb-page (args &rest body) `(define-emb-page-straight ,@args :body ( ,@body)))
