(mccme-helpers/packages:file mccme-helpers/data-transformer
  :use (mccme-helpers/struct
        mccme-helpers/basic-db
        mccme-helpers/lisp
        mccme-helpers/parsetype
        mccme-helpers/functional
        mccme-helpers/trivial
        :iterate
        )
  :export (
           #:field-description
           #:add-field-attribute
           #:transformer-description
           #:transformer-fields
           #:data-transformer
           #:data-transformer-errors
           #:transformer-db-fields
           #:transformer-db-field-names
           #:transformer-db-av-pairs
           #:data-transformer-import-data
           #:data-transformer-import-data-fast
           #:data-transformer-load-ordered-strings 
           #:transformer-pull-data
           #:transformer-push-data
           #:transformer-export-strings
           #:transformer-export-plist
           #:transformer-query
           #:transformer-http-post-collect
           #:transformer-data-env
           #:transformer-html-env
           #:transformer-html-env-split
           #:data-transformer-special
           #:transformer-schema-to-csv
           #:transformer-schema-disable-table
           #:transformer-schema-only-table
           #:transformer-schema-disable-fields
           #:transformer-schema-edit-field
           #:transformer-schema-insert-field
           #:transformer-schema-reorder-fields
           #:data-transformer-process-data
           #:data-transformer-verify-data
           #:transformer-equal
           #:transformer-correct-fields-alist  
           #:transformer-copy-errors
           #:transformer-copy-warnings
           #:transformer-find-warnings
           #:transformer-schema-query-db
           #:transformer-change-field-attribute
           #:data-transformer-meta
           #:data-transformer-content
           #:transformer-default-fill

           #:transformer-query-and-prepare-by-schema

           #:transformer-hashtable-collect
           #:transformer-hashtable-list-collect
           
           #:edit-schema-field-attribute
           
           #:transformer-flexible-pull
           )
  )

(defvar *field-attribute-descriptions*)
(setf *field-attribute-descriptions* (make-hash-table))
; Entry is an alist?
; name
; default : a function of field alist

(defmacro add-field-attribute (name default)
  `(set-> *field-attribute-descriptions* ,name 
          `((:name ',,name)
            (:default ,(lambda (x) (declare (ignorable x)) ,default)))))

(add-field-attribute :code-name nil)
(add-field-attribute :display-name (format nil "~a" (-> x :code-name)))
(add-field-attribute :type nil)
; Types
; :int
; :string
; :float
; :float-permissive
; :bool
; :date is supposed to be "DB-friendly"
; :reference

(defvar *boolean-t-regex*)
(setf *boolean-t-regex* "^(checked|[ ]*[+tyдTYД1].*)$")

(defvar *type-defaults*)
(setf *type-defaults*
  '(
    (:int -999999)
    (:string "")
    (:float -999999.0)
    (:float-permissive -999999.0)
    (:bool nil)
    (:date nil)
    (:timestamp nil)
    (:rusolymp-teacher nil)
    (:reference nil)
    (:jsonb nil)
    (nil nil)
    ))

(defmacro regex-matcher (expr)
  `(lambda (x) 
           (cl-ppcre:scan ,(concatenate 'string "^" expr "$") x)))

(defvar *type-string-verification*)
(setf *type-string-verification*
  `(
    (:int ,(regex-matcher " *([+-]?[0-9]{1,15})? *"))
    (:string ,(constantly t))
    (:float ,(regex-matcher "( *[+-]?[0-9]{1,15}([.,][0-9]*)? *)?"))
    (:float-permissive ,(constantly t))
    (:bool ,(regex-matcher "(|checked|[ ]*[-+tfynднДНTFYN01].*)"))
    (:date ,(regex-matcher "[0-9]{4}-[0-1][0-9]-[0-3][0-9]"))
    (:timestamp ,(constantly t))
    (:rusolymp-teacher ,(constantly t))
    (:reference ,(regex-matcher " *[+-]?[0-9]+"))
    (:jsonb ,(constantly t))
    (nil ,(constantly t))
    ))

(defvar *type-string-parser*)
(setf *type-string-parser* `(
  (:int ,(lambda (x) 
     (cond
       ((integerp x) x)
       (t
     (when (and x (not (equal (string-trim " " x) ""))) 
           (parse-integer (string-trim *whitespace-list* x)))))))
  (:string nil)
  (:float 
   ,(lambda 
     (x) 
     (let*
      ((x-clean (string-trim '(#\Space #\Tab #\Newline) x)))
      (when (not (equal x-clean ""))
            (parse-number::parse-number
             (cl-ppcre:regex-replace "," x-clean "."))))))
  (:float-permissive
   ,(lambda 
     (x) 
     (let*
      ((x-clean (string-trim '(#\Space #\Tab #\Newline) x)))
      (ignore-errors
       (parse-number::parse-number
        (cl-ppcre:regex-replace "," x-clean "."))))))
  (:bool ,(lambda (x) (not (null (cl-ppcre:scan *boolean-t-regex* x)))))
  (:date nil)
  (:timestamp nil)
  (:rusolymp-teacher nil)
  (:reference ,(lambda (x) (when (and x (not (equal (string-trim " " x) ""))) (parse-integer x))))
  (:jsonb nil)
  (nil ,(constantly nil))
  ))

(defvar *type-db-description*)
(setf *type-db-description* `(
  (:int (integer))
  (:string (varchar))
  (:bool (boolean))
  (:float (float))
  (:float-permissive (float))
  (:date (date))
  (:timestamp (timestamp))
  (:rusolymp-teacher (rusolymp-teacher))
  (:reference 
   ,(lambda 
     (x) 
     `(integer references ,(sql-value :table (-> x :db-referenced-table)))))
  (:jsonb (jsonb))
  ))

(defvar *type-string-export*)
(setf *type-string-export* `(
  (:int ,(lambda (x) (if x (format nil "~a" x) "")))
  (:string identity)
  (:bool ,(lambda (x) (if (or (eq x t) 
                              (cl-ppcre:scan *boolean-t-regex* x)) 
                          "да" "нет")))
  (:float ,(lambda (x) (if x (format nil "~a" x) "")))
  (:float-permissive ,(lambda (x) (if x (format nil "~a" x) "")))
  (:reference ,(lambda (x) (format nil "~a" x)))
  (:date identity)
  (:timestamp identity)
  (:rusolymp-teacher identity)
  (:jsonb identity)
  ))

(add-field-attribute :default (-> *type-defaults* (-> x :type)))
(add-field-attribute :default-function (constantly (-> x :default)))

(add-field-attribute :string-verification 
  (list (-> *type-string-verification* (-> x :type))))
; (lambda (x) (> x 2))
(add-field-attribute :data-verification nil)
; (lambda (x) (> (-> x :n) 2))
(add-field-attribute :global-verification nil)

; `( ( ,(lambda (x) x) "X is not null" ) )
; `( ( ,(lambda (x) (-> x :y)) "X is not null" :y ) )
(add-field-attribute :warnings nil)
(add-field-attribute :global-warnings nil)

(add-field-attribute :string-verification-error 
  "Строка не задаёт допустимого значения")
(add-field-attribute :data-verification-error "Некорректное значение")
(add-field-attribute :global-verification-error "Несогласованные данные")

(add-field-attribute :string-verification-error-cause (-> x :code-name))
(add-field-attribute :data-verification-error-cause (-> x :code-name))
(add-field-attribute :global-verification-error-cause (-> x :code-name))

(add-field-attribute :string-parsing (-> *type-string-parser* (-> x :type)))
(add-field-attribute :data-processing nil)

(defclass field-description () (
  (attributes :initarg :attribs
              :accessor field-description-attributes)
  (value-cache :initform (make-hash-table))
  ))
(defun field-description-function (x) 
  (make-instance 'field-description :attribs x))
(defmacro field-description (&rest x) 
  `(field-description-function ',x))

(defmethod get-entry ((obj field-description) attr &optional (default nil))
  (multiple-value-bind 
   (answer found) (gethash attr (slot-value obj 'value-cache))
   (if found answer 
       (let ((result
              (if (-> *field-attribute-descriptions* attr)
                  (if (has-entry (field-description-attributes obj) attr)
                      (-> (field-description-attributes obj) attr)
                      (let ((x (funcall
                                (-> (-> *field-attribute-descriptions* attr)
                                    :default)
                                obj
                                )))
                           (set-> (field-description-attributes obj) attr x)
                           x
                           )
                      )
                  default)))
            (setf (gethash attr (slot-value obj 'value-cache)) result)
            result))))


(defmethod has-entry ((obj field-description) attr)
  (has-entry *field-attribute-descriptions* attr))

(defclass transformer-description () (
  (fields :initarg :fields :accessor transformer-fields)
  (name-index :accessor transformer-name-index)
  (field-attributes-seen :initform (make-hash-table))
  ))

(defmacro def-iterate-fields (f args &rest body)
  `(defun ,f (obj ,@args)
          ,@(when (stringp (first body)) (list (first body)))
          (iter (for field :in-vector (transformer-fields obj))
                ,@body)))

(defmacro def-iterate-schema (f args &rest body)
  `(defun ,f (obj ,@args) 
          ,@(when (stringp (first body)) (list (first body)))
          (iterate:iter (for field :in obj)
                        (for entry := nil)
                        (iter (for pair :in field)
                              (set-> entry (first pair) (second pair)))
                        ,@body)))

(defmacro with-field (syms &rest body)
  `(let ((,(or (cadr syms) 'x) (-> ,(or (caddr syms) 'field) ,(car syms))))
        ,@body))

(def-iterate-fields transformer-collect-attributes (attr &optional (predicate))
  (let* ((pred (or predicate (lambda (x) (-> x attr)))))
        (with-field (attr) (when (funcall pred obj) (collect x)))))

(defmethod initialize-instance 
  :after ((x transformer-description) &rest initargs)
  (declare (ignore initargs))
  (let* (
         (fields (slot-value x 'fields))
         (fields-vector (map 'vector 'field-description-function fields))
         (fields-list (map 'list 'identity fields-vector))
         (fields-name-hash (iter (with res := 
                                       (make-hash-table :test 'equal))
                                 (for entry :in fields)
                                 (for entry-num :upfrom 0)
                                 (setf (gethash (-> entry :code-name) res) 
                                       entry-num)
                                 (finally (return res))
                                 ))
         )
        (setf (slot-value x 'fields) fields-vector)
        (setf (slot-value x 'name-index) fields-name-hash)
        (loop for f in fields-list do
              (iter (for (a v) :in-hashtable 
                         *field-attribute-descriptions*)
                    (declare (ignorable v))
                    (when (-> f a) 
                          (setf (gethash 
                                 a 
                                 (slot-value 
                                  x 
                                  'field-attributes-seen)) 
                                t)
                          )))
        ))

(defmethod has-entry ((obj transformer-description) attr)
  (has-entry (transformer-name-index obj) attr))

(defmethod get-entry 
  ((obj transformer-description) attr &optional (default nil))
  (let ((x (-> (transformer-name-index obj) attr)))
       (if x (-> (transformer-fields obj) x) default)))

(defun transformer-description (&rest fields)
  (if (listp (car fields))
      (make-instance 'transformer-description :fields fields)
      (make-instance 'transformer-description :fields (car fields))))

(defun transformer-description-direct (fields)
  (make-instance 'transformer-description :fields fields))

(defclass data-transformer () (
  (metadata :accessor data-transformer-meta :initarg :meta)
  (data :accessor data-transformer-content :initarg :data)
  (special-attributes :accessor data-transformer-special 
                      :initform (make-hash-table))
  (errors :accessor data-transformer-errors 
          :initform (make-hash-table)
          :initarg :errors)
  (warnings :accessor data-transformer-warnings
          :initform (make-hash-table)
          :initarg :warnings)
  ))

(defun data-transformer (meta &optional (data nil))
  (make-instance 'data-transformer :meta (transformer-description-direct meta) 
                 :data data)
  )

(defmethod has-entry ((obj data-transformer) attr)
  (has-entry (data-transformer-meta obj) attr))

(defmethod has-entry ((obj data-transformer) (attr integer))
  (< attr (length (transformer-fields obj))))

(defmethod get-entry ((obj data-transformer) attr &optional (default nil))
  (let* (
         (data (data-transformer-content obj))
         (meta (data-transformer-meta obj))
         (idx (-> (transformer-name-index meta) attr))
         )
        (if (and data idx)
            (elt data idx)
            default)))

(defmethod get-entry 
  ((obj data-transformer) (attr integer ) &optional (default nil))
  (let* (
         (data (data-transformer-content obj))
         )
        (if data
            (elt data attr)
            default)))

(defmethod get-entry ((obj data-transformer) 
  (attr field-description) 
  &optional (default nil))
  (get-entry obj (get-entry attr :code-name) default))

(defmethod has-entry ((obj data-transformer) 
  (attr field-description))
  (has-entry obj (get-entry attr :code-name)))

(defmethod with-updated-entry ((obj data-transformer) 
  (attr field-description) value)
  (with-updated-entry obj (get-entry attr :code-name) value))

(defmethod transformer-fields ((obj data-transformer))
  (transformer-fields (slot-value obj 'metadata)))

(defmethod transformer-name-index ((obj data-transformer))
  (transformer-name-index (slot-value obj 'metadata)))

; data should be either a long enough sequence or nil
(defmethod with-updated-entry ((obj data-transformer) attr value)
  (progn
   (unless (slot-value obj 'data) 
           (setf (slot-value obj 'data)
                 (make-array (length 
                              (transformer-fields obj))
                             :initial-element nil
                             )))
   (setf (elt (slot-value obj 'data) 
              (-> (transformer-name-index obj) 
                  attr))
         value)
   obj))

; data should be either a long enough sequence or nil
(defmethod with-updated-entry ((obj data-transformer) (attr integer) value)
  (progn
   (unless (slot-value obj 'data) 
           (setf (slot-value obj 'data)
                 (make-array (length 
                              (transformer-fields obj)))))
   (setf (elt (slot-value obj 'data) attr) value)
   obj))

(defun data-transformer-verify-data
  (obj verifier-name error-name &optional 
       (cause-name :code-name) (global nil) 
       (skipped-fields (make-hash-table)))
  (iter (with error-list := nil)
        (with error-map := (make-hash-table))
        (with skipped-fields-new := (make-hash-table))
        (for field :in-vector (transformer-fields obj))
        (for attr := (-> field :code-name))
        (for num :upfrom 0)
        (for verifier-clause := (-> field verifier-name))
        (for verifier-list := 
             (if (listp verifier-clause) 
                 verifier-clause 
                 (list verifier-clause)))
        (for verified-value := (cond 
                                (global obj)
                                ((-> field :type) (-> obj attr))
                                (t nil)))
        (iter (for verifier :in verifier-list)
              (when (-> skipped-fields attr) 
                    (set-> skipped-fields-new attr t))
              (unless (or
                       (-> skipped-fields-new attr)
                       (null verifier)
                       (funcall verifier verified-value))
                      (setf error-list (cons (format nil "~a: ошибка. ~a" 
                                                     (-> field :display-name)
                                                     (-> field error-name)
                                                     ) error-list))
                      (push 
                       (let
       ((e (-> field error-name)))
       (cond
         ((functionp e) (funcall e obj))
         (t e))
       )
                       (gethash (-> field cause-name)
                                (data-transformer-errors obj))
                       )
                      (set-> skipped-fields-new attr t)
                      (set-> error-map (-> field cause-name) (car error-list))
                      ))
        (finally (return (values (reverse error-list)
                                 skipped-fields-new
                                 error-map
                                 ))))
  )

(defun order-mapped-strings (obj strs)
  (iter (with res := (make-array (length 
                                  (transformer-fields 
                                   (slot-value obj 'metadata)))))
        (for field :in (transformer-fields obj))
        (for num :upfrom 0)
        (setf (elt res num) (-> strs (-> field :code-name)))
        (finally (return res))))

(defun data-transformer-process-data 
  (obj convertor-name &optional skipped-fields)
  (declare (special *backtrace-error-context*))
  (unless (-> (slot-value (data-transformer-meta obj)
                          'field-attributes-seen)
              convertor-name)
          (return-from data-transformer-process-data nil))
  (iter (for field :in-vector (transformer-fields obj))
        (for attr := (-> field :code-name))
        (for num :upfrom 0)
        (push
         (list field attr) 
         *backtrace-error-context*)
        (unless (or
                 (null (-> field convertor-name))
                 (null (-> field :type))
                 (-> skipped-fields attr)
                 )
                (let* (
                       (convertor-clause (-> field convertor-name))
                       (convertor-list (if (and 
                                            convertor-clause
                                            (or
                                             (functionp convertor-clause)
                                             (symbolp convertor-clause)
                                             ))
                                           (list convertor-clause)
                                           convertor-clause))
                       )
                      (push 
                       (-> obj attr)
                       *backtrace-error-context*)
                      (loop for func in convertor-list 
                            do
                            (set-> obj attr (funcall func (-> obj attr)))
                            )
                      (pop *backtrace-error-context*)
                      ))
        (pop *backtrace-error-context*)
        ))

(add-field-attribute :upload-keep-by-default nil)
(add-field-attribute :hide-file-value nil)
(add-field-attribute :non-blind-transfer (-> x :upload-keep-by-default))

(add-field-attribute :pull-self nil)
(add-field-attribute :pull-attribute 
  (if (-> x :pull-self) (-> x :code-name) nil))
(add-field-attribute 
  :pull-function 
  (progn
   (if (-> x :pull-attribute) 
       (if
        (-> x :upload-keep-by-default)
        (lambda (o r) 
                (or  
                 (let ((v (-> o (-> x :pull-attribute)))) 
                      (unless (emptyp v) v))
                 (-> r (-> x :pull-attribute))))
        (lambda (o &optional remote) 
                (declare (ignorable remote))
                (-> o (-> x :pull-attribute)))
        )
       nil )))
(add-field-attribute :push-self nil)
(add-field-attribute :push-attribute 
  (if (-> x :push-self) (-> x :code-name) nil))
(add-field-attribute 
  :push-function 
  (if
    (-> x :upload-keep-by-default)
    (lambda (o r) 
      (or (let ((v (-> o x))) (unless (emptyp v) v)) (-> r x)))
    (lambda (o &optional remote) 
            (declare (ignorable remote))
            (-> o x))
    ))

(add-field-attribute :skip-transfer-csv nil)
(add-field-attribute :skip-pull-csv (-> x :skip-transfer-csv))
(add-field-attribute :skip-push-csv (-> x :skip-transfer-csv))

(defun transformer-copy-errors (obj data-obj)
  (Setf (data-transformer-errors obj) (data-transformer-errors data-obj)))

(defun transformer-copy-warnings (obj data-obj)
  (Setf (data-transformer-warnings obj) (data-transformer-warnings data-obj)))

(add-field-attribute :empty-condition #'not)

(defun transformer-pull-data (obj data-obj &key only-non-empty)
  (iter (for field :in-vector (transformer-fields obj))
  (let* ((attr (-> field :code-name))
         (remote-fun (-> field :pull-function))
         (remote-val 
     (when remote-fun
       (if
         (-> field :non-blind-transfer)
         (funcall remote-fun data-obj obj)
         (funcall remote-fun data-obj))))
         )
    (when (and
      remote-fun 
      (or
        (not only-non-empty)
        (not 
          (funcall 
      (-> field :empty-condition #'not) 
      remote-val))))
      (set-> obj attr remote-val)))))

(defun transformer-push-data (obj data-obj)
  (iter (for field :in-vector (transformer-fields obj))
        (let (
              (remote-attr (-> field :push-attribute))
              (push-value 
               (if
                (-> field :non-blind-transfer)
                (funcall (-> field :push-function) obj data-obj)
                (funcall (-> field :push-function) obj)))
              )
             (when remote-attr
                   (set-> data-obj remote-attr push-value)))))

(add-field-attribute :db-field-name 
  (cond
   ((eq (-> x :type) :reference) 
    (concatenate-symbol-names (-> x :code-name) '-id))
   ((-> x :type) (-> x :code-name))
   (t nil)
   ))

(add-field-attribute :db-field-type 
  (let ((type-clause (-> *type-db-description* (-> x :type))))
       (cond
        ((listp type-clause) type-clause)
        ((functionp type-clause) (funcall type-clause x))
        (t nil)
        )))

(add-field-attribute :db-referenced-table nil)
(add-field-attribute :db-referenced-field 'id)
(add-field-attribute :db-reference-value 
         (lambda (val &optional table) (declare (ignorable table)) val))
(add-field-attribute :db-referenced-value 
         (lambda (val &optional table) (declare (ignorable table)) val))
(add-field-attribute :db-reference-extra-condition-generator nil)

(defun transformer-db-fields (obj)
  (iter (for field :in-vector (transformer-fields obj))
        (when (-> field :db-field-type) 
              (collect (cons (-> field :db-field-name) 
                             (-> field :db-field-type))))))

(defun transformer-db-av-pairs (obj)
  (iter (for field :in-vector (transformer-fields obj))
        (let ((the-type (-> field :db-field-type))
              (dbn (-> field :db-field-name))
              )
             (when (and
                    the-type 
                    (not (eq dbn :id))
                    )
                   (collect (list (sql-value :attribute dbn)
                                  (-> obj field)))))))

(add-field-attribute :db-table-name nil)

(defun transformer-field-db-name (field)
  (when (-> field :db-field-type)
        (if 
         (keywordp (-> field :db-field-name))
         (apply 'sql-value
                `(:attribute 
                  ,(-> field :db-field-name)
                  ,@(when (-> field :db-table-name)
                          `(:table ,(-> field :db-table-name))
                          )
                  )
                )
         (-> field :db-field-name)
         )))

(defun transformer-db-field-names (obj &key force-aliases)
  (iter (for field :in-vector (transformer-fields obj))
        (for db-field-name := (transformer-field-db-name field))
        (collect
          (cond
            (force-aliases
              (sql-wrap 
                nil db-field-name 'as 
                (sql-value :attribute (-> field :code-name))))
            (t db-field-name)))))

(defun data-transformer-import-data-fast (obj data)
  (setf (slot-value obj 'data) data)
  (setf (slot-value obj 'errors) (make-hash-table))
  )

(defun data-transformer-import-data (obj data)
  (setf (slot-value obj 'data) 
        (make-array (max (length data) 
                         (length (transformer-fields obj)))
                                           :element-type t))
  (loop for n from 0 to (- (length data) 1)
        do (setf (elt (slot-value obj 'data) n)
                 (elt data n)))
  (setf (slot-value obj 'errors) (make-hash-table))
  )

(defun data-transformer-load-ordered-strings 
  (obj data &key (skip-check nil) (format-first nil)
       (loopback nil))
  (let*
   (
    (*backtrace-error-context*
     (cons data *backtrace-error-context*))
    )
   (if loopback
       (when format-first 
             (iter 
              (for n from 0 to (- (length (data-transformer-content obj)) 1))
              (let ((old-data (elt (data-transformer-content obj) n)))
                   (setf (elt (data-transformer-content obj) n) 
                         (if old-data (format nil "~a" old-data) ""))
                   )))
       (data-transformer-import-data 
        obj 
        (if format-first
            (mapcar (lambda (x) (if x (format nil "~a" x) "")) data)
            data)))
   (let* ((check-values-str 
           (unless skip-check 
                   (multiple-value-list (data-transformer-verify-data 
                                         obj :string-verification 
                                         :string-verification-error 
                                         :string-verification-error-cause
                                         nil
                                         ))))
          (dummy-parsing-result (data-transformer-process-data 
                                 obj :string-parsing 
                                 (second check-values-str)))
          (check-values-data 
           (unless skip-check 
                   (multiple-value-list (data-transformer-verify-data 
                                         obj :data-verification 
                                         :data-verification-error
                                         :data-verification-error-cause
                                         nil
                                         (second check-values-str)))
                   ))
          (dummy-processing-result (data-transformer-process-data 
                                    obj :data-processing
                                    (second check-values-data)))
          (check-values-global 
           (unless skip-check 
                   (multiple-value-list (data-transformer-verify-data 
                                         obj :global-verification
                                         :global-verification-error
                                         :global-verification-error-cause
                                         t
                                         (second check-values-data)))))
          (all-errors (concatenate 
                       'list 
                       (car check-values-str) 
                       (car check-values-data) 
                       (car check-values-global)))
          )
         (declare (ignore dummy-parsing-result))
         (declare (ignore dummy-processing-result))
         (values (not all-errors) all-errors (second check-values-global))
         )
   ))

(add-field-attribute :string-export (-> *type-string-export* (-> x :type)))

(defun transformer-field-to-string (obj field)
  (let* (
         (value (-> obj field))
         (export-function (-> field :string-export))
         )
        (funcall export-function value)))

(add-field-attribute :always-export nil)

(defun transformer-export-strings 
  (obj &key with-dummies keep-nil force-convert-bool loopback hide-invisible)
  (iter (for field :in-vector (transformer-fields obj))
  (cond 
    ((-> field :type)
           (let
             ((value
                (when (or (-> obj field) (or (not keep-nil) (-> field :always-export))
                          (and (eq (-> field :type) :bool)
                               force-convert-bool))
                  (transformer-field-to-string obj field))
                ))
             (when loopback
               (set-> obj field value))
             (unless (and hide-invisible (-> field :invisible))
               (collect value))))
    (with-dummies
      (collect "")
      )
    )
  ))

(add-field-attribute :emb-keyword (-> x :code-name))

(defun transformer-export-plist (obj)
  (iter (for field :in-vector (transformer-fields obj))
        (when (-> field :type)
              (collect (-> field :emb-keyword))
              (collect (transformer-field-to-string 
                        obj field)))))

(add-field-attribute :sql-where nil)
(add-field-attribute :sql-add-from nil)
(add-field-attribute :sql-from 
  (when (-> x :sql-add-from) (-> x :db-table-name)))
(add-field-attribute :sql-order-priority nil)
(add-field-attribute :sql-order-ascending t)
(add-field-attribute :sql-group-by nil)
(add-field-attribute :sql-limit nil)

(def-iterate-fields transformer-sql-from ()
  (let ((x (-> field :sql-from))) (when x (collect x))))

(def-iterate-fields transformer-sql-where-clauses ()
  (with-field (:sql-where) (when x (collect x))))

(def-iterate-fields transformer-sql-limit ()
  (with-field (:sql-limit) (when x (collect x))))

(defun transformer-sql-where (obj)
  (apply 'sql-op 'clsql-sys::and (transformer-sql-where-clauses obj)))

(defun transformer-sql-order-by (obj)
  (let* ((clauses (iter (for f :in-vector
                             (transformer-fields obj))
                        (when (-> f :sql-order-priority)
                              (collect 
                               (list 
                                (-> f :sql-order-priority)
                                (if (-> f :sql-order-ascending)
                                    (transformer-field-db-name f)
                                    (sql-wrap 
                                     nil
                                     (transformer-field-db-name f)
                                     'desc
                                     )
                                    ))
                               ))))
         (clauses-sorted (sort clauses '< :key 'car))
         (order-by-list (mapcar 'cadr clauses-sorted))
         (order-by-clause (when order-by-list 
                                (list :order-by order-by-list)))
         )
        order-by-clause
        ))

(defun transformer-sql-group-by (obj)
  (let* ((x (iter (for f :in-vector (transformer-fields obj))
                  (when (-> f :sql-group-by)
                        (collect 
                         (transformer-field-db-name f)))))
         (group-by-clause (when x (list :group-by x))))
        group-by-clause))

(defun transformer-query (obj)
  (apply 'sql-select-list-query 
         (transformer-db-field-names obj)
         :from
         (transformer-sql-from obj)
         :where
         (transformer-sql-where obj)
   :limit
         (transformer-sql-limit obj)
         (append
          (transformer-sql-order-by obj)
          (transformer-sql-group-by obj))))

(add-field-attribute :html-input-type :text)
(add-field-attribute :html-upload-storage nil)
(add-field-attribute :upload-file-namer nil)
(add-field-attribute :html-name 
  (string-downcase (symbol-name (-> x :code-name))))
(add-field-attribute :html-display-name (-> x :display-name))
(add-field-attribute :html-attributes nil)

(defun hunchentoot-ensure-utf8 (value)
  (if (equal hunchentoot:*hunchentoot-default-external-format* hunchentoot::+utf-8+)
    value
    (recast-string-encoding value :latin-1 :utf-8)
  )
)

(def-iterate-fields transformer-http-post-collect ()
  "Iterates over given POST parameters and collects them into a data-transformer" ; captures 'field and 'obj variables
  (let ((ht-parameter (ht:post-parameter (-> field :html-name))))
    (cond
      ((and (null ht-parameter) (eq (-> field :html-input-type) :file))
        t
      )
      ((null ht-parameter)
        (set-> obj field nil)
      )
      ((eq (-> field :html-input-type) :checkbox)
        (set-> obj field "t")
      )
      ((eq (-> field :html-input-type) :onlycheckbox)
        (set-> obj field "t")
      )
      ((and (eq (-> field :html-input-type) :file) (stringp ht-parameter))
        (error "HTML forms with file fields should specify enctype=\"multipart/form-data\"")
      )
      ((eq (-> field :html-input-type) :file)
        (when
          (or
            (pathnamep (-> ht-parameter 0))
            (not (-> field :upload-keep-by-default))
          )
          (if (-> field :html-upload-storage)
              (let*
                  (
                   (fun-ex 
                    (or 
                      (-> field :upload-file-namer)
                      (lambda 
                       (name obj field) 
                       (let*
                         (
                          (basename (cl-ppcre:regex-replace "[.].*" name ""))
                          (ext (cl-ppcre:regex-replace "[^.]*[.]" name ""))
                          )
                         (format 
                           nil "~a.~a.XXXXXX.~a"
                           basename "pending" ext)
                         )
                       )
                    ))
                   (namer-ex (when fun-ex 
                    (funcall fun-ex ;(-> field :upload-file-namer)
                      (-> ht-parameter 1)
                      obj
                      field
                    )))
                   (ext-ex (when (and namer-ex (cl-ppcre:scan ".+XXXXXX\.+$" namer-ex))
                    (cl-ppcre:regex-replace ".+XXXXXX(\..+)$" namer-ex "\\1") 
                    ))
                   (namer-ex 
                     (if ext-ex
                         (cl-ppcre:regex-replace (format nil "~a$" ext-ex) 
                                                 namer-ex 
                                                 "")
                         namer-ex
                      ))
                   )
            #+sbcl(multiple-value-bind 
              (fd name)
              (progn
                (ensure-directories-exist (concatenate 'string 
                  (multifuncall (-> field :html-upload-storage))
                  "/")
                )
                   (sb-posix:mkstemp (concatenate 'string
                     (multifuncall (-> field :html-upload-storage))
                      (if fun-ex
                          namer-ex
                          (concatenate 'string
                            (let*
                              (
                                (fn (car (last (cl-utilities:split-sequence #\\
                                  (-> ht-parameter 1)
                                ))))
                              )
                              (hunchentoot-ensure-utf8 fn)
                            )
                            "-XXXXXX"
                          )
                       )
                  ))
              )
              (setf name (concatenate 'string name ext-ex))
              (sb-posix:close fd)
              (sb-ext:run-program "cp"
                `(,(namestring (-> ht-parameter 0)) ,name)
                :search t
                :output t
              )
              (sb-posix:unlink (namestring (-> ht-parameter 0)))
              (set-> obj field name)
            ))
            #-sbcl (set-> obj field (namestring (-> ht-parameter 0)))
          )
        )
      )
      (t (set-> obj field (when (-> field :type)
        (hunchentoot-ensure-utf8 ht-parameter)
      )))
    )
  )
)

(add-field-attribute :invisible nil)

(def-iterate-fields transformer-data-env (&key (raw t))
  (when (-> field :type)
        (collect 
         `(
           :display-name ,(-> field :display-name)
           :code-name ,(-> field :code-name)
           :content ,(if raw (-> obj field)
                         (transformer-field-to-string
                          obj field))
           :type ,(-> field :type)
           :invisible ,(-> field :invisible)
           ))))

(add-field-attribute :content "")
(add-field-attribute :html-content (-> x :content))
(add-field-attribute :tex-content (-> x :content))
(add-field-attribute :options nil)

(defun field-extra-render-env (obj field)
  (declare (ignorable obj))
  (case (-> field :html-input-type)
        ((:checkbox) 
         `(
           :display-name ,(-> field :display-name) 
           ))
        ((:onlycheckbox) 
         `(
           :display-name ,(-> field :display-name) 
           ))
        ((:raw-output :raw-output-required) 
         `(
           :content ,(-> field :content) 
           :html-content ,(-> field :html-content)
           :tex-content ,(-> field :tex-content)
           ))
        ((:select) 
         `(:options 
           ,(iter (for x in (multifuncall (-> field :options)))
                  (for xl :=  (if (listp x) x (list x x)))
                  (collect 
                   `(
                     :code-name ,(first xl)
                     :display-name ,(second xl))))))
        ((:radiobutton) 
         `(:options 
           ,(iter (for x in (multifuncall (-> field :options)))
                  (for xl :=  (if (listp x) x (list x x)))
                  (collect 
                   `(
                     :code-name ,(first xl)
                     :display-name ,(second xl)
                     :html-attributes ,(-> field :html-attributes)
                     )))
           :br
           ,(-> field :br)
           ))
        ((:radiobutton-multy) 
         `(:options 
           ,(iter (for x in (multifuncall (-> field :options)))
                  (for xl :=  (if (listp x) x (list x x)))
                  (collect 
                   `(
                     :code-name ,(first xl)
                     :display-name ,(second xl)
                     :html-attributes ,(-> field :html-attributes)
                     )))
           :br
           ,(-> field :br)
           ))
        ((:radiobutton-multy-main) 
         `(:options 
           ,(iter (for x in (multifuncall (-> field :options)))
                  (for xl :=  (if (listp x) x (list x x)))
                  (collect 
                   `(
                     :code-name ,(first xl)
                     :display-name ,(second xl)
                     :html-attributes ,(-> field :html-attributes)
                     )))
           :br
           ,(-> field :br)
           ))
  ((:file)
   `(
     :hide-file-value ,(-> field :hide-file-value)
     ))
        ))

(add-field-attribute :html-entries-literal nil)

(defun field-html-render-input (obj field)
  (when (-> field :type)
        (when (-> field :html-input-type)
              (cl-emb:execute-emb 
               (make-pathname 
                :type "emb" 
                :name (string-downcase (string 
                                        (-> field :html-input-type)))
                :directory (append (source-directory)
                                   '("field-html-render")))
               :env 
               `(
                 :html-name ,(-> field :html-name)
                 :value ,(-> obj field)
                 :extra-data ,(field-extra-render-env obj field)
                 :html-attributes ,(-> field :html-attributes)
     :entries-literal ,(-> field :html-entries-literal)
                 )))))

(add-field-attribute :extra-html-data nil)

(def-iterate-fields transformer-html-env ()
  "Produces an environment that can be fed into an HTML template to represent the data stored in the transformer for the sake of, for example, displaying the data along with the error messages if the data doesn't fit the required format and/or allowing to edit the data."
  (when (-> field :type)
        (for invisible :=
             (or
              (-> field :invisible)
              (find (-> field :html-input-type) 
                    '(:hidden :raw-output))))
        (collect
         `(
           ,@(multifuncall (-> field :extra-html-data))
           :display-name ,(-> field :html-display-name)
           :html-display-name-literal ,(-> field :html-display-name-literal)
           :verify-display-name ,(-> field :verify-display-name)
           :visible ,(not invisible)
           :invisible ,invisible
           :code-name ,(-> field :code-name)
           :content ,(-> obj field)
           :type ,(-> field :type)
           :html-code ,(field-html-render-input obj field)
           :errors ,(iter 
                     (for e in (-> (data-transformer-errors obj)
                                   (-> field :code-name))) 
                     (collect `(:message ,e)))
           :warnings ,(iter 
                     (for e in (-> (data-transformer-warnings obj)
                                   (-> field :code-name))) 
                     (collect `(:message ,e)))
     :html-input-type ,(-> field :html-input-type)
     :hide-file-value ,(-> field :hide-file-value)
           ))))

(defun transformer-html-env-split (env)
  (append
   (iter
    (for i in env)
    (collect (getf i :code-name))
    (collect i)
    )
   `(
     :all-fields ,env
     )
   ))

(def-iterate-fields transformer-correct-fields-alist ()
  (when (and
         (-> field :type)
         (null
          (-> (data-transformer-errors obj)
              (-> field :code-name)))
         )
        (collect 
         `(
           ,(-> field :code-name)
           ,(-> obj field)))))

(add-field-attribute :csv-no-conversion nil)

(def-iterate-schema transformer-schema-to-csv ()
  (if (or 
  (-> entry :skip-push-csv)
  (-> entry :skip-transfer-csv)
  )
    (progn
      (set-> entry :push-self nil)
      (set-> entry :push-attribute nil)
      )
    (progn
      (set-> entry :push-self t)
      )
    )
  (if (or
  (-> entry :skip-pull-csv)
  (-> entry :skip-transfer-csv)
  )
    (progn
      (set-> entry :pull-self nil)
      (set-> entry :pull-attribute nil)
      )
    (progn
      (set-> entry :pull-self t)
      )
    )
  (cond 
   ((and 
     (eq (-> entry :type) :date)
     (null (-> entry :csv-no-conversion))
    )
    (set-> entry :type :int)
    (set-> entry :pull-self nil)
    (set-> entry :push-self nil)
    (set-> entry :string-export (lambda (x) (if x (format nil "~2,'0d" x) "")))
    (let* ((old-name (symbol-name (-> entry :code-name)))
           (old-display-name (-> entry :display-name))
           (suffix-set-list `((" (день)" "-DAY" nil third 
                                         (
                                          (:html-attributes 
                                           ((:name :size :value 3)))
                                          ))
                              (" (месяц)" "-MONTH" nil second 
                                          ((:html-input-type :select)
                                           (:options (
                                                      nil
                                                      ("01" "январь")
                                                      ("02" "февраль")
                                                      ("03" "март")
                                                      ("04" "апрель")
                                                      ("05" "май")
                                                      ("06" "июнь")
                                                      ("07" "июль")
                                                      ("08" "август")
                                                      ("09" "сентябрь")
                                                      ("10" "октябрь")
                                                      ("11" "ноябрь")
                                                      ("12" "декабрь")
                                                      ))
                                           ))
                              (" (год)" "-YEAR" t first 
                                        (
                                         (:html-attributes 
                                          ((:name :size :value 5)))
                                         ))
                              ))
           (field-symbols nil)
           )
          (loop for suffix-set in suffix-set-list
                do
                (set-> entry :display-name 
                       (concatenate 'string old-display-name
                                    (first suffix-set)))
                (push (intern 
                       (concatenate 'string old-name
                                    (second suffix-set))
                       (find-package :keyword)) 
                      field-symbols)
                (set-> entry :code-name 
                       (car field-symbols))
                (set-> entry :pull-function 
                       (let* ((fcn (-> field :code-name))
                              (chooser (fourth suffix-set))
                              )
                             (lambda (x) 
                                     (funcall chooser
                                              (cl-utilities:split-sequence
                                               #\-
                                               (-> x fcn))))))
                ;(iter (for extra-attr in (fifth suffix-set))
                ;      (set-> entry (first extra-attr) (second extra-attr))
                ;      )
                (when (third suffix-set)
                      (set-> entry :global-verification 
                             (let
                              ((cn (-> entry :code-name)))
                              (lambda 
                               (x) 
                               (or (not (-> x cn))
                                   (apply 'valid-date 
                                          (let*
                                           ((date-elements
                                             (mapcar (lambda (s) (-> x s))
                                                     field-symbols)))
                                           date-elements
                                           ))))))
                      (set-> entry :global-verification-error "Дата некорректна")
                      (set-> entry :push-attribute (-> field :code-name))
                      (set-> entry :push-function
           (lambda (o)
             (when
         (every 
           'identity
           (mapcar (curry '-> o)
             field-symbols))
         (apply 'format nil "~4,'0d-~2,'0d-~2,'0d"
          (mapcar (curry '-> o)
            field-symbols)))))
                      )

                
                (collect (append 
                          (fifth suffix-set) 
                          entry 
                          (fifth suffix-set))))
          ))
   ((eq (-> entry :type) :timestamp)
    (set-> entry :type :string)
    (set-> entry :pull-self nil)
    (set-> entry :push-self nil)
    (set-> entry :string-export (lambda (x) (if x (format nil "~a" x) "")))
    (let* ((old-name (symbol-name (-> entry :code-name)))
           (old-display-name (-> entry :display-name))
           (suffix-set-list `((" (время)" "-TIME" nil second 
                                         (
                                          (:html-attributes 
                                           ((:name :size :value 6)))
                                          ))
                              (" (дата)" "-DATE" t first 
                                        (
                                         (:html-attributes 
                                          ((:name :size :value 11)))
                                         ))
                              ))
           (field-symbols nil)
           )
          (loop for suffix-set in suffix-set-list
                do
                (set-> entry :display-name 
                       (concatenate 'string old-display-name
                                    (first suffix-set)))
                (push (intern 
                       (concatenate 'string old-name
                                    (second suffix-set))
                       (find-package :keyword)) 
                      field-symbols)
                (set-> entry :code-name 
                       (car field-symbols))
                (set-> entry :pull-function 
                       (let* ((fcn (-> field :code-name))
                              (chooser (fourth suffix-set))
                              )
                             (lambda (x) 
                                     (funcall chooser
                                              (ignore-errors (cl-ppcre:split "T" 
                                                (subseq (format nil "~a" 
                                                                (local-time:unix-to-timestamp (-> x fcn))) 0 16)
                                                ))
                                              ))))
                ;(iter (for extra-attr in (fifth suffix-set))
                ;      (set-> entry (first extra-attr) (second extra-attr))
                ;      )
                (when (third suffix-set)
                      (set-> entry :push-attribute (-> field :code-name))
                      (set-> entry :push-function
           (lambda (o)
             (when
         (every 
           'identity
           (mapcar (curry '-> o)
             field-symbols))
       (format nil "~a"
         (ignore-errors (local-time:timestamp-to-unix (local-time:parse-timestring 
         (apply 'format nil "~10,'0dT~5,'0d:01+03:00" 
          (mapcar (curry '-> o)
            field-symbols))
         )))
       )
         )))
                      )
                (collect (append 
                          (fifth suffix-set) 
                          entry 
                          (fifth suffix-set))))
          ))
   ((eq (-> entry :type) :rusolymp-teacher)
    (set-> entry :type :string)
    (set-> entry :pull-self nil)
    (set-> entry :push-self nil)
;    (set-> entry :string-export (lambda (x) (if x (format nil "~2,'0d" x) "")))
    (let* ((old-name (symbol-name (-> entry :code-name)))
           (old-display-name (-> entry :display-name))
           (suffix-set-list `(("Фамилия" "-LAST-NAME" nil first)
                              ("Имя" "-FIRST-NAME" nil second)
                              ("Отчество" "-MIDDLE-NAME" nil third)
                              ("Место работы" "-WORK" nil fourth)
                              ("Должность" "-JOB" nil fifth)
                              ("Роль" "-ROLE" nil sixth 
                                          ((:html-input-type :select)
                                           (:options (""
                                                      "учитель моей школы"
                                                      "учитель другой школы"
                                                      "преподаватель вуза"
                                                      "тренер команды Москвы"
                                                      "другое"
                                                      ))
                                           ))
                              ("Балл (от 0 до 100)" "-SCORE" t seventh
                                          ((:type :int)
                                           (:data-verification ,(lambda (x) (or (not x) (> 101 x -1))))))
                              ))
           (field-symbols nil)
           )
          (loop for suffix-set in suffix-set-list
                do
                (set-> entry :display-name 
                       (concatenate 'string (first suffix-set)
                                            old-display-name))
                (push (intern 
                       (concatenate 'string old-name
                                    (second suffix-set))
                       (find-package :keyword)) 
                      field-symbols)
                (set-> entry :code-name 
                       (car field-symbols))
                (set-> entry :pull-function 
                       (let* ((fcn (-> field :code-name))
                              (chooser (fourth suffix-set))
                              )
                             (lambda (x) 
                                     (funcall chooser
                                              (cl-utilities:split-sequence
                                               #\#
                                               (-> x fcn))))))
                ;(iter (for extra-attr in (fifth suffix-set))
                ;      (set-> entry (first extra-attr) (second extra-attr))
                ;      )
                (when (third suffix-set)
                      (set-> entry :push-attribute (-> field :code-name))
                      (set-> entry :push-function
           (lambda (o)
             (when
         (every 
           'identity
           (mapcar (curry '-> o)
             field-symbols))
         (format nil "~{~a#~}"
          (reverse (mapcar (curry '-> o)
            field-symbols))))))
                      )
                (collect (append 
                          (fifth suffix-set) 
                          entry 
                          (fifth suffix-set))))
          ))
   (t (collect entry))))

(def-iterate-schema transformer-schema-disable-table 
  (table-name &optional value)
  (when 
   (equal (-> entry :db-table-name) table-name)
   (set-> entry :db-field-type nil)
   (set-> entry :string-parsing (constantly value)))
  (collect entry))

(def-iterate-schema transformer-schema-only-table (table-name)
  (when 
   (equal (-> entry :db-table-name) table-name)
   (collect entry)
   )
  )

(def-iterate-schema transformer-schema-disable-fields (&rest field-names)
  (unless
   (find (-> entry :code-name) field-names)
   (collect entry)))

(def-iterate-schema transformer-schema-edit-field (code-name processor)
  (if
   (equal code-name (-> entry :code-name))
   (collect (funcall processor entry))
   (collect entry)))

(def-iterate-schema 
  transformer-schema-insert-field (position code-name new)
  (if 
    (equal code-name (-> entry :code-name))
    (case position
      ((:before)   (collect new) (collect entry))
      ((:after)    (collect entry) (collect new))
      ((:instead)  (collect new))
      )
    (collect entry)))

(defun transformer-schema-reorder-fields (schema new-order 
  &key (drop-extra-fields nil))
  (let*
   (
    (partially-reordered 
     (iter (for x in new-order)
           (collect 
            (find-if 
             (lambda (y) (equal (-> y :code-name) x))
             schema))))
    (unselected
     (unless drop-extra-fields
             (iter (for x in schema)
                   (unless 
                    (find (-> x :code-name) new-order :test 'equal)
                    (collect x)))))
    )
   (append 
    partially-reordered
    unselected
    )
   ))

(defun transformer-equal (dt1 dt2 fields &key (test 'equal))
  (iter (for f in fields)
        (when (not (funcall test (-> dt1 f) (-> dt2 f)))
              (return (values nil f (-> dt1 f) (-> dt2 f))))
        (finally (return (values t nil nil)))))

(defun transformer-schema-query-db (schema &key 
  (kind 'transformer-html-env)
  (post-processing 'transformer-html-env-split)
  (post-load 'identity)
  )
  (let*
   (
    (transformer (data-transformer schema))
    (empty-line (funcall 
                 post-processing 
                 (funcall kind transformer)))
    (query (transformer-query transformer))
    (data (clsql:query query))
    (data-envs 
     (iter
      (for x in data)
      (data-transformer-load-ordered-strings 
       transformer x
       :skip-check t
       :format-first t)
      (funcall post-load transformer)
      (collect 
       (funcall post-processing 
                (funcall kind transformer)))
      ))
    )
   (values data-envs empty-line transformer data query)))

(def-iterate-fields transformer-find-warnings ()
  (when (-> field :warnings)
        (set->
         (data-transformer-warnings obj)
         (-> field :code-name)
         (iter (for w in (-> field :warnings))
               (when (funcall (first w) (-> obj field))
                     (collect (second w)))
               ))
        )
  (when (-> field :global-warnings)
        (iter (for w in (-> field :warnings))
              (when (funcall (first w) (-> obj field))
                    (set->
                     (data-transformer-warnings obj)
                     (third w)
                     (append
                      (->
                       (data-transformer-warnings obj)
                       (third w)
                       )
                      (list (second w))
                      )
                     ))
              ))
  )

(defun transformer-change-field-attribute (obj field attr value)
  (let*
   (
    (meta (data-transformer-meta obj))
    (fdesc (-> meta field))
    )
   (set-> fdesc 'attributes 
          (with-updated-entry (-> fdesc 'attributes) attr value))
   (set-> fdesc 'value-cache (make-hash-table))
   ))

(add-field-attribute :default-special nil)

(def-iterate-fields transformer-default-fill (&optional presentp)
  (or
   (and presentp (funcall presentp obj field))
   (set-> obj field 
          (let* ((fn (-> field :default-function))) 
                (and fn (funcall fn obj))))))

(defun transformer-query-and-prepare-by-schema
  (schema querier 
    &key 
        (pre-processing (constantly nil))
        (post-processing 'identity)
        (for-html nil)
        (debug nil)
        (a-i nil)
    )
  (let*
   (
    (dt (data-transformer schema))
    (q (transformer-query dt))
    (data-raw (funcall querier q))
    (fields (transformer-data-env dt))
    (content
     (loop 
      for d in data-raw
      do
      (progn 
       (data-transformer-load-ordered-strings 
        dt d
        :format-first t :skip-check t)
       (funcall pre-processing dt)
       )
      collect
      (if a-i 
        (list :entry
            (funcall post-processing
                     (if for-html
                         (transformer-html-env dt )
                         (transformer-data-env dt :raw nil )
                     ))
              :tt
            (when a-i (funcall a-i
                                 (if for-html
                                     (transformer-html-env dt )
                                     (transformer-data-env dt :raw nil )
                                 )))
            )
        (list :entry
            (funcall post-processing
                     (if for-html
                         (transformer-html-env dt )
                         (transformer-data-env dt :raw nil )
                     ))
            )
      )
     ))
    )
   (when debug (format *standard-output* "Query: ~s~%" q))
   (list fields content)
   ))

(add-field-attribute :br nil)
(add-field-attribute :html-display-name-literal nil)
(add-field-attribute :verify-display-name nil)

(add-field-attribute :hash-table-path nil)
(add-field-attribute :hash-table-read-path (-> x :hash-table-path))
(add-field-attribute :hash-table-write-path (-> x :hash-table-path))

(add-field-attribute :hash-table-list-path nil)
(add-field-attribute :hash-table-list-read-path  (-> x :hash-table-list-path))
(add-field-attribute :hash-table-list-write-path (-> x :hash-table-list-path))

(def-iterate-fields transformer-hashtable-collect (src)
  (set-> obj field 
   (and
     (-> field :hash-table-read-path)
     (apply '--> src (funcall (or (-> field :default-function) 
          (constantly nil)))
      (-> field :hash-table-read-path)))))

(defun transformer-hashtable-list-collect (obj src)
  (let*
    (
     (data-lists
       (iter
   (for field :in-vector (transformer-fields obj))
   (collect
     (and
       (-> field :hash-table-list-read-path )
       (apply '--> src (funcall (or (-> field :default-function) 
            (constantly nil)))
        (-> field :hash-table-list-read-path ))))))
     (len (length (find-if 'listp data-lists)))
     (ensured-lists
       (iter 
   (for l in data-lists)
   (collect
     (if (and l (listp l))
       l (make-list len :initial-element l)))
   ))
     (res (apply 'mapcar 'list ensured-lists))
     )
    res
    ))

(add-field-attribute :flexible-pull-options 
         (list (-> x :code-name) (-> x :display-name)))

(def-iterate-fields 
  transformer-flexible-pull (src)
  (iter
    (with flag := nil)
    (while (not flag))
    (for k in (-> field :flexible-pull-options))
    (when (has-entry src k)
      (set-> obj field (-> src k))
      (setf flag t))))

(defun edit-schema-field-attribute (schema field attribute value)
  (transformer-schema-edit-field schema field (lambda (y)
    (set-> y attribute value)
  ))
)
