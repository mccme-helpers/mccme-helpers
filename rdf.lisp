(mccme-helpers/packages:file mccme-helpers/rdf
  :use (
        mccme-helpers/parsetype
        mccme-helpers/lisp
        :iterate
        )
  :export (
           #:rdf-value
	   #:rdf-literal
           #:n-triple
           #:n-triple-url-value
           #:n-triples-with-base-url
           #:rdf-list
           #:maybe-with-rdf-ns
           #:*rdf-prefix*
           #:rdf-label
           )
  )

(def-and-set-var *rdf-prefix* "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
(def-and-set-var *xsd-prefix* "http://www.w3.org/2001/XMLSchema#")

(defun value-rdf-type (v)
  (cond
   ((integerp v) "decimal")
   ((typep v 'double-float) "double")
   ((listp v) (first v))
   (t "string")
   ))

(defun real-rdf-value (v)
  (if
   (listp v)
   (second v)
   v
   ))

(defun rdf-literal (v)
  (cond
    ((listp v) 
     (format 
       nil 
       "\"~a\"^^<~a~a>" 
       (escape-unicode-bmp (format nil "~a" (second v)))
       *xsd-prefix* (string-downcase (first v))))
    ((atom v)
     (format 
       nil 
       "\"~a\"^^<~a~a>" 
       (escape-unicode-bmp (format nil "~a" v)) 
       *xsd-prefix* (value-rdf-type v)))
    (t "")))

(defun is-url (v)
  (cond
   ((listp v) (eq (first v) :url))
   ((stringp v) (cl-ppcre:scan "^[a-z]+:" v))
   (t nil)
   ))

(defun is-blank (v)
  (cond
   ((listp v) (eq (first v) :blank))
   ((stringp v) (cl-ppcre:scan "^_:" v))
   (t nil)
   ))

(defun rdf-blank (v)
  (cond
   ((listp v) (format nil "_:~a" (second v)))
   (t v)
   ))

(defun url-content (v) (if (listp v) (second v) v))

(defun rdf-url (v)
  (format nil "<~a>" (url-content v)))

(defun is-prefixed (v)
  (cond
   ((listp v) (eq (first v) :prefixed))
   (t nil)
   ))

(defun rdf-prefixed (v)
  (cond
    ((and (listp v) (= (length v) 2)) (second v))
    ((and (listp v) (= (length v) 3)) 
     (format nil "~a:~a" (second v) (third v)))
    (t "")))

(defun is-var (v) (symbolp v))
(defun rdf-var (v) 
  (cond
    ((eq :* v) "*")
    (t (format nil "?~a" (symbol-name v)))
    )
  )

(defun is-escape (v)
  (cond
    ((listp v) (eq (first v) :escape))
    (t nil)))
(defun rdf-escape (v) (second v))

(defun is-path (v)
  (cond
    ((listp v) (eq (First v) :path))
    (t nil)))
(defun rdf-path (v)
  (format nil "(~{~a ~})"
	  (mapcar 'rdf-value (second v))))

(defun rdf-value (v)
  (cond
   ((is-url v) (rdf-url v))
   ((is-blank v) (rdf-blank v))
   ((is-prefixed v) (rdf-prefixed v))
   ((is-var v) (rdf-var v))
   ((is-escape v) (rdf-escape v))
   ((is-path v) (rdf-path v))
   (t (rdf-literal v))
   ))

(defun n-triple (o p v)
  (format nil "~a ~a ~a . ~%" 
          (rdf-value o)
          (rdf-value p)
          (rdf-value v)
          ))

(defun maybe-with-rdf-ns (ns o)
  (if ns (concatenate 'string (url-content ns) 
                      (escape-for-url o)) o))

(defun n-triple-url-value (o p v ns-v)
  (let*
   (
    (interim (list :url (maybe-with-rdf-ns ns-v v)))
    )
   (concatenate 
    'string 
    (n-triple o p interim)
    (n-triple interim 
              (list 
               :url (concatenate 'string *rdf-prefix* "value"))
              v)
    )
   )
  )

(defun n-triples-with-base-url (o ps vs ns-p &key ns-v ns-o)
  (apply 'concatenate 'string
         (iter
          (for p in ps)
          (for v in vs)
          (with oe := (maybe-with-rdf-ns ns-o o))
          (for pe := (maybe-with-rdf-ns ns-p p))
          (collect 
           (if ns-v
               (n-triple-url-value oe pe v ns-v)
               (n-triple oe pe v)
               ))
          )))

(let ((rdf-blank-counter 0)) 
  (defun new-blank-node (name)
         (list 
          :blank
          (format nil "MCCMEhelpersRDFblank~a~a"
                  name (incf rdf-blank-counter)
                  ))))

(defun rdf-list (vs)
  (let*
   (
    (bs (loop for x in vs collect (new-blank-node "RDFlistNode")))
    (next-lines (iter 
                 (for pb initially nil then b)
                 (for b in bs)
                 (when pb
                       (collect
                        (n-triple
                         pb
                         (concatenate
                          'string *rdf-prefix* "rest")
                         b
                         ))
                       )
                 ))
    (last-line (list (n-triple (first (last bs))
                               (concatenate
                                'string *rdf-prefix* "rest")
                               (concatenate
                                'string *rdf-prefix* "nil")
                               )
                     ))
    (value-lines (iter
                  (for b in bs)
                  (for v in vs)
                  (collect
                   (n-triple
                    b
                    (concatenate 'string *rdf-prefix* "first")
                    v
                    ))
                  ))
    (type-line (list (n-triple (first bs) 
                               (maybe-with-rdf-ns *rdf-prefix* "type") 
                               (maybe-with-rdf-ns *rdf-prefix* "List"))))
    (res (apply 'concatenate 'string
                (concatenate 'list
                             next-lines last-line value-lines
                             type-line)
                ))
    )
   (list res (first bs))
   ))

(defun rdf-label (v &rest parts)
  (n-triple v (concatenate 'string *rdf-prefix* "label") 
            (apply 'concatenate 'string parts)))
