(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :mccme-helpers/trivial)
          (defpackage :mccme-helpers/trivial (:use :cl)))
  (in-package :mccme-helpers/trivial)
  (cond
    ((find-package :sb-posix) (import (find-symbol "GETENV" :sb-posix)))
    ((find-package :ext)      (import (find-symbol "GETENV" :ext     )))
    ((find-package :ccl)      (import (find-symbol "GETENV" :ccl     )))
    ((find-package :si)       (import (find-symbol "GETENV" :si      )))
  )
)

(mccme-helpers/packages:file mccme-helpers/trivial
  :use (:iterate)
  :export (
    #:random-of
    #:random-between
    #:uni-<
    #:uni-<!
    #:sort-by
    #:temp-file-name
    #:temp-dir-name
    #:file-contents
    #:file-text
    #:trace-execution
    #:only
    #:getenv
    #:trace-progn
    #:trace-let*
    #:trace-let*-if
    #:trace-data-change
    #:pad-array
    #:pad-list
    #:*whitespace-list*
    #:emptyp
    #:nonemptyp
    #:flood
    #:take
    #:take-if
    #:setf-to-return
    #:makemultiarray
    #:submultiarray
    #:split-into-chunks
    #:join-list
    #:multiple-value-second
    #:complain-errors
  )
)

(defun random-of (l)
  (elt l (random (length l))))

(defun random-between (d u)
  (+ d (random 
        (- u d 
           (if (and 
                (integerp u)
                (integerp d)
                )
            -1 0)))))

(defun uni-< (x y)
  (cond
   ((null x) t)
   ((null y) nil)
   ((and
    (numberp x)
    (numberp y))
    (< x y))
   ((and (stringp x) (stringp y))
    (string< x y))
   ((and (listp x) (listp y))
    (iter
     (for a in (append x '(nil)))
     (for b in (append y '(nil)))
     (while (equal a b))
     (finally 
      (return (uni-< a b)))))
   ))

(defun uni-<! (l)
  (lambda 
    (x y)
    (cond
      ((null x) t)
      ((null y) nil)
      ((and
         (numberp x)
         (numberp y))
       (< x y))
      ((and (stringp x) (stringp y))
       (string< x y))
      ((and (listp x) (listp y))
       (iter
         (for a in (append x '(nil)))
         (for b in (append y '(nil)))
         (for sign in (append l (make-list (length x) :initial-element t)))
         (while (equal a b))
         (finally 
           (return (equal sign (not (not (uni-< a b))))))))
      )))

(defun sort-by (l f)
  "More or less equivalent to Ruby's Array#sort_by"
  (sort l 'uni-< :key f)
)

#+sbcl (defun temp-file-name (template)
         (let (name-pair)
           (unwind-protect
             (ensure-directories-exist template)
             (loop for k from 0 to 3
                   for try := 
                   (ignore-errors
                      (setf name-pair (multiple-value-list (sb-posix:mkstemp template)))
                      t)
                   when try return nil
                   do (sleep (expt 2 k)))
             (sb-posix:close (first name-pair))
             )
           (second name-pair)))

#+sbcl (defun temp-dir-name (template)
  (sb-posix:mkdtemp template))

(defun file-contents (fn)
  (with-open-file (f fn :element-type '(unsigned-byte 8))
                  (let* ((out-data 
                          (make-array 
                           (file-length f) 
                           :element-type '(unsigned-byte 8))))
                        (read-sequence out-data f)
                        out-data
                        )))

(defun file-text (fn &key (enc :utf-8))
  (with-open-file (f fn :external-format enc)
    (let* ((out-data 
             (make-array 
               (file-length f) 
               :element-type 'character))
           (l (read-sequence out-data f))
           (result (subseq out-data 0 l))
           )
      result
      )))

(defmacro trace-execution (&rest body)
  (let ((res-sym (gensym)))
  `(progn
    (format *error-output* "~%Executing:~%~s~%" ',body)
    (let ((,res-sym ,body))
         (format *error-output* "~%Executed:~%~s~%Result:~%~s~%" ',body ,res-sym)
         ,res-sym))))

(defmacro trace-progn (&rest body)
  (let
   ((res-sym (gensym)))
   `(progn
     ,@(loop for x in body
             collect
             `(progn
                (format *error-output* "Time before: ~a~%"
                        (local-time:now))
                (let ((,res-sym ,x))
                  (format *error-output* "VTrace: ~s~%-> ~s~%" ',x ,res-sym)
                  (format *error-output* "Time after: ~a~%"
                          (local-time:now))
                  ,res-sym)
                ))
     )
   ))

(defmacro trace-let* (assignments &rest body)
  (let
   ((res-sym (gensym)))
   `(let*
     (
      ,@(loop for x in assignments
              collect
              (if
               (consp x)
               `(,(first x) (trace-progn ',(first x) ,(second x)))
               x
               ))
      )
     (trace-progn ,@body)
     )
   ))

(defmacro trace-let*-if (predicate assignments &rest body)
 `(if ,predicate 
	 (trace-let* ,assignments ,@ body)
	 (let* ,assignments ,@ body)))

(defun only (l)
  (assert (= (length l) 1))
  (car l))

(defmacro trace-data-change (name data &rest code)
  (let*
   ((res (gensym)))
   `(progn
     (format *error-output* "~%Tracing data ~s at point named ~a (in):~%[[~%~s~%]]~%~%" ',data ,name ,data)
     (format *error-output* "~%Running code at point named ~a:~%[[~%~s~%]]~%~%" ,name ',code)
     (let
      ((,res ,code))
      (format *error-output* "~%Got result at point named ~a:~%[[~%~s~%]]~%~%" ,name ,res)
      (format *error-output* "~%Tracing data ~s at point named ~a (out):~%[[~%~s~%]]~%~%" ',data ,name ,data)
      ,res
      ))))

(defun pad-array (a n e)
  (let*
    (
     (l (length a))
     )
    (cond
      ((= l 0) (make-array n :initial-element e))
      ((= l n) a)
      ((> l n) (subseq a 0 n))
      (t (concatenate
           'vector a (make-array (- n l) :initial-element e))))
    ))

(defun pad-list (a n e)
  (let*
    ((l (length a)))
    (cond
      ((= l 0) 
       (make-list n :initial-element e))
      ((= l n) a)
      ((> l n) (subseq a 0 n))
      (t (concatenate 'list a (make-list (- n l) :initial-element e))))
    ))

(defparameter *whitespace-list* '(#\Space #\Tab #\Return #\Linefeed 
                                  #\ZERO_WIDTH_NO-BREAK_SPACE #\NO-BREAK_SPACE))

(defun emptyp (x) 
  (or
    (and (symbolp x) (emptyp (symbol-name x)))
    (and (subtypep (type-of x) 'sequence) (= (length x) 0))))

(defun nonemptyp (x)
  (not (emptyp x)))

(defun flood (&optional (amount 1000))
  (loop for x from 1 to amount do (format t "~%")))

(defmacro take (var l pos)
  `(progn
    (setf ,var (nth ,pos ,l))
    (setf ,l (append (subseq ,l 0 ,pos) (subseq ,l (1+ ,pos) (length ,l))))
    ))

(defmacro take-if (var l pred)
  `(take ,var ,l (position-if ,pred ,l)))

(defmacro setf-to-return (var &rest code)
  `(let ((,var)) ,@code ,var))

(defun makemultiarray (e &rest dims)
  (if
    dims
    (make-array (first dims) :initial-element
                (apply 'makemultiarray e (rest dims)))
    e))

(defun submultiarray (arr e &rest dims)
  (if dims
    (map
      'vector
      (lambda (x) (apply 'submultiarray x e (rest dims)))
      (subseq (pad-array arr (second (first dims))
                         (apply 'makemultiarray
                           e (mapcar (lambda (x) (- (apply '- x)))
                                     (rest dims))))
              (first (first dims)))
      )
    arr))

(defun split-into-chunks (lst num)
  "`(split-into-chunks '(1 2 3 4 5 6) 3)` yields `((1 2 3) (4 5 6))`. Works for any sequence."
  (loop
    with res := nil
    for k := 0 then (mod (1+ k) num)
    for x being the elements of lst
    when (= k 0) do (push nil res)
    do (push x (first res))
    finally (return (reverse (mapcar 'reverse res)))))

(defun join-list (l separator)
  "`(join-list (list 1 2 3) \", \")` yields `\"1, 2, 3\"`"
  (format nil (concatenate 'string "~{~A~^" separator "~}") l))

(defmacro multiple-value-second (body)
  `(multiple-value-bind (_ b) ,body
    (declare (ignore _)) b))

(defmacro complain-errors (&body body)
  (let* ((res (gensym)) (err (gensym)))
    `(multiple-value-bind (,res ,err)
      (ignore-errors ,@ body)
      (or ,res
          (progn 
            (format *error-output*
                    "Error in attempt:~%~s~%~a~%"
                    ,err ,err)
            nil)))))
