(mccme-helpers/packages:file mccme-helpers/db-versioning
  :use (
        mccme-helpers/struct
        mccme-helpers/basic-db
        mccme-helpers/data-transformer
        mccme-helpers/lisp
        mccme-helpers/trivial

        :iterate
        :clsql
        )
  :export (
           #:*versioning-db-data-schema*
           #:*versioning-db-schema*
           #:apply-changeset
           #:log-table-name
           #:tracker-extras
           #:tracked-extras
           #:tracked-schema
           #:create-changeset
           #:create-change-entry
           #:just-create-version
           #:apply-logged-changeset
    )
  )

#.(sql-on)

(defvar *versioning-db-data-schema*)
(setf   *versioning-db-data-schema* `(
  ((:code-name :id)
   (:type :int)
   (:db-field-type '(serial))
   )
  (;; Unix timestamp in nigh-infinite 
   (:code-name :timestamp)
   (:type :int)
   (:db-field-type (decimal))
   )
  ((:code-name :comment)
   (:type :string)
   )
  ((:code-name :status)
   (:type :string)
   )
  ((:code-name :change-applied)
   (:type :bool)
   )
  ((:code-name :data-source)
   (:type :string)
   )
  ((:code-name :hash)
   (:type :string)
   )
  ((:code-name :table-name)
   (:type :string)
   )
  (;; The fields affected by the changeset
   ;; ","-separated
   (:code-name :affected-fields)
   (:type :string)
   )
  ))

(defvar *versioning-db-schema*)
(setf *versioning-db-schema* `(
  (changesets
     ,(transformer-db-fields (data-transformer *versioning-db-data-schema*))
     )
    ))

(defun log-table-name (table-name)
  (concatenate-symbol-names table-name '-log))

(defun tracker-extras (table-name)
  `(
    ((:code-name :latest)
     (:type :bool)
     )
    ((:code-name :previous)
     (:type :reference)
     (:db-referenced-table ,(log-table-name table-name))
     )
    ((:code-name :previous-applied)
     (:type :reference)
     (:db-referenced-table ,(log-table-name table-name))
     )
    ((:code-name :related-data-entry)
     (:type :reference)
     (:db-referenced-table ,table-name)
     )
    ((:code-name :containing-changeset)
     (:type :reference)
     (:db-referenced-table :changesets)
     )
    ((:code-name :change-status)
     (:type :string)
     )
    ((:code-name :change-applied)
     (:type :bool)
     )
    ((:code-name :status)
     (:type :string)
     )
    ((:code-name :existing)
     (:type :bool)
     )
    ))

(defun tracked-extras (table-name)
  `(
    (
     (:code-name :record-status)
     (:type :string)
     )
    (
     ; ignore this otherwise
     (:code-name :existing)
     (:type :bool)
     )
    (
     (:code-name :last-change)
     (:type :reference)
     (:db-referenced-table ,(log-table-name table-name))
     )
    (
     (:code-name :first-change)
     (:type :reference)
     (:db-referenced-table ,(log-table-name table-name))
     )
    ((:code-name :status)
     (:type :string)
     )
    ))

(defun tracked-schema (data-schema table-name &key logical)
  (let* 
   (
    (real-data-schema 
      (append
        data-schema
        (tracked-extras table-name)
        ))
    (log-data-schema
      (append 
        data-schema
        (tracker-extras table-name)
        )
      )
    (db-schema
      (if logical
        `(
          (,table-name 
            ,real-data-schema)
          (,(log-table-name table-name)
            ,log-data-schema)
          )
        `(
          (,table-name 
            ,(transformer-db-fields (data-transformer real-data-schema)))
          (,(log-table-name table-name)
            ,(transformer-db-fields (data-transformer log-data-schema)))
          )
        )
      )
    )
   (values
    db-schema
    real-data-schema
    log-data-schema
    )))

(defun update-chain (chain-table chain-id chain-head-field
  old-head-id
  element-table element-id element-previous-link
  head-mark chain-start-field old-chain-start
  )
  (clsql:update-records 
   (sql-value :table element-table)
   :av-pairs `(
               (,(sql-value :attribute element-previous-link)
                 ,old-head-id)
               )
   :where [= [id] element-id]
   )
  (clsql:update-records 
   (sql-value :table chain-table)
   :av-pairs `(
               (,(sql-value :attribute chain-head-field) ,element-id)
               (,(sql-value :attribute chain-start-field) 
                 ,(or old-chain-start element-id))
               )
   :where [= [id] chain-id]
   )
  (when head-mark
        (clsql:update-records 
         (sql-value :table element-table) 
         :av-pairs `((
                      ,head-mark
                      "t"
                      ))
         :where [= [id] element-id]
         )
        )
  (when head-mark
        (clsql:update-records 
         (sql-value :table element-table) 
         :av-pairs `((
                      ,head-mark
                      "f"
                      ))
         :where [= [id] old-head-id]
         )
        )
  )

(defun insert-empty-record 
  (table change-id)
  (caar
   (sql-insert-id
    table
    [existing] "f"
    [first-change-id] change-id
    [last-change-id] change-id
    )))

(defun apply-single-change (change-id change-metadata 
  data-fields table log-table)
  (let* (
         (store-id (-> change-metadata :related-data-entry-id))
         )
        (unless store-id
                (progn
                 (setf store-id 
                       (insert-empty-record table change-id))
                 (set-> change-metadata :related-data-entry-id store-id)
                 (clsql:update-records 
                  (sql-value :table log-table)
                  :where
                  [= [id] change-id]
                  :av-pairs
                  `(
                    ([related-data-entry-id] ,store-id)
                    )
                  )
                 )
                )
        (unless
         (-> change-metadata :change-applied)
         (sql-pluck (sql-value :table log-table) change-id 
                    (sql-value :table table) store-id 
                    (append data-fields 
                            (list [existing] [status])))
         (clsql:update-records
          (sql-value :table log-table)
          :av-pairs 
          `(
            ([change-applied] "t")
            )
          :where
          [= [id] change-id]
          )
         (let*
          (
           (changes
            (car
             (clsql:select [first-change-id] [last-change-id] :from 
                           (sql-value :table table)
                           :where [= [id] store-id])))
           )
          (update-chain 
           table store-id 'last-change-id 
           (second changes)
           log-table change-id 'previous-applied-id
           'latest 'first-change-id (first changes) 
           ))
         )
        store-id))

(defun apply-fresh-changes (change-struct fields)
  (unless 
   (-> change-struct :change-applied)
   (let*  (
           (table-name (-> change-struct :table-name))
           (table-sym (intern (string table-name)))
           (log-table-sym (log-table-name table-sym))
           (change-metadata-schema
            `(
              ((:code-name :id)
               (:type :int)
               )
              ((:code-name :related-data-entry-id)
               (:type :int)
               )
              ((:code-name :change-applied)
               (:type :bool)
               )
              ((:code-name :rules)
               (:type nil)
               (:sql-where 
                ,[and
                  [= 
                   [containing-changeset-id]
                   (-> change-struct :id)
                   ]
                  ]
                )
               (:sql-from
                ,(sql-value :table log-table-sym))
               )
              )
            )
           (change-transformer 
            (data-transformer change-metadata-schema))
           (related-changes 
            (clsql:query
             (transformer-query 
              change-transformer
              )))
           (data-entry-ids
             (iter
               (for single-change :in related-changes)
               (data-transformer-load-ordered-strings 
                 change-transformer
                 single-change
                 :format-first t
                 :skip-check t
                 )
               (collect
                 (apply-single-change 
                   (-> change-transformer :id)
                   change-transformer
                   fields 
                   table-sym
                   log-table-sym
                   ))
               ))
           )
          (clsql:update-records
           [changesets]
           :av-pairs `(
                       ([change-applied] "t")
                       )
           :where [= [id] (-> change-struct :id)]
           )
          data-entry-ids
          )))

(defun field-list-from-string (s)
  (mapcar 
   (lambda 
    (x) (sql-value :attribute (intern (string-upcase x) :keyword)))
   (cl-ppcre:split "," s)))

(defun apply-changeset (changeset-id fields)
  (let* (
         (parser (data-transformer *versioning-db-data-schema*))
         (query (sql-select-list-query
                 (transformer-db-field-names parser)
                 :from 
                 [changesets]
                 :where
                 [= [id] changeset-id]
                 ))
         (dataline  (car 
                     (clsql:query query
                                  )))
         )
        (data-transformer-load-ordered-strings parser dataline 
                                               :format-first t
                                               :skip-check t
                                               )
        (apply-fresh-changes 
         parser
         (cond
          ((null fields) fields)
          ((listp fields) fields)
          ((eq fields :default) 
           (field-list-from-string (-> parser :affected-fields)))
          ))))

(defun create-changeset (data-source table-name fields)
  (sql-insert-id 'changesets
                 [timestamp] (local-time:timestamp-to-unix (local-time:now))
                 [data-source] data-source
                 [table-name] table-name
                 [affected-fields] (format nil "~{~a~#[~:;,~]~}" 
                                           (mapcar 
                                            (lambda 
                                             (f)
                                             (clsql:sql (sql-create-name f))
                                             )
                                            fields))
                 ))

(defun create-change-entry (changeset log-table-name related-entry &rest data)
  (apply 'sql-insert-id 
         log-table-name
         [related-data-entry-id] related-entry
         [existing] "t"
         [containing-changeset-id] changeset
         data
  ))

(defun apply-logged-changeset 
  (data-source table-name fields old-entries-ids new-entries-data)
  (let*
   (
    (changeset (create-changeset data-source table-name fields))
    (ltn (log-table-name table-name))
    (change-ids
      (iter
        (for de in new-entries-data)
        (for n upfrom 0)
        (for rde := (-> old-entries-ids n))
        (for rde-new := (or rde (insert-empty-record table-name nil)))
        (for change-id :=
             (apply 'create-change-entry
                    changeset ltn rde-new
                    (iter
                      (for f in fields)
                      (for d in de)
          (for fe := (sql-create-name f))
                      (collect fe)
                      (collect d)
                      )
                    ))
        )
      )
    )
   (apply-changeset changeset :default)
   ))

(defun just-create-version
  (table id &key reason)
  (let*
   (
    (data
     (first 
      (with-db
       (select-with-marked-fields
        [*] :from (sql-value :table table)
        :where [= [id] id]))))
    (data (remove :existing data :key 'car))
    (data (remove :id data :key 'car))
    (data (remove :first-change-id data :key 'car))
    (data (remove :last-change-id data :key 'car))
    (data (remove :status data :key 'car))
    (fields
     (loop for f in data collect
           (first f)))
    (cid 
     (create-changeset 
      (format nil "Cloning on ~a. Reason: ~a" 
              (local-time:now) reason)
      table
      fields))
    (lid 
     (apply
      'create-change-entry
      cid (log-table-name table) id
      (loop for x in data
            collect (sql-value :attribute (first x))
            collect (second x)
            )))
    )
   (declare (ignorable lid))
   (apply-changeset
    cid
    (mapcar (lambda (x) (sql-value :table x)) fields))
   cid
   ))

#.(sql-off)
