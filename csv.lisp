(mccme-helpers/packages:file mccme-helpers/csv
  :use (
    :iterate
    :mccme-helpers/trivial
    :mccme-helpers/struct
    :mccme-helpers/functional
  )
  :export (
    #:with-csv-syntax-and
    #:with-csv-file
    #:get-csv-separator
    #:load-csv-file-with-headers
    #:propagate-values-down
    #:load-fix-width-file-with-headers
    #:load-csv-file-to-array
    #:csv-array-to-sorted-boxes
    #:csv-box-keyed-line
    #:csv-box-to-ht-list
    #:csv-box-lines-with-headers
    #:csv-box-pad
    #:csv-box-split-by-empty-rows
    #:csv-box-cut-by-position
    #:csv-box-line-emptiness-change
    #:csv-boxes-take-by-order
    #:csv-box-to-bimap
    #:load-csv-file
  )
)

(defmacro with-csv-syntax-and (overrides &body body)
  `(fare-csv::with-rfc4180-csv-syntax 
    ()
    (progv (list 'fare-csv::*accept-cr* 'fare-csv::*accept-lf*  'fare-csv::*accept-crlf*
                 ,@(mapcar (lambda (x) `',(find-symbol (symbol-name (car x)) 'fare-csv)) overrides))
           (list t t t ,@(mapcar 'cadr overrides))
    ,@body
    )))

(defmacro with-csv-file-straight (file file-sym overrides &key (external-format :utf-8) body)
  `(with-open-file (,file-sym ,file :direction :input :external-format ,external-format)
                   (with-csv-syntax-and ,overrides
                                        ,@body)))

(defmacro with-csv-file (args &rest body)
  `(with-csv-file-straight ,@args :body ,body))

(defmacro load-csv-file (fn &rest args)
  (let*
    ((f (gensym)))
    `(with-csv-file 
       (,fn ,f ,@(or args (list nil)))
       (when (equal (peek-char nil ,f nil) (code-char #xfeff))
         (read-char ,f))
       (fare-csv:read-csv-stream ,f))))

(defun get-csv-separator (line &optional (first-field-symbol-p (constantly nil)) 
                               (delimiter-p (lambda (x) (equal x #\"))))
  (let*
    ((delimiter #\")
     (delimiter-seen nil)
     (in-quoted nil)
     (sep0
       (iter 
         (for c :in-string line)
         (unless (and (not (funcall delimiter-p c)) (funcall first-field-symbol-p c))
           (if (funcall delimiter-p c)
             (setf delimiter c delimiter-seen t)
             (return c))))))
    (if delimiter-seen
      (iter 
        (for c :in-string line)
        (cond
          ((equal c delimiter) (setf in-quoted (not in-quoted)))
          ((funcall first-field-symbol-p c) nil)
          (in-quoted nil)
          (t (return c))
          ))
      sep0)))

(defun load-csv-file-with-headers
  (fname lead-in header-height lead-out &key
    (test 'equalp) (line-separator " ")
    (extra-data-position nil) (extra-data-offset nil)
    (charset :utf-8)
    (charsets nil)
    (charset-check-regex nil)
    (possible-delimiters `(#\Return #\LineFeed #\Tab #\; #\, #\#))
    (first-field-symbol-p nil)
    (force-field-names nil)
    (force-quote #\")
    (keep-header-newlines nil)
    (header-trim *whitespace-list*)
    (data-trim *whitespace-list*)
    (also-whole-line nil)
    (just-process nil)
    (header-processor nil)
  )
  (let*
   (
    (charset
      (cond
        (charsets
         (loop 
           for c in charsets
           for line := (ignore-errors (with-open-file (f fname :external-format c) (read-line f)))
           when (and line (or (not charset-check-regex) (cl-ppcre:scan charset-check-regex line)))
           return c
           ))
        (t charset)
        ))
    (separator-and-quote
      (with-open-file (f fname :external-format :iso8859-1)
        (loop 
          for first-line := (read-line f)
          for separator := 
          (get-csv-separator 
            first-line 
            (or first-field-symbol-p
                (lambda (x)
                  (not
                    (find x possible-delimiters 
                          :test 'equalp))
                  )))
          for quote := (multifuncall force-quote :args (list first-line))
          when separator return (list separator quote)
          )))
    (separator (first separator-and-quote))
    (force-quote (or (second separator-and-quote) force-quote))
    (extra-data-ht (make-hash-table :test 'equal))
   )
   (with-csv-file 
     (fname f 
            ((*separator* separator) 
             (*skip-whitespace* nil)
             (*quote* force-quote)
             )
            :external-format charset)
     (when (equal (peek-char nil f nil) (code-char #xfeff))
       (read-char f))
     (iter
       (for n upfrom 1)
       (with hs := force-field-names)
       (for l := (fare-csv::read-csv-line f))
       (while l)
       (when
         (and extra-data-position
              (= n extra-data-position))
         (iter
           (generate x in (if extra-data-offset (cdr l) l))
           (let*
             (
              (key (string-trim header-trim (next x)))
              (val (string-trim data-trim (next x)))
              )
             (unless
               keep-header-newlines
               (setf key (substitute #\Space #\Return key))
               (setf key (substitute #\Space #\Linefeed key))
               (setf val (substitute #\Space #\Return val))
               (setf val (substitute #\Space #\Linefeed val))
               )
             (setf (gethash key extra-data-ht) val)
             )
           )
         )
       (cond
         ((<= n lead-in)
          )
         ((<= n (+ lead-in 1) (+ lead-in header-height))
          (setf hs (mapcar (lambda (x) (string-trim header-trim x)) l)))
         ((<= n (+ lead-in header-height))
          (setf hs (mapcar 
                     (lambda (x y) 
                       (concatenate 'string x line-separator 
                                    (string-trim header-trim y)))
                     l hs))
          )
         ((<= n (+ lead-in header-height lead-out))
          )
         (:else
           (when header-processor
             (setf hs (mapcar header-processor hs))
             (setf header-processor nil) ; I detest this
           )
           (let*
             (
              (line-data 
                (iter
                  (with ht := (make-hash-table :test test))
                  (for h in hs)
                  (for d in l)
                  (setf (gethash h ht) (string-trim data-trim d))
                  (finally 
                    (when also-whole-line
                      (setf (gethash also-whole-line ht)
                            (mapcar (lambda (x) (string-trim data-trim x)) l)))
                    (return ht))
                  )
                )
              )
             (if 
               just-process
               (funcall just-process line-data)
               (collect line-data into hts)
               )
             )
           )
         )
       (unless 
         (or keep-header-newlines
             (> n (+ lead-in header-height lead-out)))
         (setf hs (mapcar (lambda (x) (substitute #\Space #\Return x)) hs))
         (setf hs (mapcar (lambda (x) (substitute #\Space #\Linefeed x)) hs))
         )
       (finally 
         (return (values (unless just-process hts) extra-data-ht hs)))
       ))
   ))

(defun propagate-values-down (hts &rest columns)
  (iter
   (for pht initially nil then ht)
   (for ht in hts)
   (iter
    (for c in columns)
    (when
     (and
      pht
      (= (length (-> ht c)) 0)
      (-> pht c)
      )
     (set-> ht c (-> pht c))
     )
    )
   ))

(defun load-fix-width-file-with-headers 
  (file &key (encoding :utf-8) (trimmable '(#\Space #\Tab)) 
        (separator-regexp " {2,}") (header-word-delimiters "[ _]")
        (skip 0))
  (with-open-file 
   (f file :external-format encoding :direction :input)
   (let*
    (
     (first-line (read-line f))
     (dummy (loop for n from 1 to skip do (read-line f nil)))
     (headers (mapcar (lambda (x) (string-trim trimmable x)) 
                      (cl-ppcre:split separator-regexp first-line)))
     (header-symbols (loop for x in headers collect 
                           (intern (string-upcase 
                                    (cl-ppcre:regex-replace-all 
                                     header-word-delimiters x "-"))
                                   :keyword)))
     (positions 
      (iterate (for s initially 0 then x)
               (for x := 
                    (second 
                     (multiple-value-list 
                      (cl-ppcre:scan separator-regexp first-line :start s))))
               (collect x)
               (while x)
               ))
     (data
      (iterate
       (for l := (read-line f nil))
       (while l)
       (collect
        (iterate
         (for b initially 0 then e)
         (for e in positions)
         (for hs in header-symbols)
         (collect (list hs (string-trim trimmable (subseq l b e))))
         ))
       ))
     )
    (declare (ignorable dummy))
    data
    )))

(defun load-csv-file-to-array 
  (fname &key (charset :utf-8) 
         (possible-delimiters `(#\Return #\LineFeed #\Tab #\; #\, #\#))
         (first-field-symbol-p nil)
         (force-quote #\"))
  (let*
    (
     (separator
       (with-open-file (f fname :external-format charset)
         (loop 
           for first-line := (read-line f nil nil)
           while first-line
           for separator := 
           (get-csv-separator 
             first-line
             (or first-field-symbol-p
                 (lambda (x)
                   (not
                     (find x possible-delimiters 
                           :test 'equalp))
                   )))
           when separator return separator
           ))
       )
     (list-data 
       (with-csv-file (fname f 
                             ((*separator* separator) 
                              (*skip-whitespace* nil)
                              (*quote* force-quote)
                              )
                             :external-format charset)
                      (when (equal (peek-char nil f nil) (code-char #xfeff))
                        (read-char f))
                      (loop
                        while (peek-char nil f nil nil)
                        collect (fare-csv::read-csv-line f)
                        )
                      ))
     (array-data
       (map 'vector (lambda (x) (map 'vector 'identity x)) list-data))
     (res array-data)
     )
    res
    )
  )

(defun csv-array-to-boxes (data &key force-contiguous force-break)
  (let*
   (
    (height (length data))
    (width (apply 'max (map 'list 'length data)))
    (statuses (make-array (list height width) :initial-element nil))
    (cells (list))
    (boxes (list))
    )
   (loop 
    for i upfrom 0 to (1- height)
    for x := (aref data i)
    do
    (loop
     for j upfrom 0 to (1- (length x))
     for y := (aref x j)
     do
     (when
      (or
       (and 
        (> (length y) 0)
        (not
         (and 
          force-break
          (funcall
           force-break
           data x i j
           ))))
       (and 
        force-contiguous
        (funcall 
         force-contiguous
         data x i j
         )
        )
       )
      (setf (aref statuses i j) :data)
      (push (list i j) cells)
      )
     )
    )
   (loop 
    for c in cells
    when (eq (apply 'aref statuses c) :data)
    do
    (let*
     (
      (work-queue (list c))
      (lx (second c))
      (rx (second c))
      (ty (first c))
      (by (first c))
      )
     (loop
      while work-queue
      for c := (pop work-queue)
      when (eq (apply 'aref statuses c) :data)
      do
      (progn
       (setf (aref statuses (first c) (second c)) :boxed)
       (setf lx (min lx (second c)))
       (setf rx (max rx (second c)))
       (setf ty (min ty (first c)))
       (setf by (max by (first c)))
       (loop
        for dc in '((-1 0) (1 0) (0 -1) (0 1))
        for nc := (mapcar '+ c dc)
        when (<= 0 (second nc) (1- width))
        when (<= 0 (first nc) (1- height))
        do
        (progn
         (when (eq :data (apply 'aref statuses nc))
               (push nc work-queue)
               )
         )
        )
       )
      )
     (push (list ty lx by rx) boxes)
     )
    )
   (let*
    (
     (merged-boxes nil)
     )
    (loop
     for added in boxes
     do
     (let*
      (
       (curr added)
       (next-curr nil)
       (last-curr curr)
       )
      (loop
       while curr
       do
       (loop
        for x in merged-boxes
        until next-curr
        when (and
              (<= (first x) (third curr))
              (>= (third x) (first curr))
              (<= (second x) (fourth curr))
              (>= (fourth x) (second curr))
              )
        do
        (progn
         (setf merged-boxes (remove x merged-boxes :test 'equal))
         (setf curr (list
                     (min (first curr) (first x))
                     (min (second curr) (second x))
                     (max (third curr) (third x))
                     (max (fourth curr) (fourth x))
                     ))
         )
        )
       do (setf last-curr curr)
       do (setf curr next-curr)
       )
      (push last-curr merged-boxes)
      )
     )
    merged-boxes
    )
   )
  )

(defun sort-boxes (boxes side)
  (let*
   (
    (sides
     '(
       (:top-left (1 2))
       (:top-right (1 -4))
       (:bottom-right (-3 -4))
       (:bottom-left (-3 2))
       (:left-top (2 1))
       (:right-bottom (-4 -3))
       ))
    (side (-> sides side side))
    (processed-boxes (mapcar 
                      (lambda (x) 
                              (list 
                               (mapcar 
                                '* 
                                (mapcar 
                                 (lambda (y) 
                                         (nth (- (abs y) 1) x)) side) side) x))
                      boxes))
    (sorted-boxes (sort processed-boxes 'uni-<))
    (clean-boxes (mapcar 'second sorted-boxes))
    (res clean-boxes)
    )
   res
   )
  )

(defun extract-box (box data)
  (map
    'vector
    (lambda (x) 
      (pad-array
        (subseq x (min (second box) (length x)) (min (+ 1 (fourth box)) (length x)))
        (- (fourth box) (second box) -1) ""
        ))
    (subseq data (first box) (+ 1 (third box)))
    )
  )

(defun csv-array-to-sorted-boxes (data side &key cut-args sort-args)
  (mapcar (lambda (x) (extract-box x data))
          (apply 
           'sort-boxes
           (apply 'csv-array-to-boxes data cut-args)
           side
           sort-args)))

(defun csv-box-keyed-line 
  (data &key (line 0) (offset 0) (colon nil) (vert nil)
        (name-translation nil) (trim *whitespace-list*)
        (ht-creator (lambda () (make-hash-table :test 'equal))))
  (let*
    (
     (ht (funcall ht-creator))
     (dataline line)
     (dataline (if vert 
                 (map 'vector (lambda (x) (elt x dataline)) data) 
                 (elt data dataline)))
     (dataline (subseq dataline offset))
     )
    (iterate
      (generate x in-vector dataline)
      (for key := (string-trim trim (next x)))
      (for value := (string-trim trim (next x)))
      (when
        (or
          (not colon)
          (prog1
            (cl-ppcre:scan ": *$" key)
            (setf key (cl-ppcre:regex-replace ": *$" key ""))
            )
          )
        (for translated-key := (-> name-translation key key))
        (set-> ht translated-key value)
        )
      )
    ht
    )
  )

(defun translate-strings
  (data translation)
  (block nil
         (let*
          ((data data))
          (loop
           for n from 0 to (1- (length data))
           for x := (elt data n)
           for y := (-> translation x)
           when y do (setf (elt data n) y)
           finally (return data)
           )
          )
         )
  )

(defun csv-box-headers
  (data &key (from 0) (to 0) (separator " ") (translation nil)
        (trim *whitespace-list*) (vert nil))
  (iterate
   (with size := (if vert (length data) (length (elt data 0))))
   (with headers := (make-array (list size) :initial-element ""))
   (for n from from to to)
   (iter
    (for c from 0 to (1- size))
    (for h := (elt headers c))
    (for add := (if vert (elt (elt data c) n) (elt (elt data n) c)))
    (for add-trim := (string-trim trim add))
    (for h-trim := (string-trim trim h))
    (for sep := (if (or (emptyp h-trim) (emptyp add-trim)) "" separator))
    (setf (elt headers c) (concatenate 'string h-trim sep add-trim))
    )
   (finally (return (translate-strings headers translation)))
   )
  )

(defun 
  csv-box-to-ht-list
  (data headers &key (offset 1) (vert nil) (trim *whitespace-list*)
        (ht-creator (lambda () (make-hash-table :test 'equal))))
  (iterate
    (with size := (if vert (length data) (length (elt data 0))))
    (with cosize := (if vert (length (elt data 0)) (length data)))
    (for n from offset to (1- cosize))
    (for ht := (funcall ht-creator))
    (iter
      (for m from 0 to (1- size))
      (set->
        ht (-> headers m) 
        (string-trim 
          trim (if vert (--> data nil m n) (--> data nil n m))))
      )
    (collect ht)
    )
  )

(defun
  csv-box-lines-with-headers
  (data &key (vert nil) (trim *whitespace-list*) header-params body-params)
  (let*
    (
     (h (apply 'csv-box-headers data :vert vert :trim trim header-params))
     (d (apply 'csv-box-to-ht-list data h :vert vert :trim trim body-params))
     (res d)
     )
    res
    ))

(defun
  csv-box-cut-by-position
  (data &key (vert nil) (position 1) (choose nil))
  (unless position (return-from csv-box-cut-by-position (make-array 0)))
  (let*
   (
    (vert (or vert (find choose '(:left :right))))
    (cutter (lambda (x y) (if vert 
                              (map 'vector
                                   (lambda (z) (subseq z x y))
                                   data)
                              (subseq data x y))))
    (res (case choose
               ((0 :first :left :top) (funcall cutter 0 position))
               ((1 :second :right :bottom) (funcall cutter position nil))
               ((nil) (list
                       (funcall cutter 0 position)
                       (funcall cutter position nil)
                       ))
               )
         )
    )
   res
   )
  )

(defun
  csv-line-emptiness-change
  (data &key (line 1) (vert t))
  (let*
   (
    (accessor (if vert
                  (lambda (k) (elt (elt data k   ) line))
                  (lambda (k) (elt (elt data line) k   ))))
    (span
     (if vert
         (length data)
         (length (elt data line))
         ))
    (initial (= (length (funcall accessor 0)) 0))
    (switch
     (loop 
      for k from 1 to (1- span)
      unless (equal initial (= (length (funcall accessor k)) 0))
      return k))
    )
   switch
   ))

(defun 
  non-zero-length (x)
  (or
   (loop 
    for n downfrom (1- (length x)) to 0 
    when (> (length (elt x n)) 0) 
    return (1+ n))
   0))

(defun 
  csv-box-pad (data)
  (let*
   (
    (trim-empty
     (lambda (x) (subseq x 0 (non-zero-length x))))
    (data (map 'vector trim-empty data))
    (data (funcall trim-empty data))
    (w (apply 'max (map 'list 'length data)))
    )
   (map 'vector
        (lambda (x) (pad-array x w ""))
        data)
   )
  )

(defun
  csv-box-split-by-empty-rows
  (data)
  (let*
   ((res (list nil)))
   (iter
    (for x in-vector data)
    (if
     (> (non-zero-length x) 0)
     (push x (car res))
     (push nil res)))
   (mapcar
    (lambda 
     (x)
     (map 'vector
          'identity
          (reverse x))
     )
    (reverse (remove nil res)))))

; rules
; ((:top-left 0)) is a ruleset that takes one box - the top left one
(defun
  csv-boxes-take-by-order
  (data rules &key cut-args)
  (loop
   with last-order := nil
   with sorted-boxes := (apply 'csv-array-to-boxes data cut-args)
   with chosen := nil
   for rule in rules
   for new-order := (first rule)
   for choice := (second rule)
   while sorted-boxes
   ; re-sort
   unless (equal last-order new-order) 
   do (progn
       (setf last-order new-order)
       (setf sorted-boxes (sort-boxes sorted-boxes new-order))
       )
   ; choose
   when (functionp choice) 
   do (setf choice (funcall choice sorted-boxes))
   ; take
   do (take chosen sorted-boxes choice)
   collect (extract-box chosen data)
  ))

(defun csv-box-to-bimap 
  (box &key vert (trim *whitespace-list*)
       header-settings 
       row-header-settings 
       column-header-settings
       )
  (let*
   (
    (hw (or (getf row-header-settings :to) (getf header-settings :to) 0))
    (hh (or (getf column-header-settings :to) (getf header-settings :to) 0))
    (row-headers 
     (subseq
      (apply
       'csv-box-headers
       box :vert t
       (append
        row-header-settings
        header-settings
        `(:trim ,trim)
        )
       )
      (1+ hh)
      )
     )
    (column-headers 
     (subseq
      (apply
       'csv-box-headers
       box :vert nil
       (append
        column-header-settings
        header-settings
        `(:trim ,trim)
        )
       )
      (1+ hw)
      )
     )
    (real-data
     (map
      'vector
      (lambda (x) (subseq x (1+ hw)))
      (subseq box (1+ hh))
      ))
    (res (make-hash-table :test 'equal))
    )
   (loop 
    for i from 0 to (1- (length real-data))
    for row-key := (-> row-headers i) 
    do
    (loop
     for j from 0 to (1- (length (elt real-data 0)))
     for val := (aref (aref real-data i) j)
     for column-key := (-> column-headers j)
     for first-key := (if vert column-key row-key)
     for second-key := (if vert row-key column-key)
     do (set--> res val nil first-key second-key)
     )
    )
   res
   )
  )
